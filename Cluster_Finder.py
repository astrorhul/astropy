import pylab as _pl
import numpy as _np
import Image_Analysis as _Image_Analysis

class Cluster_Finder():
    '''
    Given a 2D numpy array will use a flood fill algorithm to identify stars (clusters of pixels) above a given threshold set by the user.
    '''
    def __init__(self, image, threshold = 500, debug_print = False):
        self.debug_print = debug_print
        self.image       = image
        self.thresh      = self.set_threshold(threshold)
        self.x_size      = self.image.shape[0]
        self.y_size      = self.image.shape[1]
        self.stack       = []

    def __cluster_point_checker(self, point):
        x, y = point[0], point[1]

        if self.thresh_image[x, y] == 1:
            self.thresh_image[x, y] = 0

            if x > 0:
                self. __cluster_point_checker([x - 1, y])
            if x < (self.x_size - 1):
                self.__cluster_point_checker([x + 1, y])
            if y > 0:
                self.__cluster_point_checker([x, y - 1])
            if y < (self.y_size - 1):
                self.__cluster_point_checker([x, y + 1])

            self.stack.append(point)

    def cluster(self):
        c = []

        for x in range(self.x_size):
            for y in range(self.y_size):
                if self.thresh_image[x, y] == 1.: # Marked as a pixel covering a star
                    self.__cluster_point_checker([x, y])
                    c.append(self.stack)
                    self.stack = []

        clusters = []
        for cluster in c:
            clusters.append(_np.array(cluster))

        if self.debug_print:
            print 'Cluster_Finder.cluster()>  ', clusters

        return clusters

    def set_threshold(self, thresh):
        ia = _Image_Analysis.Image_Analysis(self.image)
        if thresh < float(ia.mode()[0]):
            thresh = ia.mode()[0] + 100

        self.thresh       = thresh
        self.thresh_image = (1.0 * (self.image > self.thresh))

        if self.debug_print:
            print 'Cluster_Finder.threshold()>'
            print 'Thresh:                    ', self.thresh
            print 'Thresh image:              ', self.thresh_image

def test(threshold = 1000, simple_image = True, debug_print = False):
    import pyfits as _pyfits
    import Globals as _Globals

    if simple_image:
        image = _pyfits.open(_Globals.photom_data_example_simple)[0].data
    else:
        image = _pyfits.open(_Globals.photom_data_example_complex)[0].data


    cf = Cluster_Finder(image, threshold, debug_print)

    cf.set_threshold(threshold)

    clusters = cf.cluster()

    # Plotting
    fig = _pl.figure(1)
    fig.clf()
    _pl.imshow(cf.image, cmap = 'gray')

    for cluster in clusters:
        _pl.plot(cluster[:, 1], cluster[:, 0], '+', c = 'y', alpha = 1)
