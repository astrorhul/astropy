% Latex example for RHUL physics undergraduates 
% Stewart T. Boogert 2014

\documentclass[11pt]{article}								% [Font size]{Type of document}

% Non standard packages 
\usepackage[textwidth=17cm,textheight=24cm]{geometry}			% See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                      							% ... or a4paper or a5paper or ... 
\usepackage{graphicx}									% Package for including graphics 
\usepackage{amssymb}									% American Mathematical Society Symbols and fonts
\usepackage{epstopdf}									% Conversion of eps to pdf files 
\usepackage{hyperref}     									% Hyper text links in document and to external URLs
\linespread{1.5} 										% Single, Double, 1.5 line spacing, just put in the factor, here it is 1.5 times normal spacing



\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\title{RHUL Physics department report latex template}
\author{Dr~S.~T.~Boogert}
\date{}                                           							% Activate to display a given date or no date

\begin{document}
\maketitle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduction 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction \label{sec:Introduction}}
This is short guide to using latex. It covers the main parts of using latex for producing clear laboratory and project reports. Some of the most important aspects of physics reports are covered, these include equations (Section \ref{sec:Equations}), figures (Section \ref{sec:Figures}), tables (Section \ref{sec:Tables}), lists and enumerations (Section \ref{sec:Lists}) and finally references and labelling (Section \ref{sec:Labels}). 

Latex is not Microsoft word, is does not allow you to control many aspects of the document generation, for example exact figure placement. It does many things automatically which are very useful, like numbered sections, equations, figures, references. This makes it perfect for physics reports, which of course have many figures, equations and references. It allows the writer to concentrate on the content and not how to layout the document.

This document is not meant to be a complete overview of Latex for making scientific documents and there will be references which direct you to excellent online resources. This latex file can simply be adapted for your lab reports and then your document will conform to a basic standard which should be good enough for laboratory reports, 3rd year scientific skills reports, 4th year research review and finally and most importantly BSc and MSci project reports. This document is written with lots of comments in the latex code, please look at the comments to try and understand how to use latex commands correctly. Because you are using latex, please don't try and make your reports looks like peer reviewed papers, so don't use two columns, do not try and use overly technical words or write in a complex way. Simple sentences that are clear and to the point will convey your message clearly. Avoid using a table of contents (as well as list of figures, tables etc) as your reports rarely should be long enough to require them. 

New paragraphs are started by adding a blank line in the latex source. A paragraph should indicate a separate concept or point and not be used as a formatting tool. Look for the indentation at a start of a line, it is common for people new to latex to add lots of returns and effectively start many new paragraphs.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Equations 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Equations \label{sec:Equations}}
Equations are defined using a special notation in Latex.  Equations should appear as part of the text and read as if there were a continuation of a sentence or clause. All of the variables must be defined at least once in the document. Equations can appear inline in text, for example $y = a x + b$ or as a separate numbered equation as in Equation \ref{eqn:firstExample}. The dependent variable $y$ is given by 
\begin{equation}										% Start of equation environment 
\label{eqn:firstExample}										
y = ax + b,						
\end{equation}											% End of equation environment 
where $x$ is independent variable, $a$ is the gradient and $b$ is the intercept. Note how {\bf all} of the variables have been defined, this should happen {\bf without} exception and how punctuation is used as if the equation were just in the text, imagine reading out the sentence containing the equation aloud.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and diagrams 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Figures and diagrams \label{sec:Figures}}
Below in Figure \ref{fig:exampleFigure} is an example of how to include a pdf figure into a latex document. Figures should always be explained in the text. Figure \ref{fig:exampleFigure} shows an example of the output from the first year Python fitting program. Gaussian {\it fake} data is simulated with experimental noise and fitted using the scipy.optimize function curve\_fit.  
\begin{figure}[htbp] 										% Figure placement: [h]ere, [t]op, [b]ottom, or [p]age
   \centering											% Center in the column
   \includegraphics[width=10cm]{exampleFigure.pdf} 				% Command to include the graphics file 
   \caption{Figure made with the 1st year python curve fit program.}	% Caption
   \label{fig:exampleFigure}								% Label so the figure can be referred to before or later
\end{figure}
Figures should be as large as possible without compromising the text. They should be placed as close as possible to the text which is referring to them. Do not add returns between text and graphics as latex will assume this is a new paragraph. Like equations figures should be considered as part of the text and something that helps the reader understand what you have done, calculated, measured and/or analysed.  

Diagrams are very important in conveying an explanation of experimental apparatus or method. The diagram should be produced in a good drawing package like Inkscape, Xfig, Adobe Illustrator. These packages take some time to learn but it is useful to develop the skill early. Diagrams should be clearly labelled and annotated, using the same notation in the text. 

In Figure \ref{fig:michelsonSchematic} in a plan diagram of a Michelson interferometer. 
\begin{figure}[htbp] 										% Figure placement: [h]ere, [t]op, [b]ottom, or [p]age
   \centering											% Center in the column
   \includegraphics[width=8cm]{Michelson.pdf} 					% Command to include the graphics file 
   \caption{Michelson interferometer schematic}					% Caption
   \label{fig:michelsonSchematic}							% Label so the figure can be referred to before or later
\end{figure}
The main important components of a Michelson interferometer is a beam splitter which splits the source light into two orthogonal paths. Each path is reflected by two mirrors, $M_1$ and $M_2$. Mirror $M_1$ can be translated, marked on the figure as distance $t$, in the light direction. Mirror $M_2$ can not translate, but can be adjusted in angle to make it parallel to $M_1$. The light after reflection at $M_1$ and $M_2$ is recombined and interferes at the beam splitter and is reflected towards the viewer $E$.

Photos of experiments are also very useful, but these should be annotated and clearly explained. Try to avoid using photos of similar experiments, samples, astronomical pictures, etc downloaded from the internet unless there is a very good reason for doing so. Do not be afraid to modify figures obtained from other sources to best explain your point. 

Figure \ref{fig:telescopePhoto} is a photograph of the RHUL 12-inch diameter Meade LX200-GPS telescope. The telescope is reflecting Cassegrain design. The important components of the telescope for this observation are marked in Figure \ref{fig:telescopePhoto}
\begin{figure}[htbp] 										% Figure placement: [h]ere, [t]op, [b]ottom, or [p]age
   \centering											% Center in the column
   \includegraphics[width=8cm]{Telescope.jpg} 					% Command to include the graphics file 
   \caption{Photo of RHUL 12" Meade Cassegrain telescope}		% Caption
   \label{fig:telescopePhoto}								% Label so the figure can be referred to before or later
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tables 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Tables \label{sec:Tables}}
Tables are very good for explaining data and calculations. 

Table \ref{tab:data} shows the data files taken for different voltage source settings. Alway consider putting similar information into tables, not just data.
\begin{table}[htdp]										% Table placement: [h]ere, [t]op, [b]ottom, or [p]age
\begin{center}
\begin{tabular}{|l|l|} \hline									% Tabular environment 
 Voltage [V] & File name \\  \hline\hline
 1.0    & voltages\_v1.0.dat \\
 2.0    & voltages\_v2.0.dat  \\
 3.0    & voltages\_v3.0.dat  \\
 4.0    & voltages\_v4.0.dat \\ \hline
\end{tabular}
\caption{Data files for different voltage source settings and the corresponding data files.}
\end{center}
\label{tab:data}											% Label 
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lists and enumerations 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Enumerations and lists \label{sec:Lists}}
An enumerated list is numbered and a list is just bulleted. Here is an example of the \verb|enumerate| environment.
\begin{enumerate}
\item Theoretical introducton
\item Experimental procedure
\item Results 
\item Analysis 
\item Discussion and conclusions
\end{enumerate}

Here is the same \verb|enumerate| as a \verb|list|
\begin{itemize}
\item Theoretical introduction
\item Experimental procedure
\item Results 
\item Analysis 
\item Discussion and conclusions
\end{itemize}

Nesting a list and having sub-bullets is done by adding a list as an \verb|\item|
\begin{itemize}
\item Theoretical introduction
\begin{itemize} 
\item Newtonian gravity 
\item Einstein and general relativity
\item Quantum gravity
\end{itemize}
\item Experimental procedure
\item Results 
\item Analysis 
\item Discussion and conclusions
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Labels and cross-references 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Labels and cross-references \label{sec:Labels}}
All equations, figures, tables, sections can be cross-referenced in the text. It is done by adding Latex code like the following
\begin{verbatim}
\label{sec:Introducton}
\end{verbatim}
which labels the Introduction section. It is used in this document in the following way 
\begin{verbatim}
\section{Introduction \label{sec:Introduction}}
\end{verbatim}
To make a cross-reference, like the following to Section \ref{sec:Introduction}, the following code is used 
\begin{verbatim}
Section \ref{sec:Introduction}
\end{verbatim}
It is useful to put simple but obvious names for labels in the latex, for example labels used in this document include 
\begin{verbatim}
sec:Introduction
tab:data
fig:exampleFigure
\end{verbatim}
where \verb|sec| refers to a section, \verb|tab| a table and \verb|fig| a figure or plot.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code and programs 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Code and programs}
Generally adding code and programs is not very useful for a report and if included probably best in an appendix. There are exceptions when there is a need to explain a particular piece of code or algorithm. An example is the \verb|Python| or \verb|C++| courses where some syntax or code fragment needs explaining. For example the \verb|curve_fit| which does a non-linear least squares optimisation of a function. This is an example of how \verb|curve_fit| is called 
\begin{verbatim}
p1 = scipy.optimize.curve_fit(self.linear,xData,yData,p0,yErr,
                              absolute_sigma=True,xtol=1e-12) 
\end{verbatim}
where \verb|xData| is the independent variable \verb|numpy.array|, \verb|yData| is the dependent variable \verb|numpy.array|, \verb|p0| is an array of the first guess of the function parameters, \verb|yErr| is the error on the dependent variable and \verb|linear| is the function to fit, in this case a linear function so in \verb|Python| is 
\begin{verbatim}
def linear(x,p0,p1) : 
        return p0*x+p1
\end{verbatim} 
where again \verb|x| is an array of the independent variables, \verb|p0| and \verb|p1| are the  parameters of the linear function. The parameter \verb|absolute_sigma| indicates the errors are absolute opposed to relative and \verb|xtol| is the relative error in the approximate solution. Here when explaining the code it is done in a very similar way to equations, variables and function arguments are clearly explained after the function is defined.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Appendices 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Appendicies}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliography 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Bibliography}



\end{document}  