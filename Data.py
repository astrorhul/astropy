import pyfits as _pyfits
import numpy as _np

class Data : 
    def __init__(self, data = None, errors = None, metaData = None) : 
        self.d = data      # Data
        self.e = errors    # Errors 
        if metaData == None : 
            self.m = {}  # Meta data dictionary 
        else :
            self.m = metaData
    def add(self, par) : 
        pass
    
    def sub(self, par) : 
        pass 

    def mul(self, par) : 
        pass

    def div(self, par) : 
        pass 

    def shape(self) : 
        return self.d.shape()

    def proj(self, axis) : 
        proj  = self.d.sum(axis)
        projE = _np.sqrt((self.e**2).sum(axis))
        
        return Data(proj,projE,{'name':self.m['name']+'_proj'+str(axis),
                                'from1':self.m['name']})

    def load(self, fileName) :         
        pass 

    def save(self, fileName) : 
        pass 

    def loadFits(self, fileName) : 
        '''Load fits file and convert to NPE''' 
        f = _pyfits.open(fileName) 
        d = f[0].data
        h = f[0].header 

        gain  = h['EGAIN'] 
        self.d = d*gain 
        self.e = _np.sqrt(self.d)
        self.m['fileName'] = fileName
        self.m.update(h)
