import ErrorImage as _ErrorImage
import Manager    as _Manager 
import pyfits     as _pyfits
import pylab      as _pylab

_manager = _Manager.gm()

def displayImage(iden, figure = True) :
    if figure : 
        _pylab.figure()
    image = _manager.getImage(iden) 
    _pylab.imshow(image['img'].image)

def displayPlot(iden , figure = True, errors = True, logy = False, fmt = '-') : 
    if figure : 
        _pylab.figure()
    data = _manager.getData(iden) 
    plt  = data['plot']
    if len(plt.keys()) != 0 : 
        xKey   = plt['x']
        yKey   = plt['y']
        yErrKey= plt['yerr']
        xData   = data['data'][xKey]
        yData   = data['data'][yKey]
        yErrData= data['data'][yErrKey] 

        if errors : 
            _pylab.errorbar(xData,yData,yErrData,fmt=fmt)
        else : 
            _pylab.plot(xData,yData,fmt)
            
        if logy : 
            a = _pylab.gca() 
            a.set_yscale('log') 

def saveFigure(fileName) :
    pass

