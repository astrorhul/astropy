# Python Script for Data Analysis on Photometry Major Project
# Physics Department at Royal Holloway, University of London
# Pedro de Carvalho Paiva, 2012/13

# *** Import Section *** #
import numpy
from pylab import *
import minuit
import scipy.integrate
from scipy import ndimage
import ctypes 
import types

# load shared lib
lib = ctypes.cdll.LoadLibrary('./libC/astro.dylib')
# extract function from lib
chi2_cfun = lib.chi2

def multiGauss2D(bg, bgx, bgx2, bgy, bgy2, bgxy, A, mux, muy, sigx, sigy, angle):
    par = [bg, bgx, bgx2, bgy, bgy2, bgxy, A, mux, muy, sigx, sigy, angle]
    var = lambda x, y: [x, y]
    
    return lambda x, y: multiGauss2D_cfun(ctypes.c_void_p(var(x,y).ctypes.data), ctypes.c_void_p(par.ctypes.data))

# Define a 2D Gaussian possibly rotated
def gaussian(A, sigx, sigy, mux, muy, angle):
    sigx = float(sigx)
    sigy = float(sigy)
    angle = pi/180. * float(angle)
    a = (cos(angle)**2) / (2*sigx**2) + (sin(angle)**2) / (2*sigy**2)
    b = sin(2*angle) / (4*sigy**2) - sin(2*angle) / (4*sigx**2)
    c = (cos(angle)**2) / (2*sigy**2) + (sin(angle)**2) / (2*sigx**2)
    return lambda x, y: (A / (2*pi*sigx*sigy)) * exp(-(a * ((x - mux)**2) + 2 * b * (x - mux) * (y - muy) + c * ((y - muy)**2) ))

def multiGaussian(*par):
    bg = lambda x, y: par[0] + par[1]*x + par[2]*x**2 + par[3]*y + par[4]*y**2 + par[5]*x*y
    field = bg
    
    starNumber = len(par)/6 - 1
    
    def addFunctions(f1, f2):
        return lambda x, y: f1(x, y) + f2(x, y)
    
    for i in range(1, starNumber+1):
        gausPar = par[6*i:6*(i+1)]
        field = addFunctions(field, gaussian(*gausPar))
        
    return field


def chi2(img, *pars):
    indata = ctypes.c_void_p(img[0].ctypes.data)
    inerrors = ctypes.c_void_p(img[1].ctypes.data)
    rowcount = img[2]['NAXIS1']
    colcount = img[2]['NAXIS2']

    par = []
    for p in pars:
        par.append(p)
        
    len_par = len(par)
    pointer = (len_par * ctypes.c_double)(*par)
    
    chi2value = ctypes.c_double(0)
    chi2_cfun(indata, inerrors, rowcount, colcount, pointer, len_par, ctypes.byref(chi2value))
    print chi2value.value
    return chi2value.value

def create_starsFunc(img, starNumber = 1, name = 'stars'):
    args = 6*(starNumber+1)
    varnames = ['bg', 'bgx', 'bgx2', 'bgy', 'bgy2', 'bgxy']
    for i in range(1, starNumber+1):
        varnames.append('a'+str(i))
        varnames.append('mux'+str(i))
        varnames.append('muy'+str(i))
        varnames.append('sigx'+str(i))
        varnames.append('sigy'+str(i))
        varnames.append('angle'+str(i))
        
    co_varnames = tuple(varnames)
    
    def chi2(*pars):
        indata = ctypes.c_void_p(img[0].ctypes.data)
        inerrors = ctypes.c_void_p(img[1].ctypes.data)
        rowcount = img[2]['NAXIS1']
        colcount = img[2]['NAXIS2']
    
        par = []
        for p in pars:
            par.append(p)
        pointer = (len(par) * ctypes.c_double)(*par)
        
        print "Pointer:", pointer
        
        chi2value = chi2_cfun(indata, inerrors, rowcount, colcount, pointer)
        return chi2value
 
    y_code = types.CodeType(args,
                chi2.func_code.co_nlocals,
                chi2.func_code.co_stacksize,
                chi2.func_code.co_flags,
                chi2.func_code.co_code,
                chi2.func_code.co_consts,
                chi2.func_code.co_names,
                co_varnames,
                chi2.func_code.co_filename,
                name,
                chi2.func_code.co_firstlineno,
                chi2.func_code.co_lnotab)
 
    return types.FunctionType(y_code, chi2.func_globals, name)

def stars(img, n = 1):
    if n == 1:
        return lambda bg, bgx, bgx2, bgy, bgy2, bgxy, a, mux, muy, sigx, sigy, angle: chi2(img, bg, bgx, bgx2, bgy, bgy2, bgxy, a, mux, muy, sigx, sigy, angle)
    if n == 2:
        return lambda bg, bgx, bgx2, bgy, bgy2, bgxy, a1, mux1, muy1, sigx1, sigy1, angle1, a2, mux2, muy2, sigx2, sigy2, angle2: chi2(img, bg, bgx, bgx2, bgy, bgy2, bgxy, a1, mux1, muy1, sigx1, sigy1, angle1, a2, mux2, muy2, sigx2, sigy2, angle2)
    if n == 3:
        return lambda bg, bgx, bgx2, bgy, bgy2, bgxy, a1, mux1, muy1, sigx1, sigy1, angle1, a2, mux2, muy2, sigx2, sigy2, angle2, a3, mux3, muy3, sigx3, sigy3, angle3: chi2(img, bg, bgx, bgx2, bgy, bgy2, bgxy, a1, mux1, muy1, sigx1, sigy1, angle1, a2, mux2, muy2, sigx2, sigy2, angle2, a3, mux3, muy3, sigx3, sigy3, angle3)
     

def fitStar(img, res = 7):
    data = img[0]
    errors = img[1]
    initA, initbg, initsigx, initsigy, initmux, initmuy = moments(data)
    initsigx, initsigy = 4,4
    s = 1
    f = stars(img, s)
    m = minuit.Minuit(f, bg = initbg, a = initA,
                      mux = initmux, muy = initmuy, sigx = initsigx,
                      sigy = initsigy, limit_angle = (-90,90))
    m.fixed['bg'] = True
    m.fixed['bgx'] = True
    m.fixed['bgx2'] = True
    m.fixed['bgy'] = True
    m.fixed['bgy2'] = True
    m.fixed['bgxy'] = True
    
    print m.values
    
    m.printMode = 1
#     m.simplex()
    m.migrad()
    m.hesse()
    
    raw_input("Continue...")
    
    m_min = m
    chi2value = m_min.fval
    dof = img[2]['NAXIS1'] * img[2]['NAXIS2']
    while (chi2value > dof) and (s <= 3):
        s += 1
        f = stars(img, s)
        if s == 2:
            m = minuit.Minuit(f, bg = initbg, a1 = 2*initA/3,
                          mux1 = initmux, muy1 = initmuy, sigx1 = initsigx,
                          sigy1 = initsigy, limit_angle1 = (-90,90),
                          mux2 = initmux, muy2 = initmuy, sigx2 = initsigx,
                          sigy2 = initsigy, limit_angle2 = (-90,90))
        if s == 3:
            m = minuit.Minuit(f, bg = initbg, a1 = initA/3,
                          mux1 = initmux, muy1 = initmuy, sigx1 = initsigx,
                          sigy1 = initsigy, limit_angle1 = (-90,90),
                          mux2 = initmux, muy2 = initmuy, sigx2 = initsigx,
                          sigy2 = initsigy, limit_angle2 = (-90,90),
                          mux3 = initmux, muy3 = initmuy, sigx3 = initsigx,
                          sigy3 = initsigy, limit_angle3 = (-90,90))
            
        m.fixed['bg'] = True
        m.fixed['bgx'] = True
        m.fixed['bgx2'] = True
        m.fixed['bgy'] = True
        m.fixed['bgy2'] = True
        m.fixed['bgxy'] = True
            
        m.printMode = 1
#         m.simplex()
        m.migrad()
        m.hesse()    
        
        if m.fval < chi2value:
            m_min = m
            chi2value = m_min.fval
        
    if s == 3:
        print "Best of 3:", chi2value, "dof:", dof
        if chi2value > dof:
            print "Further analysis needed."
            
    img[2]['values'] = m.values
    img[2]['errors'] = m.errors
    img[2]['matrix'] = m.matrix(correlation=True)
    img[2]['covariance'] = m.covariance
    
    print moments(data)
    print m.values

def nStars(n, img, med = 400, xSize = 1540, ySize = 1020, res = 7):
    par = []
    
    par.append(med)
    par.append(0)
    par.append(0)
    par.append(0)
    par.append(0)
    par.append(0)
    
    for i in range(1, n+1):
        par.append(2.*med)
        par.append(xSize/2.)
        par.append(ySize/2.)
        par.append(res/2.)
        par.append(res/2.)
        par.append(0)
        
    m = minuit.Minuit(fake_fun(len(par), img))
#     for i in range(len(m.par)):
#         m.values[str(i)] = par[i]
#          
#     for i in range(1, n+1):
#         m.limits[str(6*i + 1)] = (0, xSize)
#         m.limits[str(6*i + 2)] = (0,ySize)
#         m.limits[str(6*i + 3)] = (0.3*res,res)
#         m.limits[str(6*i + 4)] = (0.3*res,res)
#         m.limits[str(6*i + 5)] = (-90,90)
#     
    return par, m

def simStarField(numStars, xSize = 1540, ySize = 1020, res = 7, gain = 2.3, bits = 16):
    par = []
    
#     Sets background level with border effects
    xOrder = floor(log10(xSize))
    yOrder = floor(log10(ySize))
    par.append(400)
    par.append(-xSize/(xOrder**10))
    par.append(1/xOrder**10)
    par.append(-ySize/(yOrder**10))    
    par.append(1/yOrder**10)
    par.append(0)
    
#     Set stars
    for i in range(numStars):
        x = xSize*rand()
        y = ySize*rand()
        sigX = 0.3*res + 0.7*res*rand()
        sigY = 0.3*res + 0.7*res*rand()
        angle = 180*rand()
        A = 2*pi*sigX*sigY * gain * (2**bits - 1) * rand()
        par.append(A)
        par.append(sigX)
        par.append(sigY)
        par.append(x)
        par.append(y)
        par.append(angle)
        
    print "Generating Star Field..."
    field = multiGaussian(*par)
    ind = indices(zeros((ySize, xSize)).shape)
    print "Calculating stars..."
    starField = field(ind[1], ind[0])
    print "Adding noise..."
#     noise = scipy.random.poisson(starField)
    
    print "Done!"
    clf()
    imshow(starField, cmap=cm.gist_stern)
    colorbar()
    
    return starField

# Define a transformation over coordinates

def trans(angle, a, b, c1, c2):
    T = matrix([[a*cos(angle), -a*sin(angle), c1], [b*sin(angle), b*cos(angle), c2], [0, 0, 1]])
    return lambda X: ( T * vstack((X.T, matrix(ones(len(X))) )) )[0:2].T

# Define a 1D Gaussian
def gauss(A, sig, mu):
    sig = float(sig)
    return lambda x: (A / (sig * sqrt(2*pi))) * exp(- (x - mu)**2 / (2 * sig**2))


# Rotate coordinates by an angle given in radians and add a constant
def transCoord(x, y, angle, c1=0, c2=0):
    x = c1 + x * cos(angle) - y * sin(angle)
    y = c2 + x * sin(angle) + y * cos(angle)
    return x, y


# Integrate a 2D gaussian over all the space
def intgaussian(A, sigx, sigy):
    i = scipy.integrate.dblquad(gaussian(A, 0, sigx, sigy, 0, 0, 0), -inf, inf, lambda x: -inf, lambda x: inf)
    return i


# Return the moments for the data set
def moments(data):
    total = numpy.abs(data).sum()
    Y, X = indices(data.shape)
    mux = numpy.abs(X*data).sum()/total
    muy = numpy.abs(Y*data).sum()/total
    #mux, muy = ndimage.measurements.center_of_mass(data)

    col = data[:, int(muy)]
    sigx = sqrt(abs((arange(col.size)-muy)**2*col).sum()/col.sum())
    row = data[int(mux), :]
    sigy = sqrt(abs((arange(row.size)-mux)**2*row).sum()/row.sum())
    bg = median(data.ravel())
    A = (data.max() - bg)*20*pi # Subtract bg and normalize with sigmas
    return A, bg, sigx, sigy, mux, muy

# Fit a 2D gaussian to the data
def fitg(data, sigdata = None):
    if sigdata is None:
        sigdata = sqrt(data)
    initA, initbg, initsigx, initsigy, initmux, initmuy = moments(data)
    chi2 = lambda A, bg, sigx, sigy, mux, muy, angle: ((gaussian(A, bg, sigx, sigy, mux, muy, angle)(*indices(data.shape)) - data)**2 / sigdata**2).sum()
    success = True
    try:
        m = minuit.Minuit( chi2, A = initA, bg = initbg, sigx = initsigx, sigy = initsigy, mux = initmux, muy = initmuy, limit_angle = (-90,90))
        #m.scan(("sigx",10,1,10), ("sigy",10,1,10), output = False)
        m.fixed['mux'] = True
        m.fixed['muy'] = True
        m.fixed['bg'] = True
        m.printMode = 0
        m.migrad()
        #raw_input("Continue...")
        m.printMode = 0 # 0 for not printing steps and 1 for printing
        m.fixed['mux'] = False
        m.fixed['muy'] = False
        m.fixed['bg'] = False
        m.migrad()
        m.hesse()
    #print "Auto1"
    except:
        try:
            m = minuit.Minuit( chi2, A = initA, bg = initbg, sigx = initsigx, sigy = initsigy, mux = initmux, muy = initmuy, limit_angle = (-90,90))
            m.printMode = 0
            m.migrad()
            m.hesse()
            #print "Auto2"
        except:
            success = False
    finally:
        return success, m


# Adjust parameters for a coordinates transformation
def fitt(coords, centres, errors, frame):
    sigcentres = array(errors[frame])
    points = matrix(centres[frame])
    chi2 = lambda angle, a, b, c1, c2: (array(trans(angle, a, b, c1, c2)(points) - coords)**2 / sigcentres**2).sum()
    m = minuit.Minuit(chi2, a = 1, b = 1)
    m.printMode = 0 # 0 for not printing steps and 1 for printing
    m.migrad()
    m.hesse()
    return m
    

#ravel(gaussian(*p)(*indices(data.shape)) - data)


def fit1g(h):
    x = sum(h[1][1:]*h[0])/sum(h[0])
    sig = sqrt(abs(sum((h[1][1:]-x)**2*h[0])/sum(h[0])))
    sigdata = sqrt(h[0])
    for i in range(len(sigdata)):
        if sigdata[i] < 0.0001: sigdata[i] = 0.0001
    chi2 = lambda A, sigma, mu: ((gauss(A, sigma, mu)(h[1][1:]) - h[0])**2 / sigdata**2).sum()
    m = minuit.Minuit(chi2, A = h[0].max() * sig, mu = h[1][h[0].argmax()], sigma = sig)
    m.printMode = 1
    m.migrad()
    m.hesse()
    return m



# Python Lessons! Analagous for 2D!
def proj(data):
    xarray = indices(data.sum(axis=0).shape)
    xarray = xarray[0]
    yarray = data.sum(axis=0)
    #print shape(xarray)
    #print shape(yarray)
    sigy = sqrt(yarray)
    #plot(xarray, yarray)
    errorbar(xarray,yarray,sigy)
    
    chi2 = lambda bg, a, mu, sigma: ((yarray - bg - a*exp(-(xarray - mu)**2 / sigma**2))**2/sigy**2).sum()
    print chi2(16000, 35000, 25, 5)
    
    m = minuit.Minuit(chi2, bg = 16000, a = 35000, mu = 25, sigma=10)
    m.printMode = 1
    m.migrad()
    m.hesse()
    print m.values, m.errors
    
    bg = m.values['bg']
    a = m.values['a']
    mu = m.values['mu']
    sigma = m.values['sigma']
    
    yfit = bg + a*exp(-(xarray-mu)**2 / sigma**2)
    plot(yfit)

    return m

