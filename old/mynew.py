
''' To Do List '''
# QTextbox (curses)
# onidle idle_event

# check for valid signatures on fit params

# Implement dark exp time check (to agree with light/correct for light)
# Implement force mode on dark sub
# Implement a nice way to keep plots with desired axis (during rezise)


''' BUG List '''


# Enable backends, LeTeX in plots and sets interpolation method
import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4']='PySide'
matplotlib.rc('text', usetex=True)
matplotlib.rc('image', interpolation='none')

# Import PySide Qt elements
import PySide
from PySide.QtGui import *
from PySide.QtCore import *
# import pyqtgraph as pg

# Import pylab and sys
from pylab import *
import sys

# Enable figures and backends for Qt
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib import gridspec

# Analysis code
from lib.PhotometryAnalysis import *


# PyQtGraph
import pyqtgraph as pg

# Main program
class Photometry(QMainWindow):
    def __init__(self):
        super(Photometry,self).__init__()
        
        # Fit Files
        self.f = None
        self.d = None # Dark frame
        self.r = None # Reduced image
        self.clusterImg = None
        
        self.mainData = zeros([1,1]) # Used for first plot
        self.mainClick = None # Set click position
        self.xData = self.mainData.sum(0)
        self.yData = self.mainData.sum(1)
        
        self.zoomSize = 40 # Size of zoom box
        
        # UI
        self.main_window()
        self.open_ui()
        self.clear_calculated()
        self.setWindowTitle('Photometry Application')
        
        
    # Reset status, check boxes, buttons, tabs, displays for new calculations
    def clear_calculated(self):
        self.calculated = {}
        self.calculated['light'] = False
        self.calculated['dark'] = False
        self.calculated['reduced'] = False
        self.calculated['clusterLight'] = False
        self.calculated['clusterReduced'] = False
        
        self.checkDark.setCheckState(Qt.Unchecked)
        self.checkCluster.setCheckState(Qt.Unchecked)
        self.checkToogle.setCheckState(Qt.Unchecked)
        
        self.btnCount.setEnabled(False)
        self.btnFitStars.setEnabled(False)
        
        self.spreadSheetTab.clearContents()
        self.spreadSheetTab.setRowCount(0)
        self.bottomBar.setTabEnabled(1,False)
        
        self.xPos.display(0)
        self.yPos.display(0)
        self.counter.display(0)
                
        if self.clusterImg:
            self.clusterImg.remove()
        self.clusterImg = None
        
        self.mainClick = None
        self.clearLines('main')
        self.clearLines('zoom')
        
        
    # Main window
    def main_window(self):
        # Define as a main Widget and with focus
        self.main_widget = QWidget(self) 
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)        
        
        # Create main figure
        self.mainFig = Figure()
        self.gs = gridspec.GridSpec(2, 2, width_ratios = [5,1], height_ratios = [3,1])
        
        # Main plot
        self.mainCanvas = FigureCanvas(self.mainFig)
        self.mainCanvas.ax = self.mainFig.add_subplot(self.gs[0])
        self.mainCanvas.ax.xaxis.tick_top()
        
        # x-projection plot
        self.xCanvas = FigureCanvas(self.mainFig)
        self.xCanvas.ax = self.mainFig.add_subplot(self.gs[2])
        self.xCanvas.ax.set_ylim(auto=True)
        #self.xCanvas.ax.get_xaxis().set_visible(False)

        # y-projection plot
        self.yCanvas = FigureCanvas(self.mainFig)
        self.yCanvas.ax = self.mainFig.add_subplot(self.gs[1])
        self.yCanvas.ax.invert_yaxis()
        #self.yCanvas.ax.get_yaxis().set_visible(False)
        self.yCanvas.ax.xaxis.tick_top()
        self.yCanvas.ax.yaxis.tick_right()
        
        # Zoom plot
        self.zCanvas = FigureCanvas(self.mainFig)
        self.zCanvas.ax = self.mainFig.add_subplot(self.gs[3])
        self.zCanvas.ax.get_xaxis().set_visible(False)
        self.zCanvas.ax.get_yaxis().set_visible(False)
        
        
    ''' ***** Set Layout ***** '''
    def open_ui(self):
        # Load required ui elements
        self.spreadSheet_ui()
        self.button_ui()
        self.checkbox_ui()
        self.sliders_ui()
        self.textbox_ui()
        self.menuBar_ui()
        self.statusBar()
        self.lcdNumber_ui()

        # Define main grid
        mainGrid = QGridLayout()
                
        # Available bars
        leftBar = QVBoxLayout()
        rightBar = QVBoxLayout()
        self.bottomBar = QTabWidget()
        self.bottomBar.setMaximumHeight(200)

        
        # Left bar stuffs
        
        
        # Bottom bar stuffs
        filesTab = QWidget()
        
        filesTab.form = QFormLayout()
        filesTab.setLayout(filesTab.form)
        
        filesTab.form.light = QHBoxLayout()
        filesTab.form.light.addWidget(self.typeLight)
        filesTab.form.light.addWidget(self.selectLight)
        
        filesTab.form.dark = QHBoxLayout()
        filesTab.form.dark.addWidget(self.typeDark)
        filesTab.form.dark.addWidget(self.selectDark)
        
        filesTab.form.addRow('Light Frame',filesTab.form.light)
        filesTab.form.addRow('Dark Frame',filesTab.form.dark)
        
        self.bottomBar.addTab(filesTab, 'Open Files')        
        self.bottomBar.addTab(self.spreadSheetTab, 'List of Stars')
        self.bottomBar.setTabEnabled(1,False)


        # Right bar stuffs
        rightBar.addWidget(self.checkDark)
        rightBar.addWidget(self.checkCluster)
        rightBar.addWidget(self.sliderClusterAlpha)
        countBar = QHBoxLayout()
        countBar.addWidget(self.btnCount)
        countBar.addWidget(self.counter)
        rightBar.addLayout(countBar)
        rightBar.addWidget(self.btnFitStars)
                
        rightBar.addStretch(1)
        
        rightBar.addWidget(self.checkToogle)
        
        rightBar.addStretch(1)

        bottomRight = QHBoxLayout()
        bR1 = QVBoxLayout()
        bR1.addWidget(self.xPosLabel)
        bR1.addWidget(self.xPos)
        bottomRight.addLayout(bR1)
        bR2 = QVBoxLayout()
        bR2.addWidget(self.yPosLabel)
        bR2.addWidget(self.yPos)
        bottomRight.addLayout(bR2)
        rightBar.addLayout(bottomRight)

        # Add bars to grid
        mainGrid.addLayout(leftBar,0,0)
        mainGrid.addWidget(self.mainCanvas,0,1)
        mainGrid.addLayout(rightBar,0,2)
        mainGrid.addWidget(self.bottomBar,1,1)
        mainGrid.addWidget(self.textBox,1,2)
        
        
        # Define mainGrid as default layout
        self.main_widget.setLayout(mainGrid)
        
        self.mainCanvas.mpl_connect('button_press_event', self.zoom)
        self.mainCanvas.mpl_connect('idle_event', self.on_idle)


    ''' SpreadSheet '''
    def spreadSheet_ui(self):            
        self.spreadSheetHeader = QHeaderView(Qt.Horizontal)
        self.spreadSheetHeader.setClickable(True)
        
        self.spreadSheetTab = QTableWidget()
        self.spreadSheetTab.setColumnCount(8)
        self.spreadSheetTab.labels = ['Star', 'Success', 'Amplitude', 'xPos', 'xSig', 'yPos', 'ySig', 'BG']
        self.spreadSheetTab.setHorizontalHeader(self.spreadSheetHeader)
        self.spreadSheetTab.setHorizontalHeaderLabels(self.spreadSheetTab.labels)
        self.spreadSheetTab.verticalHeader().setVisible(False)
        
        self.sortOrder = Qt.AscendingOrder
        self.spreadSheetHeader.sectionClicked.connect(self.sortSpreadSheet)
        
        self.spreadSheetTab.cellClicked.connect(self.selectStar)
        
        self.spreadSheetTab.cellChanged.connect(self.successChange)
        
    def sortSpreadSheet(self, index):
        self.spreadSheetTab.sortByColumn(index, self.sortOrder)
        if self.sortOrder == Qt.AscendingOrder:
            self.sortOrder = Qt.DescendingOrder
        else:
            self.sortOrder = Qt.AscendingOrder
            
    def selectStar(self, row, col):
        x = self.spreadSheetTab.item(row, 3).data(Qt.DisplayRole)
        y = self.spreadSheetTab.item(row, 5).data(Qt.DisplayRole)
        
        self.clearLines('main')
        self.clearLines('zoom')
        
        self.zCanvas.ax.set_xlim([x-self.zoomSize/2, x+self.zoomSize/2])
        self.zCanvas.ax.set_ylim([y+self.zoomSize/2, y-self.zoomSize/2])
        self.zImg.autoscale()
        
        self.mainClick = [x,y]
        self.xData = self.mainData[self.mainClick[1],:]
        self.yData = self.mainData[:,self.mainClick[0]]
        
        self.mainLineV = self.mainCanvas.ax.axvline(x)
        self.mainLineH = self.mainCanvas.ax.axhline(y)
        
        self.zLineV = self.zCanvas.ax.axvline(x)
        self.zLineH = self.zCanvas.ax.axhline(y)
        
        self.xPos.display(round(x))
        self.yPos.display(round(y))
        
        self.checkToogle.setEnabled(True)
        self.checkToogle.setChecked(True)
        self.checkToogle.setStatusTip('Toogle projections between main image and zoomed image.')
        
        # Redraw Canvas
        self.redraw()
        
    def successChange(self, text):
        row, col = self.spreadSheetTab.currentRow(), self.spreadSheetTab.currentColumn()
        if col == 1:
            comboBox = self.spreadSheetTab.cellWidget(row, col)
            print comboBox.currentText()
            if comboBox.currentText() == 'Try 2 Stars':
                pass
            if comboBox.currentText() == 'Ignore Star':
                reply = QMessageBox.question(self, 'Ignore Star', "Are you you want to ignore this star?\nThis will also remove the star from this table!", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
                if reply == QMessageBox.Yes:
                    self.spreadSheetTab.removeRow(row)

    ''' Buttons for the ui'''
    def button_ui(self):
        # Open light button
        self.selectLight = QPushButton('Choose', self)
        self.selectLight.clicked.connect(lambda target='light': self.loadFile(target))
        self.selectLight.setStatusTip('Open a new Light file')
        
        # Open dark button
        self.selectDark = QPushButton('Choose', self)
        self.selectDark.clicked.connect(lambda target='dark': self.loadFile(target))
        self.selectDark.setStatusTip('Open a new Dark file')
        
        self.btnCount = QPushButton('Count Stars')
        self.btnCount.clicked.connect(self.count)
        self.btnCount.setEnabled(False)
        self.btnCount.setStatusTip('Please load a Light Frame first')
        
        self.btnFitStars = QPushButton('Fit Stars')
        self.btnFitStars.clicked.connect(self.fitStars)
        self.btnFitStars.setEnabled(False)
        self.btnFitStars.setStatusTip('Please load a Light Frame first')
        
    
    
    '''Checkboxes on the ui'''
    def checkbox_ui(self):
        self.checkDark = QCheckBox('Reduce Dark')
        self.checkDark.clicked.connect(lambda target='dark': self.recalculate(target))
        self.checkDark.setEnabled(False)
        self.checkDark.setStatusTip('Please load a Dark Frame first.')
        
        self.checkCluster = QCheckBox('Find pixel clusters')
        self.checkCluster.clicked.connect(lambda target='cluster': self.calculateCluster(target))
        self.checkCluster.setEnabled(False)
        self.checkCluster.setStatusTip('Please load a Light Frame first.')
        
        self.checkToogle = QCheckBox('Toogle projections')
        self.checkToogle.clicked.connect(lambda target='toogled': self.redraw(target))
        self.checkToogle.setEnabled(False)
        self.checkToogle.setStatusTip('Please load a Light Frame first.')

    
    ''' Sliders on the ui'''
    def sliders_ui(self):
        self.sliderClusterAlpha = QSlider(Qt.Horizontal)
        self.sliderClusterAlpha.setRange(0,100)
        self.sliderClusterAlpha.setValue(50)
        self.sliderClusterAlpha.sliderReleased.connect(lambda target='cluster': self.redraw(target))
        self.sliderClusterAlpha.setEnabled(False)
        self.sliderClusterAlpha.setStatusTip('Please load a Light Frame first')
        self.sliderClusterAlpha.setMaximumWidth(200)
    
    
    '''Textboxes and labels on the ui'''
    def textbox_ui(self):
        # Text box with updates
        self.textBox = QTextEdit('<b>Welcome</b> to this Photometry application.<br><br><b>Please</b> open a fits file to start.')
        self.textBox.setReadOnly(True)
        self.textBox.setMaximumSize(300,200)
        
        # Text box to open files
        self.typeLight = QLineEdit('/path/to/light.fit')
        self.typeLight.setStatusTip('Open path to Light Frame')
        self.typeLight.returnPressed.connect(lambda target='light', choose = False: self.loadFile(target, choose))
        self.typeLight.setMinimumWidth(200)
        
        self.typeDark = QLineEdit('/path/to/dark.fit')
        self.typeDark.setStatusTip('Open path to Dark Frame')
        self.typeDark.returnPressed.connect(lambda target='dark', choose = False: self.loadFile(target, choose))
        self.typeDark.setMinimumWidth(200)
        
        self.xPosLabel = QLabel()
        self.xPosLabel.setText('X Position')
        self.xPosLabel.setAlignment(Qt.AlignCenter)

        self.yPosLabel = QLabel()
        self.yPosLabel.setText('Y Position')
        self.yPosLabel.setAlignment(Qt.AlignCenter)


    ''' LCD Number on the ui '''
    def lcdNumber_ui(self):
        self.xPos = QLCDNumber()
        self.xPos.setStatusTip('Display X Position on the main image')
        self.xPos.setPalette(Qt.darkGray)
        
        self.yPos = QLCDNumber()
        self.yPos.setStatusTip('Display Y Position on the main image')
        self.yPos.setPalette(Qt.darkGray)
        
        self.counter = QLCDNumber()
        self.counter.setStatusTip('Number of stars')
        self.counter.setPalette(Qt.darkGray)
    
    '''Menu Bar'''
    def menuBar_ui(self):
        self.fileMenu = self.menuBar().addMenu('&File')
        
        openFile = QAction('&Open file', self)
        openFile.setShortcut('Ctrl+O')
        openFile.triggered.connect(self.loadFile)
        
        quitAction = QAction('&Quit', self)
        quitAction.setShortcut('Ctrl+Q')
        quitAction.triggered.connect(self.close)
        
        self.fileMenu.addAction(openFile)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(quitAction)
            
    def count(self):
        self.btnCount.setText('Counting...')
        self.textBox.append('<br>Counting start... Please wait!')
        if self.checkDark.isChecked():
            if not 'countedR' in self.calculated:
                self.calculated['countedR'] = countStars(self.clusterDataR)
           
            self.spreadSheetTab.setRowCount(self.calculated['countedR'])
            self.counter.display(self.calculated['countedR'])
            self.textBox.append('<b>Found</b> '+str(self.calculated['countedR'])+' stars.')
        else:
            if not 'countedF' in self.calculated:
                self.calculated['countedF'] = countStars(self.clusterDataF)
           
            self.spreadSheetTab.setRowCount(self.calculated['countedF'])
            self.counter.display(self.calculated['countedF'])
            self.textBox.append('<b>Found</b> '+str(self.calculated['countedF'])+' stars.')

        self.bottomBar.setTabEnabled(1,True)
        self.btnFitStars.setEnabled(True)
        self.btnFitStars.setStatusTip('Try to fit parameters of a 2D Gaussian to the stars.')
        self.btnCount.setText('Recount stars')
        self.textBox.moveCursor(QTextCursor.End)
        self.textBox.ensureCursorVisible()
        
        
    def fitStars(self):
        if self.checkDark.isChecked():
            numStars = self.calculated['countedR']
            signal = self.r
            binMatrix  = self.clusterDataR.data
        else:
            numStars = self.calculated['countedF']
            signal = self.f
            binMatrix  = self.clusterDataF.data
            
        self.stars = []
        for i in range(numStars):
            flag = i + 101
            print i
            result, m = starParam(signal, binMatrix, flag)
            self.stars.append(m)
            
            item = range(self.spreadSheetTab.columnCount())
            for j in item:
                item[j] = QTableWidgetItem()
            
            item[0].setData(Qt.DisplayRole, i)
            item[1] = QTableWidgetItem(result)
            item[2].setData(Qt.DisplayRole, round(m.values['A']))
            item[3].setData(Qt.DisplayRole, round(m.values['mux']))
            item[4].setData(Qt.DisplayRole, round(100*m.values['sigx'])/100)
            item[5].setData(Qt.DisplayRole, round(m.values['muy']))
            item[6].setData(Qt.DisplayRole, round(100*m.values['sigy'])/100)
            item[7].setData(Qt.DisplayRole, round(m.values['bg']))
            
            for col in range(self.spreadSheetTab.columnCount()):
                if col == 1:
                    comboBox = QComboBox()
                    comboBox.addItem(result)
                    comboBox.addItem('Try 2 Stars')
                    comboBox.addItem('Ignore Star')
                    comboBox.currentIndexChanged.connect(self.successChange)
                    self.spreadSheetTab.setCellWidget(i, col, comboBox)
                else:
                    item[col].setFlags(33)
                    self.spreadSheetTab.setItem(i, col, item[col])
        
        
                
    def loadFile(self, target, choose = True, update=False):
        if choose:
            filename, filetype = QFileDialog.getOpenFileName(self, 'Open file', '.', 'FIT files (*.fits; *.fit; *.mfit);;All files (*.*)')    
        
        try:   
            if target == 'light':
                self.clear_calculated()
                if choose:
                    self.typeLight.setText(filename)
                filename = self.typeLight.text()
                self.f = openFits(filename)
                self.calculated['light'] = True
                
                self.checkCluster.setEnabled(True)
                self.checkCluster.setStatusTip('Identify pixel clusters (stars)')
                self.checkToogle.setStatusTip('Please zoom into a star first')
                self.btnCount.setStatusTip('Please find the pixel clusters first')
                self.btnFitStars.setStatusTip('Please find the pixel clusters first')


            
            if target == 'dark':
                if choose:
                    self.typeDark.setText(filename)
                filename = self.typeDark.text()
                self.d = openFits(filename)
                self.calculated['dark'] = True

                self.checkDark.setEnabled(True)
                self.checkDark.setStatusTip('Reduce Dark from Light Frame')
            
        except Exception, e:
            print e
            self.textBox.append('<br><b>File</b> not opened.')
            if len(filename) > 0:
                QMessageBox.warning(self, 'Error', 'Sorry, I am not sure if you selected a valid file... Please try to load the file again.')
                self.textBox.append('<b>'+filename.split('/')[-1]+'</b> might not be a valid fit file')
            
            if target == 'light':
                self.typeLight.setText('/path/to/light.fit')
            if target == 'dark':
                self.typeDark.setText('/path/to/dark.fit')
            
        else:
            self.setStatusTip('File successfully opened')
            self.textBox.append('<br><b>'+filename.split('/')[-1]+'</b> successfully opened.')
            
            if target == 'light':
                self.recalculate(target)
            
        
    def recalculate(self, target=None):  
        if self.checkDark.isChecked():
            if not self.calculated['reduced']:
                self.textBox.append('<br><b>Subtracting<b> dark.')
                self.r = subFits(self.f, self.d, mode='force') # Using force mode, implement into GUI
                self.calculated['reduced'] = True
            self.mainData = self.r[0].data
            if target == 'dark':
                self.textBox.append('<br><b>Dark</b> reduced.')
            
            if self.checkCluster.isChecked():
                if self.calculated['clusterReduced']:
                    self.clusterImg.set_data(self.clusterDataR.data)
                else:
                    self.calculateCluster(target)
            
        else:    
            self.mainData = self.f[0].data
            if target == 'dark':
                self.textBox.append('<br><b>Dark</b> ignored.')
            
            if self.checkCluster.isChecked():
                if self.calculated['clusterLight']:
                    self.clusterImg.set_data(self.clusterDataF.data)
                else:
                    self.calculateCluster(target)
                         
        if target == 'light':
            self.xData = self.mainData.sum(0)
            self.yData = self.mainData.sum(1)
        elif self.mainClick:
            self.xData = self.mainData[self.mainClick[1],:]
            self.yData = self.mainData[:,self.mainClick[0]]
                    
        self.redraw(target)
    
    def calculateCluster(self, target=None):
        if self.checkCluster.isChecked():
            self.sliderClusterAlpha.setEnabled(True)
            self.btnCount.setEnabled(True)
            self.btnCount.setStatusTip('Count the number of stars in the frame')
            self.btnFitStars.setStatusTip('Please count the number os stars first')
            self.textBox.append('<br><b>Calculating</b> cluster mask.')
        else:
            self.sliderClusterAlpha.setEnabled(False)
            self.clusterImg.set_alpha(0)
            self.btnCount.setEnabled(False)
            self.btnCount.setStatusTip('Please find the pixel clusters first')
            self.btnFitStars.setEnabled(False)
            self.btnFitStars.setStatusTip('Please count the number os stars first')
            self.textBox.append('<br>Cluster mask <b>disabled</b>.')

        if self.checkDark.isChecked():
            if not self.calculated['clusterReduced']:
                self.clusterDataR = binMatrix(self.r)
                self.calculated['clusterReduced'] = True
                    
            if self.clusterImg is None:
                self.clusterImg = self.mainCanvas.ax.imshow(self.clusterDataR.data)

        else:
            if not self.calculated['clusterLight']:
                self.clusterDataF = binMatrix(self.f)
                self.calculated['clusterLight'] = True
                
            if self.clusterImg is None:
                self.clusterImg = self.mainCanvas.ax.imshow(self.clusterDataF.data)
            
        self.recalculate(target)


    def zoom(self, event):
        if event.inaxes is self.mainCanvas.ax or self.zCanvas.ax:
            self.mainClick = [round(event.xdata),round(event.ydata)]

        # IMPLEMENT THIS
        #if event.inaxes is self.xCanvas.ax:
        #    if self.checkToogle.isChecked():
        #        print "x"
        #        self.mainClick[1] = round(event.ydata)
        #    else:
        #        self.mainClick[1] = round(event.xdata)
        #if event.inaxes is self.yCanvas.ax:
        #    print "y"
        #    if self.checkToogle.isChecked():
        #        self.mainClick[0] = round(event.xdata)
        #    else:
        #        self.mainClick[0] = round(event.ydata)
            
        self.clearLines('main')
        self.clearLines('zoom')
        
        self.zCanvas.ax.set_xlim([self.mainClick[0]-self.zoomSize/2, self.mainClick[0]+self.zoomSize/2])
        self.zCanvas.ax.set_ylim([self.mainClick[1]+self.zoomSize/2, self.mainClick[1]-self.zoomSize/2])
        self.zImg.autoscale()
        
        self.xData = self.mainData[self.mainClick[1],:]
        self.yData = self.mainData[:,self.mainClick[0]]
        
        self.mainLineV = self.mainCanvas.ax.axvline(self.mainClick[0])
        self.mainLineH = self.mainCanvas.ax.axhline(self.mainClick[1])
        
        self.zLineV = self.zCanvas.ax.axvline(self.mainClick[0])
        self.zLineH = self.zCanvas.ax.axhline(self.mainClick[1])
        
        self.xPos.display(self.mainClick[0])
        self.yPos.display(self.mainClick[1])
        
        self.checkToogle.setEnabled(True)
        self.checkToogle.setStatusTip('Toogle projections between main image and zoomed image.')
        
        # Redraw Canvas
        self.redraw()
    

    def clearLines(self, target):
        if target == 'main':
            try:
                self.mainLineV.remove()
                self.mainLineH.remove()
            except: pass  
                  
        if target == 'zoom':
            try:
                self.zLineV.remove()
                self.zLineH.remove()
            except: pass
    
    def on_idle(self, event):
        print "idle..." 
        self.xPos.display(round(event.xdata))
        self.yPos.display(round(event.ydata))
    
    
    def redraw(self, target=None):
        if target == 'light':            
            self.mainCanvas.ax.cla()
            self.mainImg = self.mainCanvas.ax.imshow(self.mainData, cmap=cm.gist_stern)
            
            self.xCanvas.ax.cla()
            self.xImg, = self.xCanvas.ax.plot(self.xData)
            self.xCanvas.ax.yaxis.set_major_locator(MaxNLocator(3))
            
            self.yCanvas.ax.cla()
            self.yImg, = self.yCanvas.ax.plot(self.yData,arange(len(self.yData)))
            self.yCanvas.ax.xaxis.set_major_locator(MaxNLocator(3))
            
            self.zImg = self.zCanvas.ax.imshow(self.mainData, cmap=cm.gist_stern)
            self.zCanvas.ax.set_xlim([0,self.zoomSize])
            self.zCanvas.ax.set_ylim([self.zoomSize,0])   
            
            self.mainCanvas.draw()
            
            mainPos = self.mainCanvas.ax.get_position().bounds
            xPos = self.xCanvas.ax.get_position().bounds
            newPos = (mainPos[0],xPos[1],mainPos[2],xPos[3])
            self.xCanvas.ax.set_position(newPos)    

        if target == 'cluster' and self.checkCluster.isChecked():
            self.clusterImg.set_alpha(self.sliderClusterAlpha.value()/100.)
            self.textBox.append('<br>Transparency set to <b>'+str(self.sliderClusterAlpha.value())+'%</b>.')
                 
        self.mainImg.set_data(self.mainData)
        self.mainImg.autoscale()
        
        if self.checkToogle.isChecked():
            if not self.xCanvas.ax.xaxis_inverted():
                self.xCanvas.ax.invert_xaxis()
            if not self.xCanvas.ax.yaxis_inverted():
                self.xCanvas.ax.invert_yaxis()

                
            self.xImg.set_ydata(arange(len(self.yData)))
            self.xImg.set_xdata(self.yData)
            self.xCanvas.ax.relim()
            self.xCanvas.ax.autoscale(tight=True)
            self.xCanvas.ax.yaxis.tick_right()
            self.xCanvas.ax.set_xlim(None,0)
            self.xCanvas.ax.set_ylim(self.mainClick[1]+self.zoomSize/2, self.mainClick[1]-self.zoomSize/2)

            if self.yCanvas.ax.yaxis_inverted():
                self.yCanvas.ax.invert_yaxis()
            
            self.yImg.set_ydata(self.xData)
            self.yImg.set_xdata(arange(len(self.xData)))
            self.yCanvas.ax.relim()
            self.yCanvas.ax.autoscale(tight=True)
            self.yCanvas.ax.xaxis.tick_bottom()
            self.yCanvas.ax.set_ylim(0)
            self.yCanvas.ax.set_xlim(self.mainClick[0]-self.zoomSize/2, self.mainClick[0]+self.zoomSize/2)

            if target == 'toogled':
                self.textBox.append('<br><b>Projections</b> toogled to zoomed area</b>')
        
        else:
            if self.xCanvas.ax.xaxis_inverted():
                self.xCanvas.ax.invert_xaxis()
            if self.xCanvas.ax.yaxis_inverted():
                self.xCanvas.ax.invert_yaxis()
                
            self.xImg.set_ydata(self.xData)
            self.xImg.set_xdata(arange(len(self.xData)))
            self.xCanvas.ax.relim()
            self.xCanvas.ax.autoscale(tight=True)
            self.xCanvas.ax.yaxis.tick_left()
            self.xCanvas.ax.set_ylim(0)
    
            self.yImg.set_xdata(self.yData)
            self.yImg.set_ydata(arange(len(self.yData)))
            self.yCanvas.ax.relim()
            self.yCanvas.ax.autoscale(tight=True)
            self.yCanvas.ax.xaxis.tick_top()
            self.yCanvas.ax.set_xlim(0)
            
            if not self.yCanvas.ax.yaxis_inverted():
                self.yCanvas.ax.invert_yaxis()
            
            if target == 'toogled':    
                self.textBox.append('<br><b>Projections</b> toogled to main image</b>')

        
        self.zImg.set_data(self.mainData)
        self.zImg.autoscale()
        
        
        self.mainCanvas.draw()
        
        # Ensures text box displays new text
        self.textBox.moveCursor(QTextCursor.End)
        self.textBox.ensureCursorVisible()


def main():
    app = QApplication.instance() # checks if QApplication already exists 
    if not app: # create QApplication if it doesnt exist 
        app = QApplication(sys.argv)
    window = Photometry()
    window.show()
    sys.exit(app.exec_())
    
    
if __name__ == '__main__':
    main()