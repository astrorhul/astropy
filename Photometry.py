''' To Do List '''
# onidle idle_event

# check for valid signatures on fit params

# Implement dark exp time check (to agree with light/correct for light)
# Implement force mode on dark sub


''' BUG List '''


# Import PySide Qt elements
import PySide
from PySide.QtGui import *
from PySide.QtCore import *
import pyqtgraph as pg

# Import pylab and sys
from pylab import *
import sys
import pyfits


# Analysis code
# from lib.PhotometryAnalysis import *


# PyQtGraph
import pyqtgraph as pg


# Image Holder
class images:
    ''' Image Library '''
    def __init__(self):
        self.light  = {}
        self.dark   = {}
        self.flat   = {}

        self.sub    = {}
        self.div    = {}
        self.final  = {}

        self.clusters = {}

    def __call__(self, type = None, key = None):
        ''' Check for key in available types
            True if present
            False if not '''
        if type:
            if key:
                return key in eval('self.'+type)
            else:
                return eval('self.'+type)
        else:
            r = {}
            r['light']      = key in self.light
            r['dark']       = key in self.dark
            r['flat']       = key in self.flat
            r['sub']        = key in self.sub
            r['div']        = key in self.div
            r['final']      = key in self.final
            r['clusters']   = key in self.clusters
            return r

    def returnNoDarkFlat(self):
        return ['light', 'sub', 'div', 'final', 'clusters']

    def setImage(self, fileName, type, key):
        ''' Set images of a give type to a give key '''
        eval('self.'+type)[key] = self.openFits(fileName)

    def removeImage(self, type, key = False):
        ''' Remove key image from a given type, 
            if key == False, clear the type '''
        if key:
            eval('self.'+type).pop(key, None)
        else:
            eval('self.'+type).clear()

    def renameImage(self, type, old, new):
        if self(type, old):
            eval('self.'+type)[new] = eval('self.'+type).pop(old)

    def openFits(self, fileName):
        ''' Open FITS file and return a list with '''
        f = pyfits.open(fileName, ignore_missing_end=True, mode = 'copyonwrite')
        data = [float64(f[0].header['EGAIN'] * f[0].data),                  # PhotoElectron Value
                sqrt( float64( f[0].header['EGAIN'] * f[0].data.copy() ) ), # Error
                f[0].header]                                                # FITS Header
        return data

    def subFits(self, key, darkKey, typ='Subtracted Frames', force = False, adjust = False):
        ''' Subtract dark frame from light one '''
        if self('light', key) and self('dark', darkKey):                # Check if both (light and dark) are loaded
            factor = self.light[key][2]['EXPTIME'] / self.dark[darkKey][2]['EXPTIME']
            if factor != 1:
                if adjust:
                    self.dark[key][0] = factor * self.dark[darkKey][0]
                    self.dark[key][1] = factor * self.dark[darkKey][1]
                else:
                    warning = 'Warning: Dark and Light frames have different exposure times!\nDo you wish to adjust this dark?'
                    return 'adjust', warning
            
            n = size(where(self.light[key][0] < self.dark[darkKey][0])) # Check if any value is going negative
            if n != 0:
                if force:
                    print 'Warning: ', n, ' values are going negative! Forcing...'
                
                else:
                    warning = 'Warning: {} values are going negative!\nDo you wish to force subtract?'.format(n)
                    return 'force', warning
        else:
            print 'Please, load both light and dark frames first.'
            return 0, 'error'

        ''' Subtract and calculate the errors '''
        temp = [None]*3
        temp[0] = self.light[key][0] - self.dark[darkKey][0]
        temp[1] = sqrt(self.light[key][0] + self.dark[darkKey][0])
        temp[2] = self.light[key][2]
        temp[2]['IMAGETYP'] = typ
        self.sub[key] = temp
        
        return 0, None
            
    def divFits(self, key, flatKey, typ='Divided Frames'):
        ''' Divide flat frames from image '''
        if self('light', key) and self('flat', flatKey):    # Check if both (light and flat) are loaded
            n = size(where(self.flat[flatKey][0] == 0))     # Check division by zero
            if n != 0:
                print 'Warning: ', n, ' points are trying to divide by ZERO!\nBad Flat!'
                return 1
            if self('sub', key):                            # Check if dark is subtracted
                useThis = self.sub[key]
            else:
                print 'No dark reduced image detected, ignoring dark...'
                useThis = self.light[key]
        else:
            print 'Please, load both light and flat frames first.'
            return 1
            
        ''' Divide and calculate the errors '''
        tempErr = sqrt( (useThis[1]/useThis[0])**2 + (self.flat[flatKey][1]/self.flat[flatKey][0])**2 )
        temp = [None]*3
        temp[0] = useThis[0] / self.flat[flatKey][0]
        temp[1] = useThis[0] * tempErr
        temp[2] = useThis[2]
        temp[2]['IMAGETYP'] = typ

        if self('sub', key):
            self.final[key] = temp
        else:
            self.div[key] = temp


class mainWindow(QMainWindow):
    def __init__(self):
        super(mainWindow, self).__init__()

        self.central = central()
        self.setCentralWidget(self.central)
        
        self.dock = dock(self.central.layout)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.dock)
        
        
    def resizeEvent(self, event):
        self.central.layout.resized()



class central(QWidget):
    def __init__(self):
        super(central, self).__init__()
        self.setFocus()
        self.layout = centralLayout()
        self.setLayout(self.layout)


class centralLayout(QGridLayout):
    def __init__(self):
        super(centralLayout, self).__init__()
        
        view = pg.GraphicsView()
        
        self.im = images()
                               
        self.gLayout = pg.GraphicsLayout()
        view.setCentralItem(self.gLayout)
        
        self.label = pg.LabelItem(justify='center')
        self.label.hide()
        self.gLayout.addItem(self.label, row = 1, col = 1)
        
        self.yPlot = self.gLayout.addPlot(row=0, col=1)
        self.yPlot.invertY()
        self.yPlot.setAutoVisible(x=True)
        self.yPlot.showGrid(x=True, y=True)
        
        self.xPlot = self.gLayout.addPlot(row = 1, col = 0)
        self.xPlot.setAutoVisible(y=True)
        self.xPlot.showGrid(x=True, y=True)
        
        self.mainHist = pg.HistogramLUTItem()
        self.mainHist.connected = False
        self.gLayout.addItem(self.mainHist, row = 0, col = 2, rowspan = 2)

        self.mainBox = myViewBox(self, lockAspect = True)
        self.mainBox.images = {}
        self.gLayout.addItem(self.mainBox, row = 0, col = 0)
        self.yPlot.setYLink(self.mainBox)
        self.xPlot.setXLink(self.mainBox)

        
        self.mainBox.autoRange()
        self.mainBox.scene().sigMouseMoved.connect(self.mainBox.mouseMoved)

        view.show()
        
        self.addWidget(view)
        
    def getChildren(self, imgData):
        for children in self.mainBox.addedItems:
            if children.data(0) == imgData:
                return children

    def loadImg(self, type, key, priority, alpha, show=True):
        image = flipud(rot90( self.im(type)[key][0] ))
        imgMedian = median(image)
        img = pg.ImageItem((image), levels = (imgMedian, 3*imgMedian), opacity = alpha/100.)
        img.setZValue(priority)
        
        imgData = type + ' .. ' + key
        img.setData(0, imgData)
        
        autoRange = False
        if not self.mainBox.addedItems:
            autoRange = True

        self.mainBox.addItem(img)
        if autoRange:
            self.mainBox.autoRange()
        self.mainBox.top.append([priority, show, img])
        self.mainBox.updateTop
        
        self.connectHist(imgData, img, imgMedian)
        
        self.setXYPlot(image)
        
        
    def setXYPlot(self, image):
        self.xPlot.clearPlots()
        self.yPlot.clearPlots()
        X, Y = image.shape
        x_i = max(0, int(self.mainBox.viewRange()[0][0]))
        x_f = min(X, int(self.mainBox.viewRange()[0][1]))
        y_i = max(0, int(self.mainBox.viewRange()[1][0]))
        y_f = min(Y, int(self.mainBox.viewRange()[1][1]))
        grid = image[x_i:x_f, y_i:y_f]
        self.xPlot.plot(range(x_i, x_f), grid.sum(1))
        self.yPlot.plot(grid.sum(0), range(y_i, y_f))


    def renameImg(self, type, oldName, newName):
        children = self.getChildren(type+' .. '+oldName)
        if children:
            print children.data(0)
            children.setData(0, type+' .. '+newName)
            print children.data(0)

    def showImg(self, imgData, visible):
        child = self.getChildren(imgData)
        child.setVisible(visible)
        [ children for children in self.mainBox.top if child is children[2] ][0][1] = visible
        
    def connectHist(self, imgData = None, img = None, imgMedian = None):
        if self.mainHist.connected:
            self.mainHist.sigLevelChangeFinished.disconnect()
            self.mainHist.connected = False
        
        if img is None:
            img = self.getChildren(imgData)
            levels = img.data(1)
            splited = imgData.split(' .. ')
            im = self.im(splited[0])[splited[1]][0]
            
            self.setXYPlot(im)
            
        self.mainHist.setImageItem(img)
        
        if imgMedian is None:
            if levels is not None:
                self.mainHist.setHistogramRange(0.95*levels[0], 1.05*levels[1])
                self.mainHist.setLevels(levels[0], levels[1])
        else:
            self.mainHist.setHistogramRange(0.95*imgMedian, 1.05*3*imgMedian)
            self.mainHist.setLevels(imgMedian, 3*imgMedian)
            
        self.mainHist.sigLevelChangeFinished.connect(lambda hist = self.mainHist, data = img: self.histChanged(hist, data))
        self.mainHist.connected = True

    def removeImg(self, imgData):
        img = self.getChildren(imgData)
        if img:
            self.mainBox.removeItem(img)
            item = [ children for children in self.mainBox.top if img is children[2] ][0]
            index = self.mainBox.top.index(item)
            self.mainBox.top.pop(index)
            self.mainBox.updateTop()

    def setAlpha(self, imgData, alpha):
        self.getChildren(imgData).setOpacity(alpha/100.)

    def setPriority(self, imgData, priority):
        child = self.getChildren(imgData)
        child.setZValue(priority)
        [ children for children in self.mainBox.top if child is children[2] ][0][0] = priority
        self.mainBox.updateTop()

    def resized(self):
        range = self.mainBox.viewRange()
        X, Y = range[0][1], range[1][1]
        relax = 0.05 * X    # Set size for between graphs
        proj  = 0.3  * Y    # Set size of projections
        width = X + relax + 2*proj
        
        totalSize = self.gLayout.layout.geometry()  # Get widget geometry
        
        # Scales dimensions based on viebox image and widget size
        scale = totalSize.width() / width
        
        newX = X * scale
        newY = Y * scale
        newRelax = relax * scale / 4
        newProj = proj * scale
        heighRelax = (totalSize.height() - (newY + newProj)) / 3
        
        # Set new geometries and define a preferred size
        self.gLayout.layout.itemAt(0, 0).setGeometry(newRelax, heighRelax, newX, newY)
        self.gLayout.layout.itemAt(0, 0).setPreferredSize(self.gLayout.layout.itemAt(0, 0).geometry().size())
        self.gLayout.layout.itemAt(0, 0).updateGeometry()
        self.gLayout.layout.itemAt(1, 0).setGeometry(newRelax, (newY + 2*heighRelax), newX, newProj)
        self.gLayout.layout.itemAt(1, 0).setPreferredSize(self.gLayout.layout.itemAt(1, 0).geometry().size())
        self.gLayout.layout.itemAt(1, 0).updateGeometry()
        self.gLayout.layout.itemAt(0, 1).setGeometry((newX + 2*newRelax), heighRelax, newProj, newY)
        self.gLayout.layout.itemAt(0, 1).setPreferredSize(self.gLayout.layout.itemAt(0, 1).geometry().size())
        self.gLayout.layout.itemAt(0, 1).updateGeometry()
#         self.gLayout.layout.itemAt(0, 2).setGeometry(totalSize.width()-120-newRelax, heighRelax, newProj, (totalSize.height()-2*heighRelax) )
#         self.gLayout.layout.itemAt(0, 2).setPreferredSize(self.gLayout.layout.itemAt(0, 2).geometry().size())
#         self.gLayout.layout.itemAt(0, 2).updateGeometry()
        
    def histChanged(self, hist, img):
        levels = hist.getLevels()
        img.setData(1, levels)        
        
class myViewBox(pg.ViewBox):
    def __init__(self, par, *args, **kwds):
        self.par = par
        
        pg.ViewBox.__init__(self, *args, **kwds)
        self.setMouseMode(self.RectMode)
        self.rbScaleBox.setZValue(100)
        self.invertY()

        self.top = []

        self.vLine = pg.InfiniteLine(angle=90, movable=False)
        self.vLine.setZValue(100)
        self.vLine.hide()
        self.hLine = pg.InfiniteLine(angle=0, movable=False)
        self.hLine.setZValue(100)
        self.vLine.hide()
        self.addItem(self.vLine, ignoreBounds=True)
        self.addItem(self.hLine, ignoreBounds=True)
        
        self.xLine = pg.InfiniteLine(angle=90, movable=False)
        self.xLine.hide()
        self.yLine = pg.InfiniteLine(angle=0, movable=False)
        self.yLine.hide()
        self.par.xPlot.addItem(self.xLine, ignoreBounds=True)
        self.par.yPlot.addItem(self.yLine, ignoreBounds=True)
        
    def updateTop(self):
        self.top.sort(key = lambda x: x[0])
        self.top.sort(key = lambda x: x[1])
        
    def getTopImg(self):
        if self.top:
            topName = self.top[-1][2].data(0)
            splited = topName.split(' .. ')
            type = splited[0].lower()
            self.image = flipud(rot90( self.par.im(type)[splited[1]][0] ))
            return True
        else:
            return False
    
    def mouseClickEvent(self, ev):
        if ev.button() == Qt.RightButton:
            self.autoRange()
            if self.getTopImg:
                self.par.setXYPlot(self.image)


        if ev.button() == Qt.LeftButton:
            self.mousePos = np.array([self.mapToView(ev.pos()).x(), self.mapToView(ev.pos()).y()])
            print self.mousePos
        
        
    def mouseMoved(self, ev):
#         Make show only inside image, make show pixel value (z) as well!
        pos = ev
        if self.sceneBoundingRect().contains(pos):
            mousePoint = self.mapSceneToView(pos)
            index = [int(mousePoint.x()), int(mousePoint.y())]
            if self.getTopImg():
                X, Y = self.image.shape
                if index[0] > 0 and index[1] > 0 and index[0] < X and index[1] < Y:
                    self.par.label.show()
                    pixelValue = self.image[index[0], index[1]]
                    self.par.label.setText("<span style='font-size: 14pt'><br><br> x = %3.0f <br> y = %4.0f <br> z = %4.0f<br>" % (mousePoint.x(), mousePoint.y(), pixelValue))
                else:
                    self.par.label.hide()
                    
            self.vLine.setPos(mousePoint.x())
            self.vLine.show()
            self.hLine.setPos(mousePoint.y())
            self.hLine.show()
            
            self.xLine.setPos(mousePoint.x())
            self.xLine.show()
            self.yLine.setPos(mousePoint.y())
            self.yLine.show()
        else:
            self.par.label.hide()
            self.vLine.hide()
            self.hLine.hide()
            self.xLine.hide()
            self.yLine.hide()


    def mouseDragEvent(self, ev):
        if ev.button() == Qt.RightButton:
            ev.ignore()
        else:
            self.vLine.hide()
            self.hLine.hide()
            self.xLine.hide()
            self.yLine.hide()
            pg.ViewBox.mouseDragEvent(self, ev)
            
        if ev.isFinish():
            self.par.xPlot.clearPlots()
            self.par.yPlot.clearPlots()
            if self.getTopImg():
                self.par.setXYPlot(self.image)


class myGroup(pg.parametertree.parameterTypes.GroupParameter):
    def __init__(self, parent, **opts):
        opts['type'] = 'group'
        opts['addText'] = "Load Image"
        opts['addList'] = ['Light Frame', 'Dark Frame', 'Flat Frame']
        
        self.par = parent
        
        pg.parametertree.parameterTypes.GroupParameter.__init__(self, **opts)
        
        self.sigTreeStateChanged.connect(self.treeChanged)
        self.alwaysForce = False
        self.alwaysAdjust = False
        
    def addNew(self, typ):
        if typ == 'Light Frame':   
            filename, filetype = QFileDialog.getOpenFileName(None, 'Open file', '.', 'FIT files (*.fits; *.fit; *.mfit);;All files (*.*)')
            if filename == '':
                return
            self.par.lightTree(typ, filename)
            
        if typ == 'Dark Frame':   
            filename, filetype = QFileDialog.getOpenFileName(None, 'Open file', '.', 'FIT files (*.fits; *.fit; *.mfit);;All files (*.*)')
            if filename == '':
                return
            self.par.darkTree(typ, filename)
            
        if typ == 'Flat Frame':   
            filename, filetype = QFileDialog.getOpenFileName(None, 'Open file', '.', 'FIT files (*.fits; *.fit; *.mfit);;All files (*.*)')
            if filename == '':
                return
            self.par.flatTree(typ, filename)        
        
    def __call__(self, typ, filename):
        splited = typ.split()
        i = len(self.childs)+1
        nm = str(splited[0]+' '+str(i))
        
        self.par.im.setImage(filename, splited[0].lower(), nm)

        if typ == 'Light Frame':
            darkVals = {}
            darkVals[' Select Dark'] = 0
            flatVals = {}
            flatVals[' Select Flat'] = 0
            for i in range(len(self.par.darkTree.children())):
                darkVals[self.par.darkTree.children()[i].name()] = i+1
            for i in range(len(self.par.flatTree.children())):
                flatVals[self.par.flatTree.children()[i].name()] = i+1
                
            newImg = self.addChild(dict(name=nm, type='group', autoIncrementName = True, removable=True, renamable=True))
            
            newImg.addChild(dict(name = 'Show', type = 'bool', value = True))
            newImg.addChild(dict(name = 'Set Levels', type = 'bool', value = True))
            newImg.addChild(dict(name = 'Alpha', type = 'int', value = '100', limits = (0,100), suffix = ' %'))
            newImg.addChild(dict(name = 'Priority', type = 'int', value = 0, limits = (0,99)))
            
            newImg.addChild(dict(name = 'File:', type = 'text', value = filename, readonly = True, expanded = False))
            newImg.param('File:').items.keys()[0].setExpanded(False)
            newImg.param('File:').items.keys()[0].widget.setEnabled(False)
            newImg.addChild(dict(name = 'Dark:', type = 'list', values = darkVals, value = 0))
            newImg.addChild(dict(name = 'Reduce Dark', type = 'bool', value = False, enabled = False))
            newImg.param('Reduce Dark').items.keys()[0].widget.setEnabled(False)
            newImg.addChild(dict(name = 'Flat:', type = 'list', values = flatVals, value = 0))
            newImg.addChild(dict(name = 'Divide Flat', type = 'bool', value = False, enabled = False))
            newImg.param('Divide Flat').items.keys()[0].widget.setEnabled(False)
                                    
        else:
            newImg = self.addChild(dict(name=nm, type='group', autoIncrementName = True, removable=True, renamable=True))
            nm = newImg.name()
            newImg.addChild(dict(name = 'File:', type = 'text', value = filename, readonly = True, expanded = False))
            newImg.param('File:').items.keys()[0].setExpanded(False)
            newImg.param('File:').items.keys()[0].widget.setEnabled(False)
           
            for children in self.par.lightTree:
                vals = children.param(splited[0]+':').opts['limits'].copy()
                i = 0
                while i in vals.values():
                    i += 1
                vals[nm] = i
                children.param(splited[0]+':').setLimits(vals)
                
    
        newImg.sigNameChanged.connect(self.nameChanged)


    def treeChanged(self, param, changes):
        tree = param

        for param, change, data in changes:
            path = self.childPath(param)    # path[0] = parent, [1] = child
            if path and tree.name() == 'Light Images' and change != 'childAdded':
                lightImgData = tree.name().split()[0].lower() + ' .. ' + path[0]
                if tree.param(path[0], 'Reduce Dark').opts['value'] and tree.param(path[0], 'Divide Flat').opts['value']:
                    imgData = 'final .. ' + path[0]
                elif tree.param(path[0], 'Reduce Dark').opts['value']:
                    imgData = 'sub .. ' + path[0]
                elif tree.param(path[0], 'Divide Flat').opts['value']:
                    imgData = 'div .. ' + path[0]
                else:
                    imgData = tree.name().split()[0].lower() + ' .. ' + path[0]
            elif path:
               imgData = tree.name().split()[0].lower() + ' .. ' + path[0]
            splited = param.name().split()
            
            
            if change == 'childRemoved' and param.name() != 'Light Images':
                type = splited[0]
                if type == 'Dark':
                    typeList = [type.lower(), 'sub', 'final']
                elif type == 'Flat':
                    typeList = [type.lower(), 'div', 'final']
                    
                for children in self.par.lightTree:
                    vals = children.param(type+':').opts['limits'].copy()
                    vals.pop(data.name())
                    children.param(type+':').setLimits(vals)
                    for t in typeList:
                        self.par.im.removeImage(t, data.name())
                        self.par.layout.removeImg(t + ' .. ' + data.name())
 
                self.par.im.removeImage(type.lower(), data.name())
                self.par.layout.removeImg(type.lower() + ' .. ' + data.name())
               
                
            if change == 'childRemoved' and param.name() == 'Light Images':
                type = splited[0].lower()
                self.par.layout.removeImg(type + ' .. ' + data.name())
                for type in self.par.im.returnNoDarkFlat():
                    self.par.im.removeImage(type, data.name())
              
                
            if change == 'childAdded' and param.name() == 'Light Images':
                type = splited[0].lower()
                nm = param.children()[-1].name()
                
                for children in self.par.lightTree:
                    if children.name() != nm:
                        with children.treeChangeBlocker():
                            children.param('Set Levels').setValue(False)
                            children.treeStateChanges = []
                
                self.par.layout.loadImg(type, nm, priority = 0, alpha = 100, show = True)
              
                
            if change == 'value' and param.name() == 'Dark:':
                tree.param(path[0], 'Reduce Dark').setToDefault()
                if data:
                    tree.param(path[0], 'Reduce Dark').items.keys()[0].widget.setEnabled(True)
                else:
                    tree.param(path[0], 'Reduce Dark').items.keys()[0].widget.setEnabled(False)


            if change == 'value' and param.name() == 'Flat:':
                tree.param(path[0], 'Divide Flat').setToDefault()
                if data:
                    tree.param(path[0], 'Divide Flat').items.keys()[0].widget.setEnabled(True)
                else:
                    tree.param(path[0], 'Divide Flat').items.keys()[0].widget.setEnabled(False)
                 
                    
            if change == 'value' and param.name() == 'Show':
                self.par.layout.showImg(imgData, data)    
                if data:
                    tree.param(path[0], 'Set Levels').setValue(True)  
            
            
            if change == 'value' and param.name() == 'Set Levels':
                if data:
                    self.par.layout.connectHist(imgData)
                for children in self.children():
                    if children.name() != path[0]:
                        with children.treeChangeBlocker():
                            children.param('Set Levels').setValue(False)
                            children.treeStateChanges = []
            
            
            if change == 'value' and param.name() == 'Alpha':
                self.par.layout.setAlpha(imgData, data)
            
            
            if change == 'value' and param.name() == 'Priority':
                self.par.layout.setPriority(imgData, data)
            
            
            if change == 'value' and param.name() == 'Reduce Dark':
                tree.param(path[0], 'Divide Flat').setToDefault()
                print imgData
                print lightImgData
                if data:
                    darkValues = tree.param(path[0], 'Dark:').opts['limits']
                    dark = tree.param(path[0], 'Dark:').opts['value']
                    darkKey = darkValues.keys()[darkValues.values().index(dark)]
                    error, warning = self.par.im.subFits(path[0], darkKey,
                                        force = self.alwaysForce,
                                        adjust = self.alwaysAdjust)
                    while error:
                        reply = QMessageBox.question(self.par, 'Bad Dark!', warning,
                                             QMessageBox.Yes | QMessageBox.YesAll | QMessageBox.No, QMessageBox.Yes)
                        if reply == QMessageBox.No:
                            with tree.treeChangeBlocker():
                                tree.param(path[0], 'Reduce Dark').setToDefault()
                                tree.treeStateChanges = []
                                return
                        if error == 'force':
                            if reply == QMessageBox.YesAll:
                                self.alwaysForce = True
                            error, warning = self.par.im.subFits(path[0], darkKey,
                                                                 force = True,
                                                                 adjust = self.alwaysAdjust)
                        if error == 'adjust':
                            if reply == QMessageBox.YesAll:
                                self.alwaysAdjust = True
                            error, warning = self.par.im.subFits(path[0], darkKey,
                                                                 force = self.alwaysForce,
                                                                 adjust = True)
                            
                    else:
                        self.par.layout.showImg(lightImgData, False)
                        priority = tree.param(path[0], 'Priority').opts['value']
                        alpha = tree.param(path[0], 'Alpha').opts['value']
                        show = tree.param(path[0], 'Show').opts['value']
                        self.par.layout.loadImg(imgData.split(' .. ')[0],
                                                imgData.split(' .. ')[1],
                                                priority, int(alpha), show) 
                else:
                    self.par.layout.showImg(lightImgData, True)
                    self.par.layout.removeImg('sub .. ' + imgData.split(' .. ')[1])
                    self.par.layout.removeImg('final .. ' + imgData.split(' .. ')[1])
                    
            
            if change == 'value' and param.name() == 'Divide Flat':
                if data:
                    flatValues = tree.param(path[0], 'Flat:').opts['limits']
                    flat = tree.param(path[0], 'Flat:').opts['value']
                    flatKey = flatValues.keys()[flatValues.values().index(flat)]
                    if self.par.im.divFits(path[0], flatKey):
                        QMessageBox.warning(self.par, 'Bad Flat!', 'Trying to divide by zero...\nSelect another flat!')
                        for child in self.par.flatTree:
                            if child.name() == flatKey:
                                self.par.flatTree.removeChild(child)

                    else:
                        self.par.layout.showImg(lightImgData, False)
                        priority = tree.param(path[0], 'Priority').opts['value']
                        alpha = tree.param(path[0], 'Alpha').opts['value']
                        show = tree.param(path[0], 'Show').opts['value']
                        self.par.layout.loadImg(imgData.split(' .. ')[0],
                                                imgData.split(' .. ')[1],
                                                priority, int(alpha), show)
                else:
                    self.par.layout.showImg(lightImgData, True)
                    self.par.layout.removeImg('div .. ' + imgData.split(' .. ')[1])
                    self.par.layout.removeImg('final .. ' + imgData.split(' .. ')[1])
                
   
    def nameChanged(self, new):
        splited = self.name().split()
        
        names = [child.name() for child in self.children()]
        oldName = ( set( self.par.im(splited[0].lower()).keys() ) - set(names) ).pop()
        
        if splited[0] != 'Light':
            for children in self.par.lightTree:
                vals = children.param(splited[0]+':').opts['limits'].copy()

                vals[new.name()] = vals.pop(oldName)
                children.param(splited[0]+':').setLimits(vals)
            self.par.im.renameImage(splited[0].lower(), oldName, new.name())
        else:
            for type in self.par.im.returnNoDarkFlat():
                self.par.im.renameImage(type, oldName, new.name())
                self.par.layout.renameImg(type, oldName, new.name())


class dock(QDockWidget):
    def __init__(self, layout):
        super(dock, self).__init__()
        
        self.layout = layout
        self.im = self.layout.im
        self.createTrees()
        
        self.setWidget(self.imgTree)
        self.setAllowedAreas(Qt.LeftDockWidgetArea)
        self.setFeatures(QDockWidget.DockWidgetMovable | QDockWidget.DockWidgetFloatable)
        self.setWindowTitle('Images')

    
    def createTrees(self):
        self.lightTree = myGroup(parent = self, name = 'Light Images')
        self.darkTree = myGroup(parent = self, name = 'Dark Images')
        self.flatTree = myGroup(parent = self, name = 'Flat Images')
        
        self.imgTree = pg.parametertree.ParameterTree(showHeader = False)
        self.imgTree.addParameters(self.lightTree)
        self.imgTree.addParameters(self.darkTree)
        self.imgTree.addParameters(self.flatTree)
        

def main():
    app = QApplication.instance()   # checks if QApplication already exists
    if not app:                     # create QApplication if it doesnt exist
        app = QApplication(sys.argv)
    main = mainWindow()
    main.show()
#    sys.exit(app.exec_())
    app.exec_()


if __name__ == '__main__':
    main()
