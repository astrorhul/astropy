import pyfits, numpy, gfit
from pylab import *
from scipy import ndimage
from os import remove
from time import time
import ctypes 

''' Important header keywords '''
'''
    NAXIS1 = size x
    NAXIS2 = size y
    EXPTIME
    IMAGETYP
    EGAIN
'''

''' To Do List '''
# Handle exceptions (sub, div)



# load shared lib
lib = ctypes.cdll.LoadLibrary('./libC/astro.dylib')
# extract function from lib
loop_cfun = lib.loop_cfun
mark_cfun = lib.mark_cfun
fieldFit_cfun = lib.outerMin


def testFit():
    f = openFits('/Users/pekapa/Documents/Internship/data/NGC869_60s_V.fit')
    data = [float64(f[0].header['EGAIN'] * f[0].data),                  # PhotoElectron Value
                sqrt( float64( f[0].header['EGAIN'] * f[0].data.copy() ) ), # Error
                f[0].header]
    rowcount = data[2]['NAXIS1']
    colcount = data[2]['NAXIS2']
    med = median(data[0])
    starNumber = 96
    res = 7
#     imshow(data[0])
    indata = ctypes.c_void_p(data[0].ctypes.data)
    
    print rowcount, colcount
    print med, starNumber, res
    fieldFit_cfun(indata, starNumber, rowcount, colcount, ctypes.c_double(med), res)
    return 1


def openFits(fileName):
    f = pyfits.open(fileName, ignore_missing_end=True, mode = 'copyonwrite')
        
    if size(f) == 1:
        newFileName = fileName.split('.')[-2]+'.mfit'
        if fileName[0] == '.':
            newFileName = '.'+newFileName
            
        
        f[0].data = float64(f[0].header['EGAIN'] * f[0].data)
        errTemp = f[0].copy()
        errTemp.data = sqrt(errTemp.data)
        f.append(errTemp)
       
        f[1].header['IMAGETYP'] += ' Errors'
        
        f.writeto(newFileName, clobber=True)
        f.close()
        f = pyfits.open(newFileName, ignore_missing_end=True, mode = 'copyonwrite')
        
    return f


def addFits(f, g, typ='Added Frames'):
    f.writeto('temp.mfit', clobber=True)
    temp = openFits('temp.mfit')
    remove('temp.mfit')
    
    temp[1].data = sqrt(temp[0].data + g[0].data)
    temp[0].data += g[0].data
    
    temp[0].header['IMAGETYP'] = typ
    temp[1].header['IMAGETYP'] = typ+' Errors'

    return temp
    
    
def subFits(f, g, typ='Subtracted Frames', mode='return'):
    n = size(where(f[0].data < g[0].data))
    
    if n != 0:
        if mode == 'force':
            print 'Warning: ', n, ' values are going negative! Forcing...'
            
        if mode == 'return':
            print 'Warning: ', n, ' values are going negative! Returning...'
            return f
  
    f.writeto('temp.mfit', clobber=True)
    temp = openFits('temp.mfit')
    remove('temp.mfit')
    
    temp[1].data = sqrt(temp[0].data + g[0].data)
    temp[0].data -= g[0].data
    
    temp[0].header['IMAGETYP'] = typ
    temp[1].header['IMAGETYP'] = typ+' Errors'

    return temp
    

def divFits(f, g, typ='Divided Frames'):
    n = size(where(g[0].data==0))
    if n != 0:
        print 'Warning: ', n, ' points are trying to divide by ZERO!'
        return f
        
    else:
        f.writeto('temp.mfit', clobber=True)
        temp = openFits('temp.mfit')
        remove('temp.mfit')
    
        tempErr = sqrt( (temp[1].data/temp[0].data)**2 + (g[1].data/temp[0].data)**2 )
        temp[0].data = temp[0].data / g[0].data
        temp[1].data = temp[0].data * tempErr
        
        temp[0].header['IMAGETYP'] = typ
        temp[1].header['IMAGETYP'] = typ+' Errors'
    
        return temp
    
    
def binMatrix(signal, medlvl = 2,  siglvl = 3):
    tbr = signal[0].copy() # To be returned file
    temp = signal[0].copy() # Temp file
    
    cut = medlvl * median(temp.data) # Sets cut lvl for median

    indata = float64(temp.data) # Sets the input data for C Function
    shape0, shape1 = indata.shape # Gets the shape of data
    outdata = zeros((shape0,shape1)) # Sets the output data for C Function
    loop = 1 # First loop of the function
    loop_cfun(ctypes.c_void_p(indata.ctypes.data), ctypes.c_double(cut), shape0, shape1, loop, ctypes.c_void_p(outdata.ctypes.data))
    temp.data = outdata # Extract the output data
    
    # Build histogram
    temp.data.shape = -1
    h = histogram(temp.data, range=(temp.data.max()/4 , temp.data.max()*3/4), bins = (cut / medlvl))

    # Fit a 1D Gaussian
    m = gfit.fit1g(h)
    # Setting threshold to cut 99.9% of the background (3sig)
    threshold = m.values['mu'] + siglvl*m.values['sigma']
    
    indata = float64(tbr.data)
    shape0, shape1 = indata.shape
    outdata = zeros((shape0,shape1))
    loop = 2
    loop_cfun(ctypes.c_void_p(indata.ctypes.data), ctypes.c_double(cut), shape0, shape1, loop, ctypes.c_void_p(outdata.ctypes.data))
    tbr.data = outdata
              
    tbr.header['IMAGETYP'] += ' Binary'
    return tbr
    
    
def countStars(binMatrix, minClusSize = 10):  
    marker = ctypes.c_int(100)
    
    colcount = binMatrix.header['NAXIS2']
    rowcount = binMatrix.header['NAXIS1']
    indata = float64(binMatrix.data)
        
    while len( where(indata == 1)[0] ):
        print marker.value-100
        marker.value += 1
        pos = where(indata == 1) # pos[0] -> x index; pos[1] -> y index
        mark_cfun(ctypes.c_void_p(indata.ctypes.data), int(pos[0][0]), int(pos[1][0]), rowcount, colcount, ctypes.byref(marker), minClusSize)
    counter = marker.value - 100

    return counter
    
def starParam(signal, binMatrix, flag, resolution = 7, sz = 25):
    grid = signal[0].data * (binMatrix == flag)
        
    total = numpy.abs(grid).sum()
    Y, X = indices(grid.shape)
    yc = numpy.abs(Y*grid).sum()/total
    xc = numpy.abs(X*grid).sum()/total
    #xc, yc = ndimage.measurements.center_of_mass(grid)
    
    ymin = max(yc - sz, 0)
    xmin = max(xc - sz, 0)
    ymax = min(yc + sz, grid.shape[0])
    xmax = min(xc + sz, grid.shape[1])
    
    newGrid = signal[0].data[ymin:ymax, xmin:xmax]
    newGridErr = signal[1].data[ymin:ymax, xmin:xmax]
    
    success, m = gfit.fitg(newGrid, newGridErr)
    if success:
        # Adjust centres to fit the image
        m.values['mux'] += xc - sz
        m.values['muy'] += yc - sz
        result = 'Success'
        if (m.values['sigx'] < resolution/3) or (m.values['sigy'] < resolution/3) or (m.values['sigx'] > resolution) or (m.values['sigy'] > resolution):
            result = 'Sigma'
        if (m.values['mux'] < 2* resolution) or (m.values['muy'] < 2* resolution) or (m.values['mux'] > grid.shape[1] - 2* resolution) or (m.values['muy'] > grid.shape[0] - 2* resolution):
            result = 'Border'
    else:
        result = 'Failed'
    
        
    return result, m
    

# Loop over region looking for pixels above threshold and return the number of local maxima for clusters greater than the minumum Cluster Size (avoid hot-pixels)
# reset modes:  0 - change back from 2 to 1
#               1 - default, change from 1 to 2
#               2 - change from 1 to 2 and call back as mode 0
def clusters(signal, binMatrix, i, j, marker, minClusSize = 10):

    # Return the extracted star and centre in case the cluster is greater than the minimum Cluster Size
    if len(seen) > minClusSize:
        
        # Create grid with the same size of data to find centre
        grid = zeros((signal[0].header['NAXIS2'], signal[0].header['NAXIS1']))
        i = 0
        while i < len(seen):
            x = seen[i][0]
            y = seen[i][1]
            grid[x][y] = signal[0].data[x][y]
            i += 1

        # Get approximate centre
        def get_centre(grid):
            total = numpy.abs(grid).sum()
            X, Y = numpy.indices(grid.shape)
            yc = numpy.argmax((Y*numpy.abs(grid)).sum(axis=1)/total)
            xc = numpy.argmax((X*numpy.abs(grid)).sum(axis=0)/total)
            return xc, yc

        # Create new grid with data values and size 2sz x 2sz arround the centre
        def adjust(centre, sz):
            xc = centre[0]
            yc = centre[1]
            if yc < sz or xc < sz or yc + sz > signal[0].header['NAXIS1'] or xc + sz > signal[0].header['NAXIS2']:
                return None, None
            else:
                ymin = yc - sz
                xmin = xc - sz
                ymax = yc + sz
                xmax = xc + sz
                return signal[0].data[ymin:ymax, xmin:xmax], signal[1].data[ymin:ymax, xmin:xmax]

        # Get centre and first 15 px neighbours, get a new centre and a larger neighbourhood
        # Avoid external parts of a star as beeing counted as a star
        centre = [0,0]
        centre[0], centre[1] = get_centre(grid)
        sz = 15
        grid, errors = adjust(centre, sz)
        # Grid is None means the star is too close to the board and should not be considered
        if grid is None:
            return None, None, None
        newcentre = [0,0]
        newcentre[0], newcentre[1] = get_centre(grid)

        # Correct the centre
        centre[1] = centre[1] + newcentre[1] - sz
        centre[0] = centre[0] + newcentre[0] - sz

        # Get a new grid 50x50
        sz = 25
        grid, errors = adjust(centre, sz)
        if grid is None:
            return None, None, None
        else:
            # Returns centre, final grid and errors grid
            return centre, grid, errors

    else:
        return None, None, None
    