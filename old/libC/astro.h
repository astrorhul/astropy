//#include <TMath.h>
//#include <TF2.h>
//#include <TH2.h>

extern "C" {
    void cluster_cfun(const void *indatav, int rowcount, int colcount, void *outdatav);
    void loop_cfun(const void *indatav, double cut, int rowcount, int colcount, int loop, void *outdatav);
    void mark_cfun(void *indatav, int i, int j, int rowcount, int colcount, int *marker_p, int minClusSize);
    double chi2(const void *indata, const void *inerrors, int rowcount, int colcount, double *par, int len_par, double *chiSq_p);
}