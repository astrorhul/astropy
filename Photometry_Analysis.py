import pyfits              as _pyfits
import Image_Analysis      as _Image_Analysis
import Cluster_Finder      as _Cluster_Finder
import numpy               as _np
import Cluster_Analysis    as _Cluster_Analysis
import Photometry_Aperture as _Photometry_Aperture
import Photometry_Fit      as _Photometry_Fit

class Photometry_Analysis():

    def __init__(self, image, error, debug_print = False):
        self.debug_print = debug_print
        self.image       = image
        self.error       = error

    def image_stats(self):
        self.ia         = _Image_Analysis.Image_Analysis(self.image)
        self.stats      = self.ia.stats()
        self.background = float(self.stats['mode'][0])

    def photometry_aperture(self, stats):
        self.pa = _Photometry_Aperture.Photometry_Aperture(self.image, self.error)
        pixels  = self.pa.get_circle_masks(stats['x_centre'], stats['y_centre'], self.pa.inner_radius, self.pa.annulus1_radius, self.pa.annulus2_radius)

        photometry_aperture                           = {}
        photometry_aperture['inner_radius']           = self.pa.inner_radius
        photometry_aperture['annulus1_radius']        = self.pa.annulus1_radius
        photometry_aperture['annulus2_radius']        = self.pa.annulus2_radius
        photometry_aperture['intensity']              = -1
        photometry_aperture['intensity_error']        = -1

        if pixels == -1:
            photometry_aperture['inner_radius_pixels']    = -1
            photometry_aperture['annulus1_radius_pixels'] = -1
            photometry_aperture['annulus2_radius_pixels'] = -1
        else:
            photometry_aperture['inner_radius_pixels']    = pixels[0]
            photometry_aperture['annulus1_radius_pixels'] = pixels[1]
            photometry_aperture['annulus2_radius_pixels'] = pixels[2]
            photometry_aperture['intensity'], photometry_aperture['intensity_error']  = self.pa.background_reduction(pixels[0], pixels[1])

        return photometry_aperture

    def photometry_fit(self, stats):
        self.pf = _Photometry_Fit.Photometry_Fit(self.image, self.error)

        photo_fit = self.pf.fit_gaussian_to_star(stats['x_centre'], stats['y_centre'], stats['x_sigma'], stats['y_sigma'], stats['min_intensity'], self.background, theta = 0)
        return photo_fit


def test(simple_image = True,  star_count = 10, threshold = 1000, sim = False, load = True, image_size = (1200, 700), background = 300, xy_variation = 100, max_amp = 20000, min_amp = 5000, std_max = 5.1, std_min = 4.9, theta = True, noise = True):
    import Globals         as _Globals
    import pylab           as _pylab
    import Image_Simulator as _Image_Simulator
    import Photometry_Fit  as _Photometry_Fit
    import Image_Analysis  as _Image_Analysis

    if sim:
        i_s                  = _Image_Simulator.Image_Simulator(star_count, image_size, background, xy_variation, max_amp, min_amp, max_std, min_std)
        x, y, z              = i_s.generate_image(noise)
        loc, std, amp, theta = i_s.get_parameters(x, y)
        image                = i_s.generate_stars(z, x, y, loc, amp, std, theta)
        # Must do error array
    else:
        if simple_image:
            hdu = _pyfits.open(_Globals.photom_data_example_sim)
        else:
            hdu = _pyfits.open(_Globals.photom_data_example_complex)
    
        image = hdu[0].data
        error = _np.sqrt(hdu[0].header['EGAIN'] * image)

    p_a = Photometry_Analysis(image, error, debug_print = False)
    p_a.image_stats()

    # Get the clusters
    cf = _Cluster_Finder.Cluster_Finder(image)
    cf.set_threshold(threshold)
    clusters = cf.cluster()
    ca       = _Cluster_Analysis.Cluster_Analysis(image, error, threshold)
    clusters = ca.sort_clusters_by_intensity(clusters)
    
    photo_apt_int = []
    photo_apt_err = []
    x_intensity   = []
    x_err         = []
    y_intensity   = []
    y_err         = []
    xy_intensity  = []
    xy_err        = []
    x_bg_red_int  = []
    x_bg_err      = []
    y_bg_red_int  = []
    y_bg_err      = []

    i = 0 # fig counter
    for cluster in clusters:
        ca.set_cluster(cluster)
        if not ca.good_cluster(): continue
        stats = ca.stats()

        # Photometry Aperture
        photo_apt = p_a.photometry_aperture(stats)
        if photo_apt['inner_radius_pixels'] == -1 or photo_apt['intensity'] == -1 or photo_apt['intensity_error'] == -1: continue

        # Photometry fit
        photo_fit = p_a.photometry_fit(stats)
        
        # Construct intensity & error arrays for plot
        photo_apt_int.append(photo_apt['intensity'])
        photo_apt_err.append(photo_apt['intensity_error'])
        
        x_intensity.append(_np.fabs(photo_fit['xProjAmp']))
        x_err.append(photo_fit['xProjErr'])
        
        y_intensity.append(_np.fabs(photo_fit['yProjAmp']))
        y_err.append(photo_fit['yProjErr'])
        
        xy_intensity.append(_np.fabs(photo_fit['2dAmp']))
        xy_err.append(photo_fit['2dErr'])
        
        x_bg_red_int.append(_np.fabs(photo_fit['x_bg_red']))
        x_bg_err.append(photo_fit['x_bg_err'])
        
        y_bg_red_int.append(_np.fabs(photo_fit['y_bg_red']))
        y_bg_err.append(photo_fit['y_bg_err'])

        # Plotting
        fig = _pylab.figure(i)

        fig.clf()
        _pylab.subplot(2, 2, 2)
        _pylab.imshow(_np.log10(image), cmap = 'gray')

        # Construct the circles
        inner    = _pylab.Circle((ca.y_w_mean, ca.x_w_mean), photo_apt['inner_radius'], color = 'r', Fill = False)
        annulus1 = _pylab.Circle((ca.y_w_mean, ca.x_w_mean), photo_apt['annulus1_radius'], color = 'b', Fill = False)
        annulus2 = _pylab.Circle((ca.y_w_mean, ca.x_w_mean), photo_apt['annulus2_radius'], color = 'g', Fill = False)
        # Plot the circles
        fig.gca().add_artist(inner)
        fig.gca().add_artist(annulus1)
        fig.gca().add_artist(annulus2)

        scatter_range = _np.arange(len(p_a.pf.expanded_points))
        _pylab.subplot(2, 2, 1)
        _pylab.scatter(scatter_range, p_a.pf.x_proj)
        _pylab.errorbar(scatter_range, p_a.pf.x_proj, yerr = p_a.pf.yerr, fmt = None)

        _pylab.plot(p_a.pf.x_ind, p_a.pf.gaussian_1D(p_a.pf.x_ind, *p_a.pf.xg_popt), c = 'r')
        _pylab.plot(p_a.pf.x_ind, p_a.pf.pred2d.sum(0), c = 'g')

        sp = _pylab.subplot(2, 2, 2)
        sp.set_xlim(0, image.shape[1]) # Format image nicely.
        sp.set_ylim(image.shape[0], 0)
        _pylab.scatter(stats['x_centre'], stats['y_centre'], marker = '+', c = 'r', s = 1000) # Mark current cluster.

        _pylab.subplot(2, 2, 3)
        _pylab.imshow(p_a.pf.expanded_points)
        _pylab.contour(p_a.pf.x_ind, p_a.pf.y_ind, p_a.pf.pred2d, 3, colors = 'w')

        yc       = ca.x_w_mean - (ca.x_w_mean - p_a.pf.grid_expansion)
        xc       = ca.y_w_mean - (ca.y_w_mean - p_a.pf.grid_expansion)
        inner    = _pylab.Circle((xc, yc), photo_apt['inner_radius'], color = 'r', Fill = False)
        annulus1 = _pylab.Circle((xc, yc), photo_apt['annulus1_radius'], color = 'g', Fill = False)
        fig.gca().add_artist(inner)
        fig.gca().add_artist(annulus1)

        _pylab.subplot(2, 2, 4)
        _pylab.scatter(p_a.pf.y_proj, scatter_range)
        _pylab.errorbar(p_a.pf.y_proj, scatter_range, xerr = p_a.pf.xerr, fmt = None) # Make the projection look nice.
        _pylab.plot(p_a.pf.gaussian_1D(p_a.pf.y_ind, *p_a.pf.yg_popt), p_a.pf.y_ind, c = 'r')
        _pylab.plot(p_a.pf.pred2d.sum(1), p_a.pf.y_ind, c = 'g')

        i += 1

    _pylab.figure(i)

    x_axis        = _np.arange(len(x_intensity))
    photo_apt_err = _np.array(photo_apt_err)
    photo_apt_int = _np.array(photo_apt_int)
    x_err         = _np.array(x_err)
    y_err         = _np.array(y_err)
    xy_err        = _np.array(xy_err)
    x_bg_err      = _np.array(x_bg_err)
    y_bg_err      = _np.array(y_bg_err)
    
    _pylab.scatter(x_axis, photo_apt_int, c = 'r', label = 'Apt phot int')
    _pylab.errorbar(x_axis, photo_apt_int, yerr = photo_apt_err, fmt = None, c = 'r')
    print 'Photo_apt error: ',(photo_apt_err / photo_apt_int) * 100
    
    _pylab.scatter(x_axis, x_intensity, c = 'b', label = 'X proj int')
    _pylab.errorbar(x_axis, x_intensity, yerr = x_err, fmt = None, c = 'b')
    print 'x proj error:', (x_err / x_intensity) * 100
    
    _pylab.scatter(x_axis, y_intensity, c = 'g', label = 'Y proj int')
    _pylab.errorbar(x_axis, y_intensity, yerr = y_err, fmt = None, c = 'g')
    print 'y proj error:', (y_err / x_intensity) * 100
    
    _pylab.scatter(x_axis, xy_intensity, c = 'y', label = '2d int')
    _pylab.errorbar(x_axis, xy_intensity, yerr = xy_err, fmt = None, c = 'y')
    print '2d proj error:', (xy_err / x_intensity) * 100
    
    _pylab.scatter(x_axis, x_bg_red_int, c = 'cyan', label = 'x bg red')
    _pylab.errorbar(x_axis, x_bg_red_int, yerr = x_bg_err, fmt = None, c = 'cyan')
    print 'x bg error:', (x_bg_err / x_intensity) * 100
        
    _pylab.scatter(x_axis, y_bg_red_int, c = 'pink', label = 'y bg red')
    _pylab.errorbar(x_axis, y_bg_red_int, yerr = y_bg_err, fmt = None, c = 'pink')
    print 'y bg error:', (y_bg_err / x_intensity) * 100
        
    _pylab.legend()
