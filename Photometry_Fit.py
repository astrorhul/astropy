import numpy                         as _np
from scipy.optimize import curve_fit as _curve_fit
from scipy.integrate import quad     as _quad
import Utilities                     as _Utilities

class Photometry_Fit():
    '''
    Used to fit clusters of pixels sent in numpy 2D array with a gaussian function and return some basic statistics on the cluster.
    '''

    def __init__(self, image, error):
        self.image = image
        self.error = error
        self.image_x_dim = self.image.shape[1]
        self.image_y_dim = self.image.shape[0]
        self.utilities   = _Utilities.Utilities()

    def gaussian_1D(self, x, amplitude, x_centre, x_sigma, offset):
        amplitude = _np.fabs(amplitude)
        x_sigma = _np.fabs(x_sigma)
        return amplitude / (_np.sqrt(2 * _np.pi) * x_sigma) * _np.exp(- ( (x - x_centre)**2 / (2 * x_sigma**2) )) + offset

    def gaussian_2D(self, (x, y), amplitude, x_centre, y_centre, x_sigma, y_sigma, theta, offset):
        amplitude = _np.fabs(amplitude)
        x_sigma   = _np.fabs(x_sigma)
        y_sigma   = _np.fabs(y_sigma)
        x_centre = float(x_centre)
        y_centre = float(y_centre)
        a        = (_np.cos(theta)**2) / (2 * x_sigma**2) + (_np.sin(theta)**2) / (2 * y_sigma**2)
        b        = -(_np.sin(2 * theta)) / (4 * x_sigma**2) + (_np.sin(2 * theta)) / (4 * y_sigma**2)
        c        = (_np.sin(theta)**2) / (2 * x_sigma**2) + (_np.cos(theta)**2) / (2 * y_sigma**2)
        gauss    = offset + amplitude / (2 * _np.pi * x_sigma * y_sigma) * _np.exp( - (a * ((x - x_centre)**2) + 2 * b * (x - x_centre) * (y - y_centre) + c * ((y - y_centre)**2)))

        return gauss.ravel()

    def poly_1D(self, x, gradient, intercept):
        return gradient * x + intercept

    def fit_poly_1D(self, x, y, x_proj, y_proj, gradient, intercept):
        p0           = [gradient, intercept]
        xpopt, xpcov = _curve_fit(self.poly_1D, x, x_proj, p0)
        ypopt, ypcov = _curve_fit(self.poly_1D, y, y_proj, p0)

        return xpopt, xpcov, ypopt, ypcov

    def integral(self, x, y, x_proj, y_proj, background, peak_width = 15):
        
        centre = self.xg_popt[1]
        distance_to_cut = _np.fabs(x - centre)
        
        if peak_width > _np.all(distance_to_cut):
            peak_width = distance_to_cut.mean() # Check if this is acceptable.
    
        cut        = distance_to_cut > peak_width
        x_data_cut = x[cut]
        y_data_cut = y[cut]

        self.xpopt, self.xpcov, self.ypopt, self.ypcov = self.fit_poly_1D(x_data_cut, y_data_cut, x_proj[cut], y_proj[cut], 0, background)
        x_background_intensities                       = self.poly_1D(x, *self.xpopt)
        y_background_intensities                       = self.poly_1D(y, *self.ypopt)
        x_gaussian_intensities                         = self.gaussian_1D(self.x_ind, *self.xg_popt)
        y_gaussian_intensities                         = self.gaussian_1D(self.y_ind, *self.yg_popt)
        self.x_reduced_intensities                     = x_gaussian_intensities - x_background_intensities
        self.y_reduced_intensities                     = y_gaussian_intensities - y_background_intensities
        self.x_int                                     = self.x_reduced_intensities.sum()
        self.y_int                                     = self.y_reduced_intensities.sum()

        return self.x_int, self.y_int

    def fit_gaussian_1D(self, x, y, x_proj, y_proj, p0x, p0y, x_sigma, y_sigma):
        xg_popt, xg_pcov = _curve_fit(self.gaussian_1D, x, x_proj, p0 = p0x, sigma = x_sigma)
        yg_popt, yg_pcov = _curve_fit(self.gaussian_1D, y, y_proj, p0 = p0y, sigma = y_sigma)

        return xg_popt, xg_pcov, yg_popt, yg_pcov

    def fit_gaussian_2D(self, (x, y), z, p0, sigma):
        return _curve_fit(self.gaussian_2D, (x, y), z, p0, sigma)

    def fit_gaussian_to_star(self, x_centre, y_centre, x_sigma, y_sigma, min_intensity, background, theta):
        self.expanded_points, self.expanded_error_points, self.grid_expansion, self.x_grid, self.y_grid = self.utilities.expand_grid(self.image, self.error, x_centre, y_centre)

        # 1D Fitting
        self.x_proj                                            = self.expanded_points.sum(0)
        self.y_proj                                            = self.expanded_points.sum(1)
        self.x_ind                                             = _np.arange((int(round(x_centre)) + self.grid_expansion) - (int(round(x_centre)) - self.grid_expansion))
        self.y_ind                                             = _np.arange((int(round(y_centre)) + self.grid_expansion) - (int(round(y_centre)) - self.grid_expansion))
        self.p0x                                               = [(self.x_proj.max() - background) * x_sigma, len(self.x_ind) / 2, x_sigma, background]
        self.p0y                                               = [(self.y_proj.max() - background) * y_sigma, len(self.y_ind) / 2, y_sigma, background]
        self.x_sigma                                           = self.expanded_error_points.sum(0)
        self.y_sigma                                           = self.expanded_error_points.sum(1)   
        self.xg_popt, self.xg_pcov, self.yg_popt, self.yg_pcov = self.fit_gaussian_1D(self.x_ind, self.y_ind, self.x_proj, self.y_proj, self.p0x, self.p0y, self.x_sigma, self.y_sigma)

        # 2D Fitting
        self.p0              = [self.xg_popt[0], x_centre, y_centre, 10, 10, theta, background]
        self.popt, self.pcov = self.fit_gaussian_2D((self.x_grid, self.y_grid), self.expanded_points.flatten(), self.p0, self.expanded_error_points.flatten())
        self.pred2d          = self.gaussian_2D((self.x_grid, self.y_grid), *self.popt)
        self.pred2d          = self.pred2d.reshape((len(self.x_ind), len(self.y_ind)))

        # Background only fit and subtraction
        self.x_red_int, self.y_red_int = self.integral(self.x_ind, self.y_ind, self.x_proj, self.y_proj, min_intensity)

        # Calculate error
        self.xerr, self.yerr  =  _np.sqrt((self.error[self.y_grid, self.x_grid]**2).sum(0)), _np.sqrt((self.error[self.y_grid, self.x_grid]**2).sum(1))
        xy_err                = (self.xerr**2).sum() + (self.yerr**2).sum()
        x_bg_sigmas           = _np.sqrt(_np.diag(self.xpcov))
        x_bg_grad_err         = (x_bg_sigmas[0] * self.x_ind)**2
        x_bg_y_intercept_err  = x_bg_sigmas[1]**2
        x_bg_line_err         = _np.sqrt((x_bg_grad_err + x_bg_y_intercept_err).sum())
        y_bg_sigmas           = _np.sqrt(_np.diag(self.ypcov))
        y_bg_grad_err         = (y_bg_sigmas[0] * self.y_ind)**2
        y_bg_y_intercept_err  = y_bg_sigmas[1]**2
        y_bg_line_err         = _np.sqrt((y_bg_grad_err + y_bg_y_intercept_err).sum())

        fits             = {}
        fits['xProjAmp'] = self.xg_popt[0]
        fits['xProjErr'] = _np.sqrt((self.xerr**2).sum())
        fits['x_centre'] = self.xg_popt[1]
        fits['x_sigma']  = self.xg_popt[2]
        fits['x_offset'] = self.xg_popt[3]
        
        fits['yProjAmp'] = self.yg_popt[0]
        fits['yProjErr'] = _np.sqrt((self.yerr**2).sum())
        fits['y_centre'] = self.yg_popt[1]
        fits['y_sigma']  = self.yg_popt[2]
        fits['y_offset'] = self.yg_popt[3]
        
        fits['2dAmp']    = self.popt[0]
        fits['2dErr']    = _np.sqrt(xy_err)
        fits['x_centre'] = self.popt[1]
        fits['y_centre'] = self.popt[2]
        fits['x_sigma']  = self.popt[3]
        fits['y_sigma']  = self.popt[4]
        fits['theta']    = self.popt[5]
        fits['background'] = self.popt[6]
        
        fits['x_bg_red'] = self.x_red_int
        fits['x_bg_err'] = _np.sqrt(xy_err + x_bg_line_err)
        
        fits['y_bg_red'] = self.y_red_int
        fits['y_bg_err'] = _np.sqrt(xy_err + y_bg_line_err)

        return fits

def test(simple_image = True, threshold = 1000):
    import Cluster_Finder   as _Cluster_Finder
    import pyfits           as _pyfits
    import Globals          as _Globals
    import Cluster_Analysis as _Cluster_Analysis
    import pylab            as _pylab
    import Image_Analysis   as _Image_Analysis

    if simple_image:
        hdu = _pyfits.open(_Globals.photom_data_example_simple)
    else:
        hdu = _pyfits.open(_Globals.photom_data_example_complex)
    
    image = hdu[0].data
    error = _np.sqrt(hdu[0].header['EGAIN'] * image)
    ia         = _Image_Analysis.Image_Analysis(image)
    stats      = ia.stats()
    background = float(stats['mode'][0]) # Return type should be a float? Probably.

    cf = _Cluster_Finder.Cluster_Finder(image)
    cf.set_threshold(threshold)
    clusters = cf.cluster()

    ca = _Cluster_Analysis.Cluster_Analysis(image, error, threshold)
    clusters = ca.sort_clusters_by_intensity(clusters)
    pf = Photometry_Fit(image, error)

    i = 0 # fig counter
    for cluster in clusters:
        ca.set_cluster(cluster)
        if not ca.good_cluster(): continue

        stats = ca.stats()
        fig = _pylab.figure(i)

        _pylab.clf()

        # Perform the fits
        pf.fit_gaussian_to_star(stats['x_centre'], stats['y_centre'], stats['x_sigma'], stats['y_sigma'], stats['min_intensity'], background, theta = 0)

        # Plotting
        sub_fig221 = _pylab.subplot(2, 2, 1)
        scatter_range = _np.arange(len(pf.expanded_points))
        _pylab.scatter(scatter_range, pf.x_proj)
        _pylab.errorbar(scatter_range, pf.x_proj, yerr = pf.yerr, fmt = None)
        _pylab.plot(pf.x_ind, pf.gaussian_1D(pf.x_ind, *pf.xg_popt), c = 'r')
        _pylab.plot(pf.x_ind, pf.pred2d.sum(0), c = 'g')
        _pylab.plot(pf.x_ind, pf.poly_1D(pf.x_ind, *pf.xpopt), c = 'black')
        y2_sub_fig221 = sub_fig221.twinx()
        y2_sub_fig221.plot(pf.x_ind, pf.x_reduced_intensities, color = 'purple')

        sp = _pylab.subplot(2, 2, 2)

        _pylab.imshow(_np.log10(image), cmap = 'gray')
        _pylab.scatter(stats['x_centre'], stats['y_centre'], marker = '+', c = 'r', s = 1000) # Mark current cluster.
        sp.set_xlim(0, image.shape[1]) # Format image nicely.
        sp.set_ylim(image.shape[0], 0)

        _pylab.subplot(2, 2, 3)
        _pylab.imshow(pf.expanded_points)
        _pylab.contour(pf.x_ind, pf.y_ind, pf.pred2d, 3, colors = 'w')

        sub_fig224 = _pylab.subplot(2, 2, 4)
        _pylab.scatter(pf.y_proj, scatter_range)
        _pylab.errorbar(pf.y_proj, scatter_range, xerr = pf.xerr, fmt = None) # Make the projection look nice.
        _pylab.plot(pf.gaussian_1D(pf.y_ind, *pf.yg_popt), pf.y_ind, c = 'r')
        _pylab.plot(pf.pred2d.sum(1), pf.y_ind, c = 'g')
        _pylab.plot(pf.poly_1D(pf.y_ind, *pf.ypopt), pf.y_ind, c = 'black')
        y2_sub_fig224 = sub_fig224.twiny()
        _pylab.plot(pf.y_reduced_intensities, pf.y_ind, color = 'purple')

        i += 1

        _pylab.show()
