import pylab  as _pl 
import numpy  as _np
import pyfits as _pf

class NuView : 
    def __init__(self) : 
        pass 

    def plotSummary(self) :
        _pl.figure(1)
        _pl.clf()
        self.plotWavelengthPosition(25,25);
        self.plotWavelengthPosition(20,25);
        self.plotWavelengthPosition(15,25);
        self.plotWavelengthPosition(10,25);
        self.plotWavelengthPosition(5,25);
        self.plotWavelengthPosition(0,25);
        _pl.grid(True)
        _pl.xlabel('position on camera $x$ mm')
        _pl.ylabel('wavelength $\lambda$ nm')

    def plotWavelengthPosition(self,alpha = 15.0, dv0 = 30.0) :
        x = _np.arange(-5e-3,5e-3,0.1e-3)
        wl = self.wavelengthPosition(alpha,dv0,x)
        dx = 1e-4

        g = (self.wavelengthPosition(alpha,dv0,dx)-self.wavelengthPosition(alpha,dv0,-dx))/(2*dx)
        print g
#        g = 14433.7278623
        wl0 = self.wavelengthPosition(alpha,dv0,0)
        wlfit = g*x+wl0

        _pl.plot(x/1e-3,wl)
        _pl.plot(x/1e-3,wlfit,'b--')

    def wavelengthPosition(self, alpha = 15.0, dv0 = 30.0 , x = 0) :
        alpha = alpha/180.0*_np.pi
        dv0   = dv0/180.0*_np.pi

        k  = 1 
        n  = 1200
        f1 = 50e-3
        f2 = 50e-3

        dv = dv0 + _np.arctan(x/f2)

        wl = 2/(1e-6*k*n)*_np.sin((dv+2*alpha)/2.0)*_np.cos(dv/2.0)

        return wl

    def reverseFibreAnalysis(self, dir,fileList) :
        '''    
        dir      : Absolute file path 
        fileList : list of files where fibre has been scanned
        '''

        iFile = 0 

        f = open(dir+'/'+fileList) 

        for l in f :
            f = pf.open(dir+l)
            print iFile, f[0].data.sum()        
            iFile = iFile + 1
            _pl.imshow(f[0].data)

