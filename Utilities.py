import numpy as _np

class Utilities():
    
    def __init__(self):
        pass
    
    def expand_grid(self, image, error, x_centre, y_centre, grid_expansion = 30):
        '''
        Make an expanded grid around a centre point.
        x_centre       : The central x-coordinate that the expansion is conducted.
        y_centre       : The central y-coordinate that the expansion is conducted.
        grid_expansion : How much the cluster is to be expanded by in both x and y directions.
        '''
        distance_to_x_edge = abs(int(round(image.shape[1] - x_centre)))
        distance_to_y_edge = abs(int(round(image.shape[0] - y_centre)))

        if distance_to_x_edge > grid_expansion and distance_to_y_edge > grid_expansion:
            grid_expansion = grid_expansion
        else:
            temp           = _np.array([distance_to_x_edge, distance_to_y_edge])
            grid_expansion = temp.min()

        exp_x                   = _np.arange(int(round(x_centre)) - grid_expansion, int(round(x_centre)) + grid_expansion, 1)
        exp_y                   = _np.arange(int(round(y_centre)) - grid_expansion, int(round(y_centre)) + grid_expansion, 1)
        x_grid, y_grid          = _np.meshgrid(exp_x, exp_y)
        expanded_cluster_points = image[y_grid, x_grid]
        expanded_error_points   = error[y_grid, x_grid]

        return expanded_cluster_points, expanded_error_points, grid_expansion, x_grid, y_grid