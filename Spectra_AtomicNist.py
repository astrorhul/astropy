import numpy as _np
import pylab as _pl 
import re 

class AtomicNist :
    '''
    http://www.nist.gov/pml/data/asd.cfm
    http://physics.nist.gov/PhysRefData/ASD/lines_form.html
    Lower wavelength : 400 nm 
    Upper wavelength : 1000 nm
    Output Options : ASCII (text)
    Retrive data
    Cut, paste and save ascii file
    '''

    def __init__(self, fileName) :
        self.fileName = fileName 
        self.iWavelength = 1
        self.iRelInt = 3

        self.load()
        self.plot()

    def load(self) :
        f = open(self.fileName,'r')

        self.Wavelength = []
        self.RelInt     = []
        iLine = -1

        for l in f :
            iLine = iLine+1
            if iLine<6 :
                continue 
            sl = l.split('|') 
            sl = [x.strip(' ') for x in sl]
            if len(sl) != 17 : 
                continue 
        
            if len(re.sub('[^0-9]','',sl[self.iRelInt])) != 0 and len(sl[self.iWavelength]) != 0 :
                self.Wavelength.append(float(sl[self.iWavelength]))
                self.RelInt.append(float(re.sub('[^0-9]','',sl[self.iRelInt])))
        self.Wavelength = _np.array(self.Wavelength)
        self.RelInt = _np.array(self.RelInt)
            
    def plot(self) :
#        _pl.semilogy(self.Wavelength,self.RelInt,'*')
        _pl.plot(self.Wavelength,self.RelInt/(1.1*self.RelInt.max()),'*')

