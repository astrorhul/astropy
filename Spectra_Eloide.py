import pylab        as _pl
import pyfits       as _pf 
import scipy.signal as _si


class Eloide : 
    '''http://atlas.obs-hp.fr/elodie/'''

    def __init__(self,fileName = './data/Eloide/vega.fits') :
        f = _pf.open(fileName)
        self.lambdaStart = 4000
        self.lambdaEnd   = 6800
        self.lambdaStep  = 0.05
        self.lambdaR     = _pl.arange(self.lambdaStart,
                                      self.lambdaEnd,
                                      self.lambdaStep)
        self.spectrum    = f[0].data

    def convolve(self, function) :
        self.spectrumNV = _pl.convolve(self.spectrum,function,'same')
        return self.spectrumNV

    def convolveFFT(self, function) :
        self.spectrumNV = _si.fftconvolve(self.spectrum,function,'full')
        return self.spectrumNV

    def setLambdaStep(lambdaStep = 0.05) : 
        '''Data steps for wavelength'''
        self.lambdaStep = lambdaStep

    def makeGaussian(self, npoints = 512) :
        '''Make Gaussian convolution kernel'''
        sigma = 1.0/self.lambdaStep
        xoff = npoints/2
        x = _pl.arange(0,npoints,1)
        y = _pl.exp(-(x-xoff)**2/(2*sigma**2))
        y = y/y.sum()

        return y 

    def plot(self) :
        f = _pl.figure(1)
        f.subplotpars.left = 0.15
        _pl.clf()
        _pl.xlabel("Wavelength $\\lambda$ [0.1 nm]")
        _pl.ylabel("Intrumental flux $F_{\\lambda}$ [arbitary]")
        _pl.plot(self.lambdaR,self.spectrum, label='Raw data')
        _pl.plot(self.lambdaR,self.spectrumNV, label='Convoluted')
        _pl.legend()
        _pl.savefig("./plots/makeNuViewSpectrum.pdf")

def EloideMakeNuViewSpectrum(fileName='./data/Eloide/vega.fits') :
    e     = Eloide()
    gauss = e.makeGaussian(512)
    e.convolve(gauss)
    e.plot()

