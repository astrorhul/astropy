import ErrorImage as _ErrorImage
import Manager    as _Manager 
import pyfits     as _pyfits
import pylab      as _pylab
import numpy      as _numpy

_manager = _Manager.gm()

def minimum(iden) : 
    image = _manager.getImage(iden)['img'] 
    var   = iden+":minimum"
    typ   = 'value'    
    val   = image.minimum()
    _manager.addData(var,typ,val)
    return val

def maximum(iden) : 
    image = _manager.getImage(iden)['img']
    var   = iden+":maximum"
    typ   = 'value'    
    val   = image.maximum()
    _manager.addData(var,typ,val)
    return val 

def mean(iden) : 
    image = _manager.getImage(iden)['img']
    var   = iden+":mean"
    typ   = 'value'    
    val   = image.mean()
    _manager.addData(var,typ,val)
    return val

def median(iden) : 
    pass 

def mode(iden) : 
    pass

def rms(iden) : 
    image = _manager.getImage(iden)['img']
    var   = iden+":rms"
    typ   = 'value'    
    val   = image.rms()
    _manager.addData(var,typ,val)
    return val

def histogram(iden,nbins,start,end) : 
    image = _manager.getImage(iden)['img']
    var   = iden+":histogram"
    typ   = 'table'    
    [entries,binEdges]   = image.histogram(nbins,start,end)
    binCentres = ((_numpy.roll(binEdges,1)+binEdges)/2.0)[0:-1]
    entriesErr = _numpy.sqrt(entries) 
    val                  = {'binEdges':binEdges,'binCentres':binCentres,'entries':entries,
                            'entriesErr':entriesErr}    
    plt                  = {'x':'binCentres','y':'entries','yerr':'entriesErr'}
    _manager.addData(var,typ,val,plt)
    return val

def projectionX(iden) : 
    image = _manager.getImage(iden)['img']
    var   = iden+":xprojection"
    typ   = 'table' 
    [pix,proj,projErr] = image.projectionX()
    val            = {'pixelCoord':pix,'xProjection':proj,'xProjectionErr':projErr}
    plt            = {'x':'pixelCoord','y':'xProjection','yerr':'xProjectionErr'}
    _manager.addData(var,typ,val,plt)
    return val

def projectionY(iden) : 
    image = _manager.getImage(iden)['img']
    var   = iden+":yprojection"
    typ   = 'table' 
    [pix,proj,projErr] = image.projectionY()
    val            = {'pixelCoord':pix,'yProjection':proj,'yProjectionErr':projErr}
    plt            = {'x':'pixelCoord','y':'yProjection','yerr':'yProjectionErr'}
    _manager.addData(var,typ,val,plt)
    return val

def cluster(iden) : 
    pass
