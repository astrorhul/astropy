import numpy as _np

class Photometry_Aperture():

    def __init__(self, image, error, inner_radius = 15, annulus1_radius = 20, annulus2_radius = 25, debug_print = False):
        self.debug_print         = debug_print
        self.image               = image
        self.error               = error
        self.image_x_dim         = self.image.shape[1]
        self.image_y_dim         = self.image.shape[0]
        self.x                   = _np.arange(0, self.image_x_dim, 1)
        self.y                   = _np.arange(0, self.image_y_dim, 1)
        self.x_grid, self.y_grid = _np.meshgrid(self.x, self.y)
        self.inner_radius        = inner_radius
        self.annulus1_radius     = annulus1_radius
        self.annulus2_radius     = annulus2_radius

    def get_circle_masks(self, a, b, inner_radius, annulus1_radius, annulus2_radius): # Efficiency can be improved.
        '''
        Returns a list of numpy arrays of (x, y) grid coordinates within a specified radius from a central point (a, b).
        Returns -1 if there are no pixels detected.
        a               : The x coordinate of the centre of the circle.
        b               : The y coordinate of the centre of the circle.
        inner_radius    : The radius of the innermost circle.
        annulus1_radius : The radius of the middle, second circle.
        annulus2_radius : The radius of the outermost circle.
        '''
        a               = round(a, 0)
        b               = round(b, 0)
        inner_mask      = ((self.x_grid - a)**2 + (self.y_grid - b)**2) < inner_radius**2
        annulus1_mask   = _np.logical_and( ((self.x_grid - a)**2 + (self.y_grid - b)**2) < annulus1_radius**2,
                                                ((self.x_grid - a)**2 + (self.y_grid - b)**2) > inner_radius**2)
        annulus2_mask   = _np.logical_and( ((self.x_grid - a)**2 + (self.y_grid - b)**2) < annulus2_radius**2,
                                                ((self.x_grid - a)**2 + (self.y_grid - b)**2) < annulus1_radius**2)
        inner_pixels    = [self.x_grid[inner_mask], self.y_grid[inner_mask]]
        annulus1_pixels = [self.x_grid[annulus1_mask], self.y_grid[annulus1_mask]]
        annulus2_pixels = [self.x_grid[annulus2_mask], self.y_grid[annulus2_mask]]
        pixels          = [inner_pixels, annulus1_pixels, annulus2_pixels]

        if annulus1_pixels[0].size == 0 or inner_pixels[0].size == 0:
            pixels = -1

        if self.debug_print:
            print 'Photometry_Aperture.get_circle_masks()    '
            print 'Inner radius:                             ', inner_radius
            print 'Inner mask:                               ', inner_mask
            print 'Inner pixels:                             ', inner_pixels
            print 'Annulus1 radius:                          ', annulus1_radius
            print 'Annulus1 mask:                            ', annulus1_mask
            print 'Annulus2 radius:                          ', annulus2_radius
            print 'Annulus2 mask:                            ', annulus2_mask

        return pixels

    def intensity(self, pixels):
        star_intensity = self.image[pixels[1], pixels[0]].sum()
        error          = _np.sqrt((self.error[pixels[1], pixels[0]]**2).sum())

        if self.debug_print:
            print 'Photometry_Aperture.intensity()           '
            print 'Intensity:                                ', cluster_intensity

        return star_intensity, error

    def aperture_intensity(self, inner_radius_pixels, annulus1_pixels):
        inner_radius_intensity = self.intensity(inner_radius_pixels)
        annulus1_intensity     = self.intensity(annulus1_pixels)
        aperture_intensity     = inner_radius_intensity - annulus1_intensity

        if self.debug_print:
            print 'Photometry_Aperture.aperture_intensity()  '
            print 'Aperture intensity:                       ', aperture_intensity

        return aperture_intensity


    def area(self, pixels):
        area = len(pixels[0])

        if self.debug_print:
            print 'Photometry_Aperture.area()                '
            print 'Area:                                     ', area

        return area

    def mean_intensity(self, image, pixels):
        intensity         = float(self.intensity(image, pixels))
        area              = float(self.area(pixels))
        average_intensity = intensity / area

        if self.debug_print:
            print 'Photometry_Aperture.mean_intensity()      '
            print 'Average Intensity:                        ', average_intensity

        return average_intensity

    def background_reduction(self, inner_pixels, annulus1_pixels):
        # Intensity reduction
        inner_intensity, inner_error       = self.intensity(inner_pixels)
        annulus1_intensity, annulus1_error = self.intensity(annulus1_pixels)
        inner_area                         = float(self.area(inner_pixels))
        annulus1_area                      = float(self.area(annulus1_pixels))
        area                               = inner_area / annulus1_area
        background_reduction               = inner_intensity - (area * annulus1_intensity)
        
        # Error
        background_error           = _np.sqrt((inner_error**2 + annulus1_error**2).sum())
        background_reduction_error = _np.sqrt(inner_error**2 + (inner_area / annulus1_area * background_error)**2)

        print inner_intensity, annulus1_intensity, inner_area, annulus1_area

        if self.debug_print:
            print 'Photometry_Aperture.background_reduction()'
            print 'Inner area:                               ', inner_area
            print 'Annulus1 area:                            ', annulus1_area
            print 'Background reduction:                     ', background_reduction

        return background_reduction, background_reduction_error

def test(simple_image = True, threshold = 1000, debug_print = False):
    import pyfits           as _pyfits
    import Globals          as _Globals
    import Cluster_Finder   as _Cluster_Finder
    import Cluster_Analysis as _Cluster_Analysis
    import pylab            as _pl

    if simple_image:
        hdu = _pyfits.open(_Globals.photom_data_example_simple)
    else:
        hdu = _pyfits.open(_Globals.photom_data_example_complex)
    
    image = hdu[0].data
    error = _np.sqrt(hdu[0].header['EGAIN'] * image)

    cf = _Cluster_Finder.Cluster_Finder(image)
    cf.set_threshold(threshold)
    clusters = cf.cluster()
    ca       = _Cluster_Analysis.Cluster_Analysis(image, error, threshold)
    clusters = ca.sort_clusters_by_intensity(clusters)

    for cluster in clusters:
        ca.set_cluster(cluster)
        if not ca.good_cluster(): continue

        print '-----------------------------------------'

        pa     = Photometry_Aperture(image, error, debug_print = debug_print)
        pixels = pa.get_circle_masks(ca.y_w_mean, ca.x_w_mean, pa.inner_radius, pa.annulus1_radius, pa.annulus2_radius)

        if pixels == -1: continue # No pixels were found.
        pa.background_reduction(pixels[0], pixels[1])

        # Plotting
        fig      = _pl.gcf()
        inner    = _pl.Circle((ca.y_w_mean, ca.x_w_mean), pa.inner_radius, color = 'r', Fill = False)
        annulus1 = _pl.Circle((ca.y_w_mean, ca.x_w_mean), pa.annulus1_radius, color = 'b', Fill = False)
        annulus2 = _pl.Circle((ca.y_w_mean, ca.x_w_mean), pa.annulus2_radius, color = 'g', Fill = False)
        fig.gca().add_artist(inner)
        fig.gca().add_artist(annulus1)
        fig.gca().add_artist(annulus2)
        _pl.plot(pixels[0][0], pixels[0][1], 'r+') # Mark the innermost pixels for a visual check on count.

    _pl.imshow(_np.log10(cf.image), cmap = 'gray') # Plot the clusters