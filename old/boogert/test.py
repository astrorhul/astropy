from AstroPython import * 

# file loading
loadFits("dark","./data/photometry/60s_Dark.fit")
loadFits("light","./data/photometry/NGC869_60s_V.fit")

# background subtract 
subImages("light","dark","lightsub")

# basic analysis 
pmin = minimum("lightsub")
pmax = maximum("lightsub")
mean("lightsub") 
median("lightsub") 
mode("lightsub") 
rms("lightsub")

# intermediate analysis 
histogram("lightsub",100,pmin,pmax)
projectionX("lightsub")
projectionY("lightsub")

# distance measurement 


# Photometry 



# display 
displayImage("lightsub", figure= True)
displayPlot("lightsub:xprojection", figure = True, errors=True)
displayPlot("lightsub:yprojection", figure = True, errors=True)
displayPlot("lightsub:histogram", figure = True, errors=False, logy=True, fmt = '*')

