import ErrorImage as _ErrorImage
import Manager    as _Manager 
import pyfits     as _pyfits

_manager = _Manager.gm()

def getManager() : 
    '''Get the manager'''
    return _manager

def listImages() : 
    '''Lists images contained in manager'''
    return _manager.imageList()

def listData() : 
    '''Lists data contained in manager'''
    return _manager.dataList()

def resetManager() : 
    '''Resets manager removing all images and data'''
    _manager.reset()

def loadAvi(iden, fileName) : 
    '''Loads avi file as idenXXX, where XXX is the frame number'''
    pass

def load(iden, fileName) : 
    '''Generic load of a file avi, bmp, tiff, jpeg, fits
       Checks file extension and passes to correct method'''
    pass

def loadBmp(iden, fileName) :
    '''Load bmp file'''
    pass

def loadTiff(iden, fileName) : 
    '''Load tiff file'''
    pass

def loadJpeg(iden, fileName) : 
    '''Load jpg file'''
    pass

def loadFits(iden, fileName) : 
    '''Loads fits file with 'fileName' and puts images into image/data manager
       with label 'iden' '''
    # open file and make error image
    f      = _pyfits.open(fileName)
    img    = _ErrorImage.ErrorImage(f[0])
    header = f[0].header
    f.close()
    
    # add image to manager
    _manager.addImage(iden,img,header)    

def saveFits(iden, fileName) : 
    pass

def deleteImage(iden) : 
    '''Removes image 'iden' from the image/data manager'''
    _manager.deleteImage(iden)    
    
def getImage(iden) :
    '''Returns 'iden' ErrorIamge from the image/data manager'''
    return  _manager.getImage(iden) 

def copyImage(iden1, iden2) : 
    '''Copies image from iden1 to iden2'''
    pass



