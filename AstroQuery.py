from   astroquery.simbad import Simbad as _simbad
from   astroquery.vizier import Vizier as _vizier 
import astropy.coordinates as _coord
import astropy.units as _units
import pylab as _pl

class Simbad :
    def __init__(self) :
        pass

    def query(self, ra="0h0m0s",dec="0.0000deg",rad = '0.10000deg') :
        c = _coord.ICRS(ra,dec)
        self.qr = _simbad.query_region(c,radius=rad) # Query result
        print(self.qr)
        return self.qr 
        
class Vizier : 
    def __init__(self) : 
        pass
    
    def query(self, ra="0h0m0s",dec="0.0000deg",rad = '0.10000deg',cat='TYCHO-2') :
        c = _coord.ICRS(ra,dec) 
        self.qr = _vizier.query_region(c,radius=rad,catalog=cat)
        print(self.qr)
        return self.qr
    
    def plot(self) :
        t    = self.qr[self.qr.keys()[0]]
        ra   = t['_RAJ2000']
        dec  = t['_DEJ2000']
        bMag = t['Bmag']
        vMag = t['Vmag']
        rMag = t['Rmag']
        
        print len(ra),len(dec),len(bMag),len(vMag),len(rMag)
        print ra
        print dec
        print bMag
        print vMag
        print rMag        
        
        _pl.plot(dec,ra,"+")
        
        return [bMag,vMag,rMag]
