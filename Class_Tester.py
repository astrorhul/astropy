import pyfits
import numpy as _np
import Cluster_Analysis
import Cluster_Finder
import Photometry_Aperture
import Image_Analysis
import Photometry_Fit

class Class_Tester():

    def __init__(self):
        self.image_path = './data/Rhul_photom/Atlas_Pleione_e1_blue.fit'
        self.image      = pyfits.open(self.image_path)[0].data

    def run_tests_on_all_clusters(self):
        clusters = self.cluster_finder_test()

        for cluster in clusters:
            self.run_tests(_np.array(cluster))

    def cluster_finder_test(self, threshold = 1000, display_plot = True, alpha = 1.0):
        cf       = Cluster_Finder.Cluster_Finder(self.image)
        cf.threshold(threshold)
        clusters = cf.cluster(display_plot, alpha)

        return clusters

    def cluster_analysis_test(self, cluster = 0, display_plot = True, alpha = 1.0):
        ca               = Cluster_Analysis.Cluster_Analysis(self.image, cluster)
        loc              = ca.centre(display_plot)
        dimensions       = ca.dimensions()
        stats            = ca.get_cluster_statistics()
        std_dev          = stats[0]
        rms              = stats[1]
        fwhm             = ca.full_width_half_height_maximum(1000)
        
        ca.intensity()
        ca.plot_intensity_distribution_histogram()       

    def photometry_aperture_test(self, cluster = 0):
        ca      = Cluster_Analysis.Cluster_Analysis(self.image, cluster)
        fwhm    = ca.full_width_half_height_maximum(1000)

        if fwhm == -1: return # Ignored cluster.

        if fwhm == 0: return # Hot pixel so return.

        pa      = Photometry_Aperture.Photometry_Aperture(self.image.shape[0], self.image.shape[1])
        centre  = ca.centre()
        pa.plot_circle((centre[1], centre[0]), fwhm[0], 4.5 * fwhm[0], 6.5 * fwhm[0])
        pixels  = pa.get_circle_masks(centre[1], centre[0], fwhm[0], 4.5 * fwhm[0], 6.5 * fwhm[0])
        
        pa.intensity(self.image, pixels[0])
    
    def photometry_fit_test(self, cluster = 0):
        pf      = Photometry_Fit.Photometry_Fit()
        pa      = Photometry_Aperture.Photometry_Aperture(self.image.shape[0], self.image.shape[1])
        ca      = Cluster_Analysis.Cluster_Analysis(self.image, cluster)
        centre  = ca.centre()
        
        fwhm    = ca.full_width_half_height_maximum(1000)
        
        pixels  = pa.get_circle_masks(centre[1], centre[0], fwhm[0], 4.5 * fwhm[0], 6.5 * fwhm[0])
        
        stats   = ca.get_cluster_statistics()
        x_sigma, y_sigma = stats[0][0], stats[0][1]
        
        x, y = pixels[0][0], pixels[0][1]
        
        x_centre, y_centre = centre[1], centre[0]
        
        intensities = self.image[y, x]
        
        data = pf.plot_2D_gaussian(intensities, x, y, x_centre, y_centre, x_sigma, y_sigma)
        
    def image_analysis_test(self, cluster = 0):
        ia = Image_Analysis.Image_Analysis(self.image)
        ia.plot_image_intensity_distribution_histogram()
        clusters = self.cluster_finder_test(display_plot = False)
        
        cluster_intensities = []
        
        for cluster in clusters:
            ca = Cluster_Analysis.Cluster_Analysis(self.image, _np.array(cluster))
            cluster_intensities.append(ca.intensity())
        
        intensities = _np.array(cluster_intensities)
        
        ia.plot_cluster_intensity_distribution_histogram(intensities)
        ia.mean()
        ia.mode()
        ia.median()

    def run_tests(self, cluster):
        print '\nCluster_Analysis test results'
        self.cluster_analysis_test(cluster, True, .5)

        print '\nAperture_Photometry test results'
        self.photometry_aperture_test(cluster)
        
        print '\nPhotometry_Fit test results'
        self.photometry_fit_test(cluster)
        
        print '\nImage_Analysis test results'
        self.image_analysis_test(cluster)
