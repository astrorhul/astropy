import numpy   as _np
import os      as _os
import Globals as _Globals
import pyfits  as _pf

class Image_Simulator():
    '''
    A class which can be used to create a 2D numpy array mimicking a .fits file.
    '''

    def __init__(self, star_count = 10, image_size = (1200, 700), background = 300, xy_variation = 100, max_amp = 20000, min_amp = 5000, std_max = 5.1, std_min = 4.9, debug_print = False):
        '''
        Initialises an Image_Simulator object which enables the generation of 2d numpy arrays mimicking a .fits file.
        star_count           : The number of stars the image should contain.
        image_size           : The size of the grid in (x, y) dimensions.
        background_threshold : The level of background noise the image should have, approximatley will vary by 50 either side of the mean.
        xy_variation         : The covariance matrix, this effects the overall spread of the data.
        max_amp              : The maximum amplitude that a star should have.
        min_amp              : The minimum amplitude that a star should have.
        std_max              : The maximum standard deviation a star should have.
        std_min              : The minimum standard devaition a star should have.
        noise                : Whether or not the background should have noise, default is True.
        '''
        
        self.debug_print          = debug_print
        self.star_count           = star_count
        self.image_size           = image_size
        self.background_threshold = [background, background]
        self.xy_variation         = [[xy_variation, 0], [0, xy_variation]]
        self.max_amp              = max_amp
        self.min_amp              = min_amp
        self.std_min              = std_min
        self.std_max              = std_max

    def __elliptical_gaussian_2D(self, amplitude, x, y, x_centre, y_centre, x_sigma, y_sigma, theta): # Maybe put in a utilities function?  Need gauss functions elsewhere. Normalise?
    
        a     = (_np.cos(theta)**2) / (2 * x_sigma**2) + (_np.sin(theta)**2) / (2 * y_sigma**2)
        b     = -(_np.sin(2 * theta)) / (4 * x_sigma**2) + (_np.sin(2 * theta)) / (4 * y_sigma**2)
        c     = (_np.sin(theta)**2) / (2 * x_sigma**2) + (_np.cos(theta)**2) / (2 * y_sigma**2)
        gauss = amplitude * _np.exp(- (a * (x - x_centre)**2) - (c * (y - y_centre)**2)  - (2. * b * (x - x_centre) * (y - y_centre)))
        
        if self.debug_print:
            print '-------------------------------------------'
            print 'Image_Simulator.__elliptical_gaussian_2D()>'
            print 'x_sigma                                    ', x_sigma
            print 'y_sigma                                    ', y_sigma
            print 'x_centre                                   ', x_centre
            print 'y_centre                                   ', y_centre
            print 'a:                                         ', a
            print 'b:                                         ', b
            print 'c:                                         ', c
            print 'gauss                                      ', gauss
            
        return gauss
        
    def get_parameters(self, x, y):
        '''
        Either loads or randomly generates parameters used to generate a star.
        x             : The x coordinates of the image the clusters will be placed on (used for bounding).
        y             : The y coordinates of the image the clusters will be placed on (used for bounding).
        std_max       : The maximum standard deviation that should be used in the cluster generation.
        std_min       : The minimum standard deviation that should be used in the cluster generaton.
        max_amp       : The maximum amplitude that should be used in the cluster generation.
        min_amp       : The minimum amplitude that should be used in the cluster generation.
        '''
        
        loc      = _np.array([x.max() * _np.random.random_sample(self.star_count), y.max() * _np.random.random_sample(self.star_count)])
        std      = (self.std_max - self.std_min) * _np.random.random_sample((self.star_count, 2)) + self.std_min
        amp      = (self.max_amp - self.min_amp) * _np.random.random_sample((self.star_count, 1)) + self.min_amp
        theta    = 2 *_np.pi * _np.random.random_sample((self.star_count, 1))
            
        return loc, std, amp, theta
        
    def generate_image(self, noise = True):
        '''
        Generates an image constructed using a 2D numpy array.  The image will be a range of values in a uniform distribution, unless noise is set to False, in which case the image will be zero valued.
        mean      : The level of background noise the image should have, approximatley will vary by 50 either side of the mean.
        cov       : The covariance matrix, this effects the overall spread of the data.
        grid_size : The size of the grid in (x, y) dimensions.
        noise     : Whether or not the background should have noise, default is True.
        '''
        
        x       = _np.arange(0, self.image_size[0], 1)  # x size
        y       = _np.arange(0, self.image_size[1], 1) # y size
        xx, yy  = _np.meshgrid(x, y)
        z       = _np.zeros((len(y), len(x)))
        
        if noise: # This adds background noise to z
            z += _np.random.multivariate_normal(self.background_threshold, self.xy_variation, self.image_size[0] * self.image_size[1] / 2).reshape((self.image_size[1], self.image_size[0]))
        
        gain = 5 * _np.random.random_sample() # What should the gain range be?
        
        if self.debug_print:
            print '-------------------------------------------'
            print 'x:                                         ', x
            print 'y:                                         ', y
            print 'xx:                                        ', xx
            print 'yy:                                        ', yy
            print 'z:                                         ', z
            
        return xx, yy, z, gain

    def generate_stars(self, image, x, y, loc, amp, std, theta):
        '''
        Given a set of parameters stars are generated using a 2D gaussian function and then placed at specifed locations into a 2D numpy array.
        image : The 2D numpy array the stars are to be placed in.
        x : The x coordinates.
        y : The y coordinates.
        loc : An array of the central coordinates for the gaussians to be placed in.
        cluster_count : The number of stars to be generated.
        amp : An array of the amplitudes of each star to be created.
        std : An array of the standard deviations of each star to be created.
        theta : An array of the rotations of each star to be created in a clockwise rotation.
        '''
         
        for i in range(0, self.star_count):
            x_centre = loc[0][i]
            y_centre = loc[1][i]
            
            image += self.__elliptical_gaussian_2D(amp[i], x, y, x_centre, y_centre, std[i, 0], std[i, 1], theta[i])
            i     += 1
            
        if self.debug_print:
                print '-------------------------------------------'
                print 'Image_Simulator.generate_clusters()>       '
                print 'x_coords:                                  ', loc[0]
                print 'y_coords:                                  ', loc[1]
                print 'Standard deviation:                        ', std
                print 'Amplitude (max, min):                      ', amp.max(), amp.min()

        return image
        
    def write_simulation_to_file(self, image, x, y, amp, std_x, std_y, star_params_filename = 'Image_Simulation_cluster_params'):
        '''
        Writes the data used to create each cluster to a .csv file.  By default the filename will be saved as 'Image_Simulation.csv' in the ./data/Simulations directory.
        x     : A numpy array of x coordinates of the stars.
        y     : A numpy array of y coordinates of the stars.
        amp   : A numpy array of amplitudes of the stars.
        std_x : A numpy array of the x standard deviations used to generate the stars.
        std_y : A numpy array of the y standard deviations used to generate the stars.
        '''
        
        if not _os.path.isdir(_Globals.simulation_directory):
            _os.mkdir(_Globals.simulation_directory)
            
        star_params_filename += '.csv'
        
        if self.debug_print:
            print '-------------------------------------------'
            print 'Image_Simulator.write_simulation_to_file() '
            print x
            print y
            print amp
            print std_x
            print std_y

        _np.savetxt(_os.path.join(_Globals.simulation_directory, star_params_filename), _np.array([x, y, amp, std_x, std_y], dtype=float).transpose(), delimiter = ',')

    def create_fits_file(self, image, gain, image_filename = 'Image_Simulation'):
        prihdr          = _pf.Header()
        prihdr['EGAIN'] = gain
        prihdu          = _pf.PrimaryHDU(image, header = prihdr)
        hdulist         = _pf.HDUList([prihdu])
        
        if not _os.path.isdir(_Globals.simulation_directory):
            _os.mkdir(_Globals.simulation_directory)
            
        image_filename += '.fits' # Need to handle file extensions
        
        hdulist.writeto(_os.path.join(_Globals.simulation_directory, image_filename), clobber = True) # Only wish to preserve the most recent sim if no name is provided.
def generate_images(image_count = 30, average_count = 100, image_size = (1200, 700), amplitude = 50000):
    for j in range(1, average_count):
        for i in range(1, image_count):
            i_s                  = Image_Simulator(star_count = i, image_size = image_size, max_amp = amplitude, min_amp = amplitude)
            x, y, z, gain        = i_s.generate_image()
            loc, std, amp, theta = i_s.get_parameters(x, y)
            image                = i_s.generate_stars(z, x, y, loc, amp, std, theta)
            i_s.write_simulation_to_file(image, loc[0], loc[1], amp.reshape((i,)), std[:,0], std[:,1], star_params_filename = 'alg_test_params_' + str(i) + '_' + str(j))
            i_s.create_fits_file(image, gain, image_filename = 'alg_test_image_' + str(i) + '_' + str(j))
def test_image_simulator(star_count, image_size, background, xy_variation, max_amp, min_amp, std_max, std_min, noise, debug_print):
    import pylab as _pl
    
    i_s                  = Image_Simulator(star_count, image_size, background, xy_variation, max_amp, min_amp, std_max, std_min, debug_print)
    x, y, z, gain        = i_s.generate_image(noise)
    loc, std, amp, theta = i_s.get_parameters(x, y)
    image                = i_s.generate_stars(z, x, y, loc, amp, std, theta)
    i_s.write_simulation_to_file(image, loc[0], loc[1], amp.reshape((star_count,)), std[:,0], std[:,1])
    i_s.create_fits_file(image, gain)
    
    # Plotting
    fig = _pl.gcf()
    _pl.clf()
    _pl.subplot(2, 2, 1)
    _pl.xlim(x.min(), x.max())
    _pl.ylim(y.max(), y.min())
    _pl.imshow(_np.log10(image), cmap = 'gray')
    _pl.subplot(224)
    _pl.imshow(z)
    _pl.subplot(221)
    
    return image, x, y, z

def test(star_count = 10, threshold = 1000, image_size = (1200, 700), background_threshold = 300, xy_variation = 100, max_amp = 20000, min_amp = 5000, std_max = 5.1, std_min = 4.9, noise = True, debug_print = False):
    import Cluster_Finder      as _Cluster_Finder
    import Image_Analysis      as _Image_Analysis
    import pylab               as _pl
    
    # Make simulated image.
    image, x, y, z = test_image_simulator(star_count, image_size, background_threshold, xy_variation, max_amp, min_amp, std_max, std_min, noise, debug_print)

    # Basic image analysis 
    ia = _Image_Analysis.Image_Analysis(image, debug_print)
    print ia.stats()

    # Cluster analysis 
    cf = _Cluster_Finder.Cluster_Finder(image)
    cf.set_threshold(threshold)
    clusters = cf.cluster()
    
    # Plotting 
    for cluster in clusters:
        _pl.plot(cluster[:, 1], cluster[:, 0], '+', c = 'y', alpha = 1)
        
    _pl.subplot(2, 2, 2)
    _pl.imshow(_np.log10(image), cmap = 'gray')
    
    _pl.xlim(x.min(), x.max())
    _pl.ylim(y.max(), y.min()) # Keep orientation the same as default imshow()
    _pl.subplot(2, 2, 3)
    _pl.hist(_np.log(image.flatten()))
    
    _pl.show()
    
    print '-------------------------------------------'
    print 'Number of clusters found:                  ', len(clusters)