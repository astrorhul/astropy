from scipy import ndimage
import matplotlib.pyplot as plt
from astropy.io import fits
import numpy as np
from scipy import optimize
import math
import matplotlib
import os
import os.path
import datetime
import matplotlib.dates
from scipy.misc import factorial

plt.ion()

#prints header/data info to the screen of input .fit file
def hduInfo(imageFile):
	hduList = fits.open(imageFile)
	hduList.info()
	
#called in other functions to save a .FITS file. Caller function 
#can take (fileName = "")
def saveFits(kwargs, dataToSave):

	if 'fileName' in kwargs:
		#print"File Saved as: %s" % (kwargs['fileName'])
		fits.writeto(kwargs['fileName'], dataToSave)


#called in other functions to save the current figure as a .png
#caller function takes (fileName = "").		
def saveFig(kwargs):
	
	if 'fileName' in kwargs:
		plt.savefig(kwargs['fileName'])
		print"File Saved as: %s" % (kwargs['fileName'])

def figPlot(kwargs):
        if 'figNum' in kwargs:
                plt.figure(kwargs['figNum'])

#Shows the input .fit file on screen. Can then save as .png etc
def viewFits(imageFile, **kwargs):

	imageData = fits.getdata(imageFile)

        figPlot(kwargs)
	plt.imshow(imageData, cmap = 'gray')
        plt.xlabel('Pixel Value')
        plt.ylabel("No. of Pixels")
	plt.colorbar()
	plt.show()
	saveFig(kwargs)


#Takes the log of the input file data and shows on screen
def viewLogFits(imageFile, **kwargs):

	imageData = fits.getdata(imageFile)
	logImageData = np.log10(imageData)

        figPlot(kwargs)
	plt.imshow(logImageData, cmap = 'gray')
        plt.xlabel('Pixel Value')
        plt.ylabel("No. of Pixels")
	plt.colorbar()
	plt.show()
	saveFig(kwargs)


#plots the data contained in input file into a histogram
def absHistMkr(inputFile, **kwargs):

	figPlot(kwargs)

	upperLim = brightestPixel(inputFile)

	imageData = fits.getdata(inputFile)
	histogram = plt.hist(imageData.flat, upperLim, histtype = 'step')
	plt.yscale('log')

        plt.xlabel('Pixel Value')
        plt.ylabel("No. of Pixels")

	plt.show()
	saveFig(kwargs)

	
#plots the log of the data from the input file into a histogram
def logHistMkr(imageFile, numBins, **kwargs):

	imageData = fits.getdata(imageFile)
	logImageData = np.log10(imageData)
	histogram = plt.hist(logImageData.flat, numBins, histtype = 'step')
        plt.xlabel('Log of Pixel Value')
        plt.ylabel("No. of Pixels")
	plt.yscale('log')
	plt.show()
	saveFig(kwargs)
		
#plots the input file and its histogram to the screen. 
#Allows user to specify if they want the log image/hist combo
#or the absolute. 
#Using 'log' or 'abs' in logOrAbs when calling the function allows the user 
#to choose if they want the log or absolute data.
def imAndHist(imageFile, logOrAbs, numBins):
	
	imageData = fits.getdata(imageFile)
	logImageData = np.log10(imageData)
	
	if logOrAbs == 'log':
		plt.imshow(logImageData, cmap = 'gray')
		plt.colorbar()
		plt.show()
		
		plt.figure(2)
		histogram = plt.hist(logImageData.flat, numBins, histtype = 'step')
		plt.yscale('log')
		plt.show()
	
	elif logOrAbs == 'abs':
		plt.imshow(imageData, cmap = 'gray')
		plt.colorbar()
		plt.show()
		
		plt.figure(2)
		histogram = plt.hist(imageData.flat, numBins, histtype = 'step')
		plt.yscale('log')
		plt.show()
		
#This function iterates through the 2-D array of pixel values that make up
#image. It makes a note of the current highest pixel value and when it is
#finished iterating through the image, it prints out the highest pixel value
#present in the .fit image. Useful to quickly see if there are saturated pixels
def brightestPixel(imageFile):

	imageData = fits.getdata(imageFile)
	xVal, yVal, maxVal = 0,0,0

	for y in range(imageData.shape[0]):
		for x in range(imageData.shape[1]):	
		
			brightness = imageData[y][x]
			
			if brightness > maxVal:
			
				maxVal = brightness
				yVal = y
				xVal = x
				currentBrightest = brightness
	
	#print "Brightest pixel located at: %s \nPixel value is: %s" % ((xVal, yVal), currentBrightest)

	return currentBrightest 

#Takes an input .fit and converts all pixel values above a user specified threshold
#to black and all pixels below this to white. Makes a highly contrasted images
#with bright areas showing black and dark areas showing white.
#User can specify that they want to save the file by passing in 
# fileName = 'whatEverNameYouWant' when calling the function.
def legacyShowStars(imageFile, upperLimit, **kwargs):
	
	BLACK = 0
	WHITE = 1
	imageData = fits.getdata(imageFile)
	
	for y in range(imageData.shape[0]):
		for x in range(imageData.shape[1]):
		
			pixVal = imageData[y][x]
			
			if pixVal >= upperLimit:
				imageData[y][x] = BLACK
			else:
				imageData[y][x] = WHITE		
	
	saveFits(kwargs, imageData)
	plt.imshow(imageData, cmap = 'gray')
	plt.draw()	
		
#Takes a user input threshold and looks at each pixel in the image. If
#the image has a value higher than that set by the threshold it sets the value
#to 1, if lower it sets value to 0. This lets the user see clearly 
#which pixels have a brightness higher than the threshold allowing for star
#detection.
def showStars(imageFile, upperLimit, invertOrNot, **kwargs):

	imageData = fits.getdata(imageFile)
	binaryImage = (1.0*(imageData > upperLimit))
	
	if invertOrNot == True:
		binaryInverter(binaryImage)
		saveFits(kwargs, binaryImage)
		
	elif invertOrNot == False:
		plt.imshow(binaryImage, cmap = 'gray')
		saveFits(kwargs, binaryImage)
		
#Switches the values of a binary image generated above. 
#Used if you want to view black stars on a white background.
def binaryInverter(imageData):

	for y in range(imageData.shape[0]):
		for x in range(imageData.shape[1]):
			
			pixelVal = imageData[y][x]
			
			if pixelVal == 0:
				imageData[y][x] = 1
				
			elif pixelVal == 1:
				imageData[y][x] = 0
				
	plt.figure(2)
	plt.imshow(imageData, cmap = 'gray')
	plt.show()

def gaussian(x, a, b, c):
	return a*np.exp(-(x-b)**2/(2*c**2))

def poisson(k, l):
	return (l**k/factorial(k)) * np.exp(-l)

def curveFitTester(inputFile):

	binaryData = curveFit(inputFile, printText = False, standAlone = True)

	print binaryData

	plt.imshow(binaryData)

	

#Fits a Gaussian to the histogram of the pixel values of an image.
#This Gaussian allows the calculation of the threshold to use to separate
#the stars from the space in between them	
def curveFit(inputFile, printText, standAlone, **kwargs):

	numBins = brightestPixel(inputFile)
	imageData = fits.getdata(inputFile)

	if printText == True:
		histogramData = plt.hist(imageData.flat, numBins, histtype = 'step')

	if printText == False:
		histogramData = np.histogram(imageData.flat, numBins)

	y = histogramData[0]
	x=[]
	
	for i in xrange(len(histogramData[1])-1):
		x.append(0.5* (histogramData[1][i] + histogramData[1][i+1]))

	ampEst = 0
	
	for imdat in range(len(histogramData[0])):
		if histogramData[0][imdat] >= ampEst:
			ampEst = histogramData[0][imdat]
			yMeanEst = imdat

	meanEst = x[yMeanEst]

	popt, pcov = optimize.curve_fit(gaussian, x, y, p0 = [ampEst, meanEst, 10])
	x_fit = np.linspace(x[0], x[-1], numBins)
	y_fit = gaussian(x_fit, *popt)


	if printText == True:
		print "The amplitude of the gassian is : ", popt[0]
		print "The mean of the gaussian is : ", popt[1]
		print "Sigma isfn=: ", popt[2]	
		plt.plot(x_fit, y_fit, lw = 2, color = "r")
		plt.yscale('log')
		plt.ylim(1, 1e+07)

	maxVal = 0
	for k in range(len(y_fit)):
		yVal= y_fit[k]
		if yVal > maxVal:
			maxVal = yVal
			peakPos = k
	
	for j in range(len(y_fit)):
		if j>= peakPos and y_fit[j] <= 1:
			xMinVal = j
			break

	if printText == True:
		print "Threshold for count stars is %s: " % (x_fit[xMinVal])

	binaryImage = (1.0*(imageData > (x_fit[xMinVal])))
	saveFits(kwargs,binaryImage)

	if standAlone == False:
		return binaryImage
		
#Takes in a binary image file where a threshold pixel value to
#determine what constitutes as a star has already been decided.
#Counts the total number of 'objects' in the image. 
#Produces a 2D array containing the x-y values of  the pixels
#that make up each object. This functions caluculates the mean pixel
#value of the star, the standard deviation of the pixels and the centre
#of brightness.

def countStars(origionalImage, printText, standAlone):


	referenceBinaryData = curveFit(origionalImage, printText = False, standAlone = False)
	imageData = curveFit(origionalImage, printText = False, standAlone = False)
	
	#imageData = fits.getdata(binaryFile)
	#referenceBinaryData = fits.getdata(binaryFile)
	origionalImageData = fits.getdata(origionalImage)
	logImageData = np.log10(origionalImageData)
	result = 0
	masterPixelList =[]

	for y in range(imageData.shape[0]):
		for x in range(imageData.shape[1]):
			if imageData[y][x] == 1:
			
				singleStarPix =[]
				result +=1

				fill(imageData, imageData.shape[1], imageData.shape[0], y, x, singleStarPix)
				
				masterPixelList.append(singleStarPix)

	if printText == True:
		print "%s Objects Detected In Image:" %(result)


	#cobList = list containing centre of brightness of each star
	cobList = []
	#integrated brightness for each star
	totalBrightList = []
	pixelNoList = []
	cobAndNoOfPix = []
	starCounter = 0


	cobAndTotalBrightness = []

	
	for k in range(len(masterPixelList)):
	
		noOfPixelsPerStar = 0
		totalBrightnessPerStar = 0
		xCobNumerator = 0
		yCobNumerator = 0
		pixValList = []
		errorList = []
	
		for l in range(len(masterPixelList[k])):
			noOfPixelsPerStar += 1
			yVal, xVal = masterPixelList[k][l]
			pixVal = origionalImageData[yVal][xVal]
			
			pixValList.append(pixVal)
			totalBrightnessPerStar += pixVal
			
			xCobNumerator += (pixVal * xVal)
			yCobNumerator += (pixVal * yVal)
		
		if noOfPixelsPerStar >= 10:


			starCounter += 1
			xValCob = int((xCobNumerator / totalBrightnessPerStar))
			yValCob = int((yCobNumerator / totalBrightnessPerStar))
		
			cobPos = (xValCob, yValCob)
			cobList.append(cobPos)
			totalBrightList.append(totalBrightnessPerStar)

			cobAndBrightness = [(xValCob, yValCob), totalBrightnessPerStar]

			cobAndTotalBrightness.append(cobAndBrightness)


			pixelNoList.append(noOfPixelsPerStar)

			cobAndNoOfPix.append((noOfPixelsPerStar, cobPos))
		
			meanPixVal, stdev = meanAndStDev(pixValList)

			if printText == True:
		
				print"Star No: %s \n%s pixels" %(starCounter, noOfPixelsPerStar)
				print "Integrated brightness: ", totalBrightnessPerStar
				print "Mean value: ", meanPixVal
				print "Standard Deviation: ", stdev
				print "Effective centre of mass located at: ", cobPos
				print "------------------------------------------"
	
	totalBrightness = 0
	weightList = []
	
	for b in range(len(totalBrightList)):
		totalBrightness += totalBrightList[b]
	
	for c in range(len(totalBrightList)):
		brightWeight = totalBrightList[c] / totalBrightness
		weightList.append(brightWeight)
	
	if printText == True:
		fig = plt.figure(1)
		ax0 = fig.add_subplot(111, autoscale_on = False, xlim = (0, 1530), ylim =(1020,0))
		plt.imshow(logImageData, cmap = 'gray') #logImageData for plotting on log image
		ax = fig.add_subplot(111, autoscale_on = False, xlim = (0, 1530), ylim =(1020,0)) 

		for d in range(len(weightList)):
	
			xPos, yPos = cobList[d]
			width = (weightList[d]*1000)
			height = (weightList[d]*1000)
		
			el = matplotlib.patches.Ellipse((xPos,yPos), width, height, fill = False ,edgecolor = 'red')
			ax.add_patch(el)

			ax.text(xPos + 10, yPos + 10, d+1, fontsize = 15, color = 'white', alpha = 0.2)
		
		fig2 = plt.figure(2)

		plt.imshow(referenceBinaryData)

	if standAlone == False:

		return pixelNoList, cobAndNoOfPix, cobAndTotalBrightness, referenceBinaryData

def pairMatcher(cobAndPix, cobAndPixTwo, numberOfObjToAlign, printText):

	reducedCobNPix = []
	reducedCobNPixTwo = []
	distancesList = []
	starMatchesList = []		
	matchedCobListOne = []
	matchedCobListTwo = []
	
	cobPixSorted = sorted(cobAndPix, reverse = True)
	cobPixSortedTwo = sorted(cobAndPixTwo, reverse = True)

	for i in range(numberOfObjToAlign):
		reducedCobNPix.append(cobPixSorted[i])
		reducedCobNPixTwo.append(cobPixSortedTwo[i])
		if printText == True:

			print "--------------------------------------------"
			print numberOfObjToAlign, " largest objects in image one: "
			print reducedCobNPix

			print "--------------------------------------------"
			print numberOfObjToAlign, "largest objects in image two: "
			print reducedCobNPixTwo
			print "--------------------------------------------"
	
	for j in range(len(reducedCobNPix)):
		xValSource = reducedCobNPix[j][1][0]
		yValSource = reducedCobNPix[j][1][1]

		for k in range(len(reducedCobNPixTwo)):
			xValTwo = reducedCobNPixTwo[k][1][0]
			yValTwo = reducedCobNPixTwo[k][1][1]

			distance = int(math.sqrt( (xValSource - xValTwo)**2 + (yValSource - yValTwo)**2 ))
			distancesList.append((distance, j, k))

	sortedDifferences = sorted(distancesList)

	for l in range(numberOfObjToAlign):
		starMatchesList.append(sortedDifferences[l])

	for m in range(len(starMatchesList)):
		starNumberOne = starMatchesList[m][1]
		starNumberTwo = starMatchesList[m][2]

		matchedCobListOne.append(reducedCobNPix[starNumberOne][1])
		matchedCobListTwo.append(reducedCobNPixTwo[starNumberTwo][1])

	if printText == True:
		print "Matched Objects In Image One:"
		print matchedCobListOne
		print "Matched Objects In Image Two"
		print matchedCobListTwo
	
	return matchedCobListOne, matchedCobListTwo, 

def imageAlign(imageOne, imageTwo, binaryImageOne, binaryImageTwo, cobListOne, cobListTwo, printText, **kwargs):

	imageOneData = fits.getdata(imageOne)
	imageTwoData = fits.getdata(imageTwo)

	thetaListOne = []
	thetaListTwo = []
	xPos = []
	yPos = []
	xPosTwo = []
	yPosTwo = []

	for i in range(len(cobListOne)-1):
		for j in range (1):
			if (cobListOne[i][j] < cobListOne[i+1][j]) and (cobListOne[i][j+1] > cobListOne[i+1][j+1]):
				adjacent = float(cobListOne[i+1][j] - cobListOne[i][j])
				opposite = float(cobListOne[i][j+1] - cobListOne[i+1][j+1])

				thetaRad = math.atan(opposite/adjacent)
				thetaDeg = math.degrees(thetaRad)
				thetaListOne.append(thetaDeg)

				#For Debugging
				#print "Class 1:"
				#print "Star No. ", i+1, " (",cobListOne[i][j], cobListOne[i][j+1], ")"
				#print "Star No. ", i+2, " (", cobListOne[i+1][j], cobListOne[i+1][j+1], ")"

				#print "adjacent is: ", adjacent
				#print "opposite is: ", opposite
				#print "op/ad", opposite/adjacent

				#print "Theta is: ", thetaDeg
				#print " --------------------------------------------------------"

			elif (cobListOne[i][j] > cobListOne[i+1][j]) and (cobListOne[i][j+1] > cobListOne[i+1][j+1]):
				adjacent = float(cobListOne[i][j] - cobListOne[i+1][j])
				opposite = float(cobListOne[i][j+1] - cobListOne[i+1][j+1])

				thetaRad = math.atan(opposite/adjacent)
				thetaDeg = 180 -math.degrees(thetaRad)

				thetaListOne.append(thetaDeg)

			elif (cobListOne[i][j] > cobListOne[i+1][j]) and (cobListOne[i][j+1] < cobListOne[i+1][j+1]):
				adjacent = float(cobListOne[i][j] - cobListOne[i+1][j])
				opposite = float(cobListOne[i+1][j+1] - cobListOne[i][j+1])

				thetaRad = math.atan(opposite / adjacent)
				thetaDeg = math.degrees(thetaRad)

				thetaListOne.append(thetaDeg)

			elif (cobListOne[i][j] < cobListOne[i+1][j]) and (cobListOne[i][j+1] < cobListOne[i+1][j+1]):
				adjacent = float(cobListOne[i+1][j] - cobListOne[i][j])
				opposite = float(cobListOne[i+1][j+1] - cobListOne[i][j+1])

				thetaRad = math.atan(opposite/adjacent)
				thetaDeg = 180 - math.degrees(thetaRad)

				thetaListOne.append(thetaDeg)

			else:
				thetaDeg = 0
				thetaListOne.append(thetaDeg)

	for k in range(len(cobListTwo)-1):
		for l in range (1):
			if (cobListTwo[k][l] < cobListTwo[k+1][l]) and (cobListTwo[k][l+1] > cobListTwo[k+1][l+1]):
				adjacent = float(cobListTwo[k+1][l] - cobListTwo[k][l])
				opposite = float(cobListTwo[k][l+1] - cobListTwo[k+1][l+1])

				thetaRad = math.atan(opposite/adjacent)
				thetaDeg = math.degrees(thetaRad)

				thetaListTwo.append(thetaDeg)
			
			elif (cobListTwo[k][l] > cobListTwo[k+1][l]) and (cobListTwo[k][l+1] > cobListTwo[k+1][l+1]):
				adjacent = float(cobListTwo[k][l] - cobListTwo[k+1][l])
				opposite = float(cobListTwo[k][l+1] - cobListTwo[k+1][l+1])

				thetaRad = math.atan(opposite/adjacent)
				thetaDeg = 180 -math.degrees(thetaRad)

				thetaListTwo.append(thetaDeg)

			elif (cobListTwo[k][l] > cobListTwo[k+1][l]) and (cobListTwo[k][l+1] < cobListTwo[k+1][l+1]):
				adjacent = float(cobListTwo[k][l] - cobListTwo[k+1][l])
				opposite = float(cobListTwo[k+1][l+1] - cobListTwo[k][l+1])

				opOverAdj = opposite/ adjacent

				thetaRad = math.atan(opOverAdj)
				thetaDeg = math.degrees(thetaRad)

				thetaListTwo.append(thetaDeg)

			elif (cobListTwo[k][l] < cobListTwo[k+1][l]) and (cobListTwo[k][l+1] < cobListTwo[k+1][l+1]):
				adjacent = float(cobListTwo[k+1][l] - cobListTwo[k][l])
				opposite = float(cobListTwo[k+1][l+1] - cobListTwo[k][l+1])

				thetaRad = math.atan(opposite/adjacent)
				thetaDeg = 180 - math.degrees(thetaRad)

				thetaListTwo.append(thetaDeg)
			else:
				thetaDeg = 0
				thetaListTwo.append(thetaDeg)

	#gemerates two arrays of x and y positions respectively
	for m in range(len(cobListOne)):
		xPos.append(cobListOne[m][0])
		yPos.append(cobListOne[m][1])
		xPosTwo.append(cobListTwo[m][0])
		yPosTwo.append(cobListTwo[m][1]) 

	mserrXInit = 0
	mserrYInit = 0
	dividerPos = 0
	mserrThetaInit = 0
	dividerTheta = 0		

	for n in range(len(xPos)):
		mserrXInit += float(((xPos[n] - xPosTwo[n])))
		mserrYInit += float(((yPos[n] - yPosTwo[n])))
		dividerPos += 1

	mserrX = mserrXInit / dividerPos
	mserrY = mserrYInit / dividerPos

	#Mean absolute error for theta
	for o in range(len(thetaListOne)):

		mserrThetaInit += float((thetaListOne[o] - thetaListTwo[o]))
		dividerTheta += 1

	mserrTheta = mserrThetaInit / dividerTheta
	imageRot = ndimage.rotate(imageTwoData, mserrTheta, reshape = False)
	shiftedImageTwo = np.zeros((1020, 1530))

	amountToShiftY = mserrY
	amountToShiftX = mserrX

	for y in range(imageOneData.shape[0]):
		if (y - amountToShiftY) <= 1019 and (y + amountToShiftY) >= 0:		
			for x in range(imageOneData.shape[1]):
				if (x - amountToShiftX) <= 1529 and (x - amountToShiftX) >=0:
					shiftedImageTwo[y][x] = imageRot[y - amountToShiftY][x- amountToShiftX]

	print "Mean Error in x = ", mserrX	
	print "Mean Error in y = ", mserrY
	print "Mean Error in theta = ", mserrTheta

	if printText == True:
		stackedIm = (imageOneData + shiftedImageTwo)/2

		plt.imshow(imageOneData, cmap = "gray")
		plt.figure(2)	
		plt.imshow(imageTwoData, cmap = "gray")
		plt.figure(3)
		plt.imshow(shiftedImageTwo, cmap = "gray")
		plt.figure(4)
		plt.imshow(stackedIm, cmap = "gray")

	saveFits(kwargs, shiftedImageTwo)

def multiImAlign(folderName, noOfObjToAlign):

	fileStrings = os.listdir(folderName)
	fileStrings.sort()

	alignedImPath = folderName + "alignedImages/"
	os.makedirs(alignedImPath)

	print "ALIGNED IMAGES WILL SAVE TO: ", alignedImPath

	for i in range(len(fileStrings)- 1):
		
		print "---------------------------------------------------------"
		print "PROGRESS: Aligning Image", i+1, "out of ", (len(fileStrings)-1)

		pixelNoListSource, cobAndPix, cobAndBright, binaryImageSource = countStars(folderName + fileStrings[0], printText = False, standAlone = False)

		pixelNoListTwo, cobAndPixTwo, cobAndBrightTwo, binaryImageTwo = countStars(folderName + fileStrings[i+1], printText = False, standAlone = False)

		matchingStarCobListOne, matchingStarCobListTwo = pairMatcher (cobAndPix, cobAndPixTwo, noOfObjToAlign, printText = False)

		imageAlign(folderName + fileStrings[0], folderName + fileStrings[i+1], binaryImageSource, binaryImageTwo, matchingStarCobListOne, matchingStarCobListTwo, printText = False, fileName = alignedImPath + `i+1` + ".fit")

	return alignedImPath

def stacker(alignedImPath, folderPath, fileName):


	print"-------------------------------------------------"
	print "Beginning Stack"
	
	fileStrings = os.listdir(alignedImPath)
	fileStrings.sort()

	stackedImagePath = folderPath + "stackedImage/"
	os.makedirs(stackedImagePath)

	fileDataVars = {}

	stackedIm = np.zeros((1020, 1530))
	dividers = np.zeros((1020, 1530))

	numerator = 0
	denominator = 0

	for i in range(len(fileStrings)):
		fileDataVars[i] = fits.getdata(alignedImPath + fileStrings[i])

	for j in range(len(fileStrings)):

		print "Stacking Image %s with reference Image" %(j+1) 

		for y in range(fileDataVars[j].shape[0]):
			for x in range(fileDataVars[j].shape[1]):

				if fileDataVars[j][y][x] <= 0:

					stackedIm[y][x] += 0
					dividers[y][x] += 0

				if fileDataVars[j][y][x] > 0:

					stackedIm[y][x] += fileDataVars[j][y][x]
					dividers[y][x] += 1

	for y2 in range(dividers.shape[0]):
		for x2 in range(dividers.shape[1]):
			if dividers[y2][x2] == 0:
				dividers[y2][x2] = 1
					
	averagedIm = stackedIm / dividers

	fits.writeto(stackedImagePath + fileName, averagedIm)
	print "Stacking Complete"
	print "Composite Image Saved To: ", stackedImagePath + fileName

	#for debugging
	#print stackedIm
	#print dividers
	#print averagedIm

def alignNStack(unAlignedFolder, outPutFileName, noOfObjToAlign):
	
	alignedImPath = multiImAlign(unAlignedFolder, noOfObjToAlign)
	stacker(alignedImPath, unAlignedFolder, outPutFileName)


def dFrameStacker(folderName, **kwargs):

	fileStrings = os.listdir(folderName)
	denominator = 0
	numerator = 0

	for i in range(len(fileStrings)-1):
		numerator += fits.getdata(folderName + fileStrings[i])
		denominator += 1

	stackedIm = numerator / denominator
	plt.imshow(stackedIm, cmap = 'gray')

	saveFits(kwargs, stackedIm)

def dFramSubtraction(darkIm, stackedIm, **kwargs):

	imOfInterest = fits.getdata(stackedIm)
	darkFrame = fits.getdata(darkIm)

	reducedIm = imOfInterest - darkFrame

	plt.imshow(reducedIm, cmap = 'gray')
				
	saveFits(kwargs, reducedIm)

#Algorithm which is used to 'fill' the potential stars by iterating through
#the bright pixels it consists of. The algorithm works by taking a starting
#position in the image (a bright pixel found in the loop contained in the function
#above). The function then adds the nearest neighbors of the initial pixel 
#to the 'queue' where they will processed to see
#if they are a bright or dark pixel. The function continues like this until 
#all the bright pixels that make up an object have been filled. 
#	This function also acts to append the bright pixels of an object to a list
#allowing the user to access and see each pixel that makes up an object

def fill(image, xSize, ySize, yStart, xStart, singleStarPix):

	queue = [(xStart, yStart)]
	while queue:
		x, y, queue = queue[0][0], queue[0][1], queue[1:]
		
		if image[y][x] == 1:
		
			image[y][x] = 0
			singleStarPix.append((y,x))
			
			if x > 0:
				queue.append((x-1, y))
			if x < (xSize - 1):
				queue.append((x+1, y))
			if y > 0:
				queue.append((x, y-1))
			if y< (ySize - 1):
				queue.append((x, y+1))
	return image
	return singleStarPix

#Simple function to work out the standard deviation of a list passed in
def meanAndStDev(pixelList):
	
	cumulativeDifference = 0
	totalEntries = 0
	cumulativePixVal = 0
	
	for k in range(len(pixelList)):
	
		totalEntries +=1
		cumulativePixVal += pixelList[k]
		
	mean = cumulativePixVal / totalEntries
	
	for j in range(len(pixelList)):
	
		difFromMeanSqr = (pixelList[j] - mean)**2
		totalEntries +=1
		cumulativeDifference += difFromMeanSqr
		
	var = (cumulativeDifference / totalEntries)
	standardDev = var**0.5

	return mean, standardDev


		
def varDiff(folderName, noOfObjToAlign):

	fileStrings = os.listdir(folderName)
	fileStrings.sort()
	matchedCobList = []
	masterMatchedCobAndBright = []
	timeData = []
	errorList = []

	for i in range(len(fileStrings)- 1):

		pixelNoListSource, cobAndPix, cobAndBright, binaryImageSource = countStars(folderName + fileStrings[0], printText = False, standAlone = False)

		pixelNoListTwo, cobAndPixTwo, cobAndBrightTwo, binaryImageTwo = countStars(folderName + fileStrings[i+1], printText = False, standAlone = False)

		matchingStarCobListOne, matchingStarCobListTwo = pairMatcher(cobAndPix, cobAndPixTwo, noOfObjToAlign, printText = False)

		#print matchingStarCobListTwo
		#print matchingStarCobListOne
		#print "---------------------------------------------------------------"
		#print cobAndBrightTwo

		matchedBrightList = []
		#print fileStrings[i]
		
		timePoint = timeGen(folderName + fileStrings[i])
		timeData.append(timePoint)		

		for j in range(len(matchingStarCobListTwo)):
			for k in range(len(cobAndBrightTwo)):
				if matchingStarCobListTwo[j] == cobAndBrightTwo[k][0]:

					matchedCobAndBright = matchingStarCobListTwo[j], cobAndBrightTwo[k][1]

					matchedBrightList.append(matchedCobAndBright)



		print sorted(matchedBrightList)
		
		print "------------------------------------------------------"

		masterMatchedCobAndBright.append(sorted(matchedBrightList))

	#print masterMatchedCobAndBright

	#for l in range(len(masterMatchedCobAndBright)):
	#	print (masterMatchedCobAndBright[l])

	brightnessDataPerStar = []
	errorOnPhotonCount = []

	starToPlot = int(raw_input("Enter star number: "))

	for m in range(len(masterMatchedCobAndBright)):

		dataPoint = masterMatchedCobAndBright[m][starToPlot][1]
		yError = np.sqrt(dataPoint)
		errorOnPhotonCount.append(yError*10)		
	
		brightnessDataPerStar.append(dataPoint)

	print brightnessDataPerStar
	print errorOnPhotonCount


     	plt.xlabel("Time")
        plt.ylabel("Integrated Intensity of Star")
	plt.title("Intergrated Brightness of a Star in Successive Frames")

	#plt.xticks(timeData, timeData)

	plt.plot(timeData, brightnessDataPerStar, 'bs')
	plt.errorbar(timeData, brightnessDataPerStar, yerr = errorOnPhotonCount)


def timeGen(fileName):

	hduList = fits.open(fileName)
	hdu = hduList[0]
	date = hdu.header['DATE-OBS']

	year = str(date[0]+date[1]+date[2]+date[3])
	intYear = int(year)
	month = str(date[5]+date[6])
	intMonth = int(month)
	day = str(date[8]+date[9])
	intDay = int(day)
	hour = str(date[11]+date[12])
	intHour = int(hour)
	minute = str(date[14] + date[15])
	intMinute = int(minute)
	second = str(date[17] + date[18])
	intSecond = int(second)

	time = str(hour+minute+second)
	intTime = int(time)

	dateTime = datetime.datetime(intYear,intMonth,intDay,intHour,intMinute,intSecond)
	return dateTime

def errorGen(fileName):

	hduList = fits.open(fileName)
	hdu = hduList[0]
	eGain = hdu.header['EGAIN']
	
	image = hduList[0].data

	pixError = np.sqrt(eGain * image)

	print pixError



	






