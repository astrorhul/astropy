import ErrorImage as _ErrorImage
import Manager    as _Manager 
import pyfits     as _pyfits

_manager = _Manager.gm()

def addImages(idenIn1, idenIn2, idenOut) : 
    '''Add image 'idenIn1' to 'idenIn2', with output 'idenOut' 
       idenOut = idenIn1 + idenIn2 '''
    i1 = _manager.getImage(idenIn1)
    i2 = _manager.getImage(idenIn2)
    i3 = i1['img'].add(i2['img']) 
    _manager.addImage(idenOut,i3,i1['header'])

def subImages(idenIn1, idenIn2, idenOut) : 
    '''Subtract image 'idenIn2' from 'idenIn1' with output 'idenOut' 
       idenOut = idenIn1 - idenIn2 '''
    i1 = _manager.getImage(idenIn1)
    i2 = _manager.getImage(idenIn2)
    i3 = i1['img'].subtract(i2['img']) 
    _manager.addImage(idenOut,i3,i1['header'])    

def divImages(idenIn1, idenIn2, idenOut) : 
    '''Divide image 'idenIn1' by 'idenIn2' with output 'idenOut' 
       idenOut = idenIn1/idenIn2 '''
    i1 = _manager.getImage(idenIn1)
    i2 = _manager.getImage(idenIn2)
    i3 = i1['img'].divide(i2['img']) 
    _manager.addImage(idenOut,i3,i1['header'])    

def mulImages(idenIn1, idenIn2, idenOut) : 
    '''Multiply image 'idenIn1' by 'idenIn2' with output 'idenOut' 
       idenOut = idenIn1*idenIn2 '''
    i1 = _manager.getImage(idenIn1)
    i2 = _manager.getImage(idenIn2)
    i3 = i1['img'].multipy(i2['img']) 
    _manager.addImage(idenOut,i3,i1['header'])    

def addConstant(idenIn1, const, idenOut) : 
    pass

def subConstant(idenIn1, const, idenOut) : 
    addConstant(idenIn1,-const,idenOut)

def pixelTransform(indenIn1, function) : 
    pass

def areaOfInterest(indenIn1, dXStart, dXend, dYStart, dYEnd) : 
    pass
