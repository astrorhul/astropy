#include "astro.h"
#include <stdio.h>
#include <algorithm>
#include <list>
#include <iostream>
#include <math.h>
//#include <TMath.h>
//#include <TF2.h>
//#include <TH2.h>

class point { 
    public :
        point(int x, int y) {
        this->_x = x;
        this->_y = y;
    }
    
    int _x;
    int _y;
    
    bool operator==(const point &a) const{
        bool out = false;
        if (a._x == this->_x && a._y == this->_y){
            out = true;
        }
        return out;
    }

};  

void cluster_cfun(const void * indatav, int rowcount, int colcount, void * outdatav) {
  const double * indata = (double *) indatav;
  double * outdata = (double *) outdatav;
  
  int i    = 0;
  int j    = 0;
  
  double k = 0;
  for(int i =0; i<rowcount;i++) {
    for(int j=0; j<colcount;j++) { 
      k = indata[i*rowcount+j];
      outdata[i*rowcount+j] = i*j*1.0;
    }
  }
  
  printf("%f\n",k);
  std::list<point> q;
  q.push_back(point(1,1));
  q.push_back(point(2,2));
  q.push_back(point(2,5));
  q.push_back(point(3,7));

  printf("q size : %i\n",(int)q.size());

  point p = q.back();
  q.pop_back();

  printf("%i %i\n",p._x,p._y);

  printf("q size : %i\n",(int)q.size());

  return;
}

void loop_cfun(const void *indatav, double cut, int rowcount, int colcount, int loop, void *outdatav) {
    const double *indata = (double *) indatav;
    double *outdata = (double *) outdatav;
    
    int i = 0;
    int size = rowcount * colcount;
                
    for (i=0; i<rowcount*colcount; i++){
        if (indata[i] < cut && loop == 1){
            outdata[i] = indata[i];
        }
        else if (indata[i] > cut && loop == 2){
            outdata[i] = 1;
        }
    }
        
    return;
}


// Mark stars in a image
// Mark = 100 -> smaller than minimum cluster size
// Makr > 100 -> cluster
void mark_cfun(void *indatav, int i, int j, int rowcount, int colcount, int *marker_p, int minClusSize){
    double *binMatrix = (double *) indatav;
    int marker = *marker_p;
    std::list<point> q;
    std::list<point> s;
    std::list<point> marked;
    
    q.push_back(point(i, j));
    s.push_back(point(i, j));
    
    // Run over a queue checking the neighbours
    while (q.size() > 0){
        point p = q.back();
        point t = q.back();
        q.pop_back();
        
        std::cout << t._x << " " << t._y << std::endl;
        
        if (binMatrix[p._x * rowcount + p._y] == 1.){
            binMatrix[p._x * rowcount + p._y] = marker;
            marked.push_back(p);
            
            // Add neighbours to the queue if ther were not checked yet
            if (p._x > 0){
                t._x -= 1;
                if (std::find(s.begin(), s.end(), t) == s.end()){
                    q.push_back(t);
                    s.push_back(t);
                }
                t._x += 1;
            }
            if (p._y > 0){
                t._y -= 1;
                if (std::find(s.begin(), s.end(), t) == s.end()){
                    q.push_back(t);
                    s.push_back(t);
                }
                t._y += 1;
            }
            if (p._x < rowcount-1){
                t._x += 1;
                if (std::find(s.begin(), s.end(), t) == s.end()){
                    q.push_back(t);
                    s.push_back(t);
                }
                t._x -= 1;
            }
            if (p._y < colcount-1){
                t._y += 1;
                if (std::find(s.begin(), s.end(), t) == s.end()){
                    q.push_back(t);
                    s.push_back(t);
                }
                t._y -= 1;
            }        
        }

    }
    
    // Remove clusters smaller than minClusSize
    if (marked.size() < minClusSize){
        while (marked.size() > 0){
            point m = marked.back();
            marked.pop_back();
            binMatrix[m._x * rowcount + m._y] = 100;
        }
        (*marker_p)--;
    }
    
    return;
}


    const double PI = 3.14159265359;
double gauss2D(double *x, double *par){
    double A = par[0];
    double mux = par[1];
    double muy = par[2];
    double sigx = par[3];
    double sigy = par[4];
    double angle = par[5] * PI / 180.0;
    
    double a = pow(cos(angle), 2) / (2*sigx*sigx) + pow(sin(angle), 2) / (2*sigy*sigy);
    double b = sin(2*angle) / (4*sigy*sigy) - sin(2*angle) / (4*sigx*sigx);
    double c = pow(cos(angle), 2) / (2*sigy*sigy) + pow(sin(angle), 2) / (2*sigx*sigx);
    
    return (A / (2*PI*sigx*sigy) ) * exp( -(a*pow((x[0] - mux), 2) + 2*b*(x[0]-mux)*(x[1]-muy) + c*pow((x[1]-muy), 2)) );
}

// par = [*bg[6], *gauss1[6], *gauss2[6], *gauss3[6]...]
double multiGauss2D(double *x, double *par, int starNumber){
    double bg = par[0] + par[1]*x[0] + par[2]*x[0]*x[0]
                    + par[3]*x[1] + par[4]*x[1]*x[1] + par[5]*x[0]*x[1];
    double field = bg;
    
    for (int s = 1; s<starNumber; s++){
        field += gauss2D(x, &par[6*s]);
    }
    
    
    return field;
}

double chi2(const void *indata, const void *inerrors, int rowcount, int colcount, double *par, int len_par, double *chiSq_p){
    double *data = (double *) indata;
    double *errors = (double *) inerrors;
    double var[] = {0, 0};
//    double chiSq = *chiSq_p;
    int starNumber = len_par/6;
    
    std::cout << "\n";
    
    for (int x=0; x<rowcount; x++){
        for (int y=0; y<colcount; y++){
            var[0] = x;
            var[1] = y;
            double field = multiGauss2D(var, par, starNumber);
            (*chiSq_p) += pow((data[x*rowcount + y] - field), 2) / errors[x*rowcount + y];
            
            
        }
    }
    
    std::cout << "chiSq: " << (*chiSq_p) << std::endl;
    
//    return chiSq;
}

/*
void fitGauss(const void *indatav, int rowcount, int colcount, int starNumber, double *x, double *moments){

    const Int_t npar = 6 + starNumber*6; // BG + N*2DGauss params
    
    Double_t params[npar] (npar, 0);
    params[0] = bg;
    
    for (int s = 1; s<=starNumber; s++){
        
//        double A = moments[0];
//        double mux = moments[1];
//        double muy = moments[2];
//        double sigx = moments[3];
//        double sigy = moments[4];
        
        param[6*s + 0] = A;
        param[6*s + 1] = mux;
        param[6*s + 2] = muy;
        param[6*s + 3] = sigx;
        param[6*s + 4] = sigy;
        param[6*s + 5] = angle;
    }
    
    TF2 *func = new TF2("MultiGauss2D", multiGauss2D, x[0], x[1], npar);
    func -> SetParameters(params);

} */

/*  
    Somehow minimize the minimization
    i.e. minimize the ChiSq(starNumber) from the minimization with starNumber defined
    Work with sigma limits [0.3*res , res]
    Background = median
    Amplitude = 2*median
    mux, muy = center or random
 */

/*
Double_t myMinChi2(int starNumber, const int rowcount, const int colcount, const int median, const int res, TH2D *data){
    
    const Int_t npar = 6 + starNumber*6; // BG + N*2DGauss params
    
    Double_t params[npar];
    params[0] = median;
    params[1] = params[2] = params[3] = params[4] = params[5] = 0;
    params[6] = starNumber;
    
    for (int s = 1; s<=starNumber; s++){
        
        //        double A = moments[0];
        //        double mux = moments[1];
        //        double muy = moments[2];
        //        double sigx = moments[3];
        //        double sigy = moments[4];
        
        params[6*s + 1] = 2*median;
        params[6*s + 2] = rowcount/2;
        params[6*s + 3] = colcount/2;
        params[6*s + 4] = res/2;
        params[6*s + 5] = res/2;
        params[6*s + 6] = 0;
        
    }
    
    TF2 *func = new TF2("MultiGauss2D", multiGauss2D, 0, rowcount, 0, colcount, npar);
    func -> SetParameters(params);
    
    for (int s = 1; s<=starNumber; s++){
        func -> SetParLimits(6*s + 1, 0, 100000000);
        func -> SetParLimits(6*s + 2, 0, rowcount);
        func -> SetParLimits(6*s + 3, 0, colcount);
        func -> SetParLimits(6*s + 4, 0.3*res, res);
        func -> SetParLimits(6*s + 5, 0.3*res, res);
        func -> SetParLimits(6*s + 6, -90, 90);
    }
        
    data -> Fit("MultiGauss2D", "V");
//
//    TF1 *fit = data -> GetFunction("MultiGauss2D");
//    Double_t chi2 = fit->GetChisquare();
//    return chi2;
    return 0;
}*/

/*
TH2D *data;
Double_t minChi2(Double_t *x, Double_t *par){
    Double_t rtn = myMinChi2(par[0], par[1], par[2], par[3], par[4], data);
    
    return rtn;
}*/
    
/*
void img2hist(const void *indatav, int rowcount, int colcount){
    const double *indata = (double *) indatav;
    
    for(int x =0; x<rowcount; x++) {
        for(int y=0; y<colcount; y++) {
            double z = indata[x*rowcount+y];
            data -> Fill(x, y, z);
        }
    }
}*/

    
/*
void outerMin(const void *indatav, int starNumber, int rowcount, int colcount, double median, int res){
    
    printf("lol\n");

    std::cout << "Inside C\n";
    
    const double *image = (double *) indatav;
    Double_t rowc = (int) rowcount;
    Double_t colc = (int) colcount;
    
    std::cout << "Going to create data\n";
    
    data = new TH2D("Data", "Hist from Image", rowcount, 0, rowcount, colcount, 0, colcount);
    
    std::cout << "Hist created\n";

    img2hist(image, rowcount, colcount);
    
    std::cout << "Img2Hist ok\n";

    const Int_t npar = 5;
    
    Double_t params[npar];
        
    TF2 *func = new TF2("minChi2", minChi2, 0, rowc, 0, colc, npar);
    func -> SetParameters(params);
    
    func -> SetParameter(0, starNumber);
    func -> FixParameter(1,rowcount);
    func -> FixParameter(2,colcount);
    func -> FixParameter(3,median);
    func -> FixParameter(4,res);
    
    std::cout << "All set!\n";

    data -> Fit("minChi2", "V");
//    
//    TF1 *fit = data -> GetFunction("MultiGauss2D");
//    Double_t chi2 = fit->GetChisquare();
//    std::cout << chi2;
}*/