import numpy as _np
import pylab as _pylab
import pyfits as _pyfits
import ErrorImage as _ErrorImage

class Manager : 
    def __init__(self) :
        self.images   = dict()
        self.data     = dict()

    def loadImage(self, name, fileName, notes = '') : 
        # open file and make error image
        f      = _pyfits.open(fileName)
        img    = _ErrorImage.ErrorImage(f[0])
        header = f[0].header
        f.close()

        # add image to manager
        self.addImage(name,img, header)
 
    def addImage(self,name,img,header = None) : 
        self.images[name] = {'header':header,'img':img,'name':name}

    def addData(self,name, type, data, plot = {}) : 
        self.data[name] = {'name':name,'type':type,'data':data,'plot':plot}

    def getImage(self,name) : 
        return self.images[name] 

    def getData(self,name) : 
        return self.data[name]

    def deleteImage(self,name) : 
        # remove image from dictionary
        self.images.pop(name)

    def duplicateImage(self,name, newName) : 
        pass

    def imageList(self) : 
        return self.images.keys()

    def dataList(self) : 
        return self.data.keys()

    def reset(self) : 
        self.images   = dict()
        self.data     = dict()        

manager = Manager()

def gm() : 
    return manager
                

        
