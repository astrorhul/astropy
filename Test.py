import matplotlib.pyplot as _plt
from matplotlib.path import Path
import matplotlib.patches as _patches

def rect_intersection(r1coords = [(0,0),(0,1),(1,1),(1,0)], r2coords= [(0.5,0.5),(0.5,1.5),(1.5,1.5),(1.5,0.5)]) : 
    r1verts = r1coords
    r1verts.append((0.,0.))
    r2verts = r2coords
    r2verts.append((0.,0.))        
    codes = [Path.MOVETO,Path.LINETO,Path.LINETO,Path.LINETO,Path.CLOSEPOLY]

    path1 = Path(r1verts, codes)
    path2 = Path(r2verts,codes)
    fig = _plt.figure(1)
    ax  = fig.add_subplot(111)
    
    patch1 = _patches.PathPatch(path1, facecolor='red', lw=1, alpha=0.2)    
    patch2 = _patches.PathPatch(path2, facecolor='blue', lw=1, alpha=0.2)    
    ax.add_patch(patch1)
    ax.add_patch(patch2)

    ax.set_xlim(-2,2)
    ax.set_ylim(-2,2)

    # loop around r1
    # determine intersections with all lines of r2, or identity 


    _plt.show()    

def test() : 
    image = LoadImage() 
    image = Image_Simulator(nstars = 20)
    ia = Image_Analysis(image, debugPrint = FALSE)
    cf = Cluster_Finder(image, debugPrint = FALSE )
    ca = Cluster_Analysis(debugPrint = TRUE) 
    pa = Photometry_Analysis(image)
    pf = Photometry_Fit(image)
    pa = Photometry_Aperture(image) 

    ia.process()
    cf.find() 
    ca.process()
    
    for cluster in cf.get_clusters() : 
        if not ca.good_cluster(cluster) continue
        
        car = ca.process(cluster)
        pfr = pf.process(cluster)
        par = pa.process(cluster) 
        
        print pfr.integral 
 
        # plotting
        
        
        

    


