import numpy                     as _np
import matplotlib.pyplot         as _plt
from matplotlib.mlab import find as _find
import Utilities                 as _Utilities

class Cluster_Analysis():
    '''
    USEAGE WARNING: Please note that the coordinates and data accessors are inverse to each other, that is the
    intensity located at data[x,y] is found on the plot at location (y, x).
    '''

    def __init__(self, image, error, threshold, debug_print = False):
        self.debug_print = debug_print
        self.image       = image
        self.error       = error
        self.threshold   = threshold
        self.utilities   = _Utilities.Utilities()

    def set_cluster(self, cluster, grid_expansion = 30):
        self.cluster     = cluster
        self.x           = cluster[:, 0]
        self.y           = cluster[:, 1]
        self.intensities = self.image[self.x, self.y]
        self.centre() # Sets self.x_w_mean, self.y_w_mean

    def root_mean_square(self, grid_expansion = 0): # Supposed to use grid expansion here?
        if grid_expansion != 0:
            expanded_cluster_points, expanded_error_points, grid_expansion, exp_x, exp_y = self.utilities.expand_grid(self.image, self.error, self.y_w_mean, self.x_w_mean, grid_expansion)
            total_cluster_points                  = expanded_cluster_points.sum()
            self.x_rms                            = _np.sqrt((expanded_cluster_points * (exp_x - self.x_w_mean)**2).sum() / total_cluster_points)
            self.y_rms                            = _np.sqrt((expanded_cluster_points * (exp_y - self.y_w_mean)**2).sum() / total_cluster_points)
        else:
            total_intensity = self.intensities.sum()
            self.x_rms      = _np.sqrt((self.intensities * (self.x - self.x_w_mean)**2).sum() / total_intensity)
            self.y_rms      = _np.sqrt((self.intensities * (self.y - self.y_w_mean)**2).sum() / total_intensity)

        if self.debug_print:
            print 'Cluster_Analysis.__root_mean_square()>             ', self.x_rms, self.y_rms
            print 'Grid expansion used for calculation:               ', grid_expansion

        return [self.x_rms, self.y_rms]

    def centre(self):
        '''
        Returns the weighted means of the x and y values in the 2D numpy array.  This means that x_w_mean will correspond to the physical y coordinate but
        the x index within the array.  The converse is true of y_w_mean.
        '''
        self.x_w_mean = (self.intensities * self.x).sum() / self.intensities.sum()
        self.y_w_mean = (self.intensities * self.y).sum() / self.intensities.sum()

        if self.debug_print:
            print 'Cluster_Analysis.centre()>                          %4.2f %4.2f' % (self.y_w_mean, self.x_w_mean) # (x, y) coords.

        return (self.x_w_mean, self.y_w_mean)

    def dimensions(self):
        self.x_dim = self.x.max() - self.x.min()
        self.y_dim = self.y.max() - self.y.min()

        if self.debug_print:
            print 'Cluster_Analysis.dimensions()>                     ', self.x_dim, self.y_dim

        return [self.x_dim, self.y_dim]

    def full_width_half_height_maximum(self): # Refactor
        '''
        Description:
            * Returns the full width at half height maximum of a cluster.

        Arguments:
            * Threshold: The threshold of the cluster, used to determine whether or not to ignore the cluster.

        Returns:
            * 0: if the cluster is a hot pixel.
            * -1: if the cluster is to be ignored.
        '''

        dimensions = self.dimensions() # Necessary call?
        if dimensions[0] == 0 and dimensions[1] == 0:
            self.fwhm = 0
            return 0 # This is a hot pixel.

        half_max = self.intensities.max() / 2.

        if half_max < self.threshold:
            self.fwhm = -1
            return -1 # Prevents index out of bounds error.

        slope_sign = _np.sign(half_max - self.intensities[0:-1]) - _np.sign(half_max - self.intensities[1:])

        if (slope_sign == _np.zeros(slope_sign.shape)).all():
            self.fwhm = -1
            return -1 # No change in gradient here, so ignore it.

        left_index  = _find(slope_sign > 0)[0]
        right_index = _find(slope_sign < 0)[-1]
        self.fwhm   = [abs(self.y[right_index] - self.y[left_index]), abs(self.x[right_index] - self.x[left_index])]

        if self.debug_print:
            print 'Cluster_Analysis.full_width_half_height_maximum()> ', self.fwhm

        return self.fwhm

    def intensity(self):
        self.total_intensity =  self.intensities.sum()

        if self.debug_print:
            print 'Cluster_Analysis.intensity()>                      ', self.total_intensity

        return self.total_intensity

    def good_cluster(self):
        good_cluster = True
        self.full_width_half_height_maximum()
        if len(self.cluster) == 1: good_cluster = False
        if self.fwhm == 0 or self.fwhm == -1: good_cluster = False

        return good_cluster

    def stats(self):
        stats = {}

        stats['fwhm']          = self.full_width_half_height_maximum()
        stats['x_centre']      = self.y_w_mean
        stats['y_centre']      = self.x_w_mean
        stats['dimensions']    = self.dimensions()
        stats['intensity']     = self.intensity()
        stats['min_intensity'] = self.intensities.min()
        stats['max_intensity'] = self.intensities.max()
        rms                    = self.root_mean_square()
        stats['x_sigma']       = rms[0]
        stats['y_sigma']       = rms[1]

        return stats
    
    def sort_clusters_by_intensity(self, clusters):
        clusters_with_intensity = []
        
        for cluster in clusters:
            self.set_cluster(cluster)
            clusters_with_intensity.append([cluster, self.intensity()])
         
        clusters_with_intensity.sort(key = lambda i: i[1])
        clusters_with_intensity.reverse()
        
        clusters_sorted = _np.array(clusters_with_intensity)
        
        return clusters_sorted[:, 0] # Only want the clusters back
    
def test(threshold = 1000, simple_image = True):
    import pyfits         as _pyfits
    import Cluster_Finder as _Cluster_Finder
    import Globals        as _Globals
    import numpy          as _np
    import pylab          as _pylab

    if simple_image:
        hdu = _pyfits.open(_Globals.photom_data_example_simple)
    else:
        hdu = _pyfits.open(_Globals.photom_data_example_complex)
    
    image = hdu[0].data
    error = _np.sqrt(hdu[0].header['EGAIN'] * image)

    cf = _Cluster_Finder.Cluster_Finder(image)
    cf.set_threshold(threshold)
    cl = cf.cluster()
    ca = Cluster_Analysis(image, error, threshold, debug_print = True)
    cl = ca.sort_clusters_by_intensity(cl)

    ccx = [] # cluster centre x
    ccy = [] # cluster centre y
    cin = [] # cluster insensity
    cxw = [] # cluster x width
    cyw = [] # cluster y width

    for cluster in cl:
        ca.set_cluster(cluster)
        if not ca.good_cluster(): continue

        print '-----------------------------------------'
        print 'Cluster_Analyis.test> Npix in cluster             :',len(cluster)

        stats = ca.stats()

        ccx.append(stats['x_centre'])
        ccy.append(stats['y_centre'])
        cin.append(stats['intensity'])
        cxw.append(stats['x_sigma'])
        cyw.append(stats['y_sigma'])
        
        print stats

    # Plotting
    f = _pylab.figure(1)
    f.clf()
    _pylab.subplot(2,2,1)
    _pylab.imshow(_np.log10(image), cmap = 'gray') # taking the log helps to show the movement
    _pylab.plot(ccx, ccy, "+", c = 'y')

    _pylab.subplot(2, 2, 2)
    _pylab.hist(cin, 20, (0, 500000))
    _pylab.semilogy(True)

    _pylab.subplot(2, 2, 3)
    _pylab.hist(cxw, 20, (0, 20), histtype = 'step')
    _pylab.hist(cyw, 20, (0, 20), histtype = 'step')

    _pylab.subplot(2, 2, 4)
    _pylab.plot(cxw, cyw, "+")
