import Cluster_Finder      as _Cluster_Finder
import Cluster_Analysis    as _Cluster_Analysis
import numpy               as _np
import Image_Simulator     as _Image_Simulator
import pyfits              as _pf
import Photometry_Aperture as _Photometry_Aperture
import pylab               as _pl
import os                  as _os

class Algorithm_Tester():

    def __init__(self):
        self.images = []
        self.errors = []
        self.truths = []
    
    def generate_images(self, image_count = 10, image_size = (1200, 700)):
             for i in range(1, image_count):
                 i_s = _Image_Simulator.Image_Simulator(star_count = i, image_size = image_size)
                 x, y, z, gain        = i_s.generate_image()
                 loc, std, amp, theta = i_s.get_parameters(x, y)
                 image                = i_s.generate_stars(z, x, y, loc, amp, std, theta)
                 i_s.write_simulation_to_file(image, loc[0], loc[1], amp.reshape((i,)), std[:,0], std[:,1], star_params_filename = 'alg_test_params_' + str(i))
                 i_s.create_fits_file(image, gain, image_filename = 'alg_test_image_' + str(i))
 
                 self.images.append(image)
                 self.errors.append(gain)
                 self.image_size = image_size
    
    def load_images(self, directory): # Loading into memory all at once a bad idea?  Need to check with Boogert.
        # Load the .fits files and params from the directory
        for file in _os.listdir(directory):
            filepath = _os.path.join(directory, file)
            if file.endswith('s'): # Either a .fits or .csv so this is ok.
                hdu      = _pf.open(filepath)
                image    = hdu[0].data
                error    = _np.sqrt(hdu[0].header['EGAIN'] * image)
                
                self.images.append(image)
                self.errors.append(error)
            else:
                sim_x_centre, sim_y_centre, sim_amp, sim_x_width, sim_y_width = _np.loadtxt(filepath, delimeter = ',').T
                self.truths.append([sim_x_centre, sim_y_centre])
            
        self.images = _np.array(self.images)
        self.errors = _np.array(self.errors)
        self.truths = _np.array(self.truths)
    
    def cluster_finder_test(self, threhsold):
        efficiencies = []
        avergae_distance   = []
        densities           = [] # Density doesn't vary over the range, so don't need an average.
        
        for image in self.images:
            cf       = _Cluster_Finder.Cluster_Finder(image, threshold)
            clusters = cf.cluster()
            ca       = _Cluster_Analysis.Cluster_Analysis(image, error, threshold)
            
            clusters_coords_detected = []
            for cluster in clusters:
                ca.set_cluster(cluster)
                if not ca.good_cluster():
                    continue
                centre = ca.centre() # (y, x)
                clusters_coords_detected.append(centre[1], centre[0])
            
            # Is there a way to reduce looping here?
            distances = []
            matched   = []
            for detected_cluster_coords in clusters_coords_detected:
                for simulated_cluster_coords in self.truths:
                    distance = _np.sqrt((j[0] - i[0])**2 + (j[1]-i[1])**2)
                    distances.append(distance)
                    if distance < 1: # make this a variable to be passed in
                        matched.append(detected_cluster_coords)
                        
            distances  = _np.array(distances)
            matched    = _np.array(matched)
            efficiency = (1.0 * len(matched)) / len(simulated_cluster_coords)
            efficiencies.append(efficiency)
            densities.append((1.0 * len(simulated_cluster_coords)) / (image.shape[0] * image.shape[1]))
        
        average_efficiency = []
        i = 0
        for i in range(0, len(efficiencies)):
            avg_eff = eff[:, i].mean()
            average_efficiency.append(avg_eff)
        
        return average_efficiency, density

    def cluster_finder_test(self, threshold, star_count, fits_file, params_file):
        hdu      = _pf.open(fits_file)
        image    = hdu[0].data
        error    = _np.sqrt(hdu[0].header['EGAIN'] * image)
        cf       = _Cluster_Finder.Cluster_Finder(image, threshold)
        clusters = cf.cluster()
        ca       = _Cluster_Analysis.Cluster_Analysis(image, error, threshold)
        clusters = ca.sort_clusters_by_intensity(clusters)

        clusters_measured = []

        for cluster in clusters:
            ca.set_cluster(cluster)
            if not ca.good_cluster(): 
                continue
            stats = ca.stats()
            clusters_measured.append([stats['x_centre'], stats['y_centre']])

        sim_x_centre, sim_y_centre, sim_amp, sim_std_x, sim_std_y = _np.loadtxt(params_file, delimiter = ',').T

        clusters_simulated = []
        clusters_simulated.append([sim_x_centre, sim_y_centre])

        c_measured  = _np.array(clusters_measured)
        c_measured  = _np.sort(c_measured, axis = 0)
        c_simulated = _np.array(clusters_simulated)
        c_simulated = c_simulated.transpose().reshape((star_count, len(clusters_measured[0])))
        c_simulated = _np.sort(c_simulated, axis = 0)
        matched     = []
        distances   = []
        
        for i in c_measured:
            for j in c_simulated:
                distance = _np.sqrt((j[0] - i[0])**2 + (j[1]-i[1])**2)
                distances.append(distance)
                if distance < 1:
                    matched.append(j)
        
        fig = _pl.gcf()
        _pl.clf()
        _pl.subplot(121)
        h = _pl.hist(distances, bins = 20)
        
        matched = _np.array(matched)

        return len(c_simulated), len(matched), distances

    def intensity_test(self, matched_stars):
        pa = _Photometry_Aperture.Photometry_Aperture()
        for star in matched_stars:
            pass

def test_one(directory):
    import pyfits  as _pf
    import Globals as _Globals
    import pylab   as _pl
    
    at = Algorithm_Tester()
    print 'Loading images'
    at.load_images(directory)
    print 'Loaded images'
    
    print 'calculate efficiencies & densities'
    efficiencies, densities = at.cluster_finder_test(1000)
    print 'calculated efficiencies & densities'
    
    _pl.scatter(efficiencies, densities)
    _pl.xlim(0, 0.005)
    _pl.show()

def test(image_count = 20, average_count = 10):
    import pyfits  as _pf
    import Globals as _Globals
    import pylab   as _pl

    average_clusters_matched = []
    average_clusters_simulated = []
    average_density = []
    average_efficiency = []

    for j in range(1, average_count):
        at = Algorithm_Tester()
        at.generate_images(image_count = image_count)

        clusters_simulated = []
        clusters_matched   = []
        density            = []
        i                  = 1 # Star / image counter
        for image in at.images:
            fits_file = _Globals.alg_sim_image_tester + str(i) + '.fits'
            params_file = _Globals.alg_sim_params_tester + str(i) + '.csv'
            simulated, matched, distance = at.cluster_finder_test(threshold = 1000, star_count = i, fits_file = fits_file, params_file = params_file)
            clusters_simulated.append(simulated)
            clusters_matched.append(matched)
            density.append((simulated * 1.0) / (at.image_size[0] * at.image_size[1]))
            i += 1

        clusters_matched   = _np.array(clusters_matched)
        clusters_simulated = _np.array(clusters_simulated)
        density            = _np.array(density)
        efficiency         = (clusters_matched * 1.0) / clusters_simulated
        
        average_clusters_matched.append(clusters_matched)
        average_efficiency.append(efficiency)

    average_clusters_matched = _np.array(average_clusters_matched)
    average_efficiency       = _np.array(average_efficiency)

    avg_eff = []

    for i in range(0, len(average_clusters_matched[0])):
        avg_eff.append(average_efficiency[:, i].mean())

    # Plotting
    fig = _pl.gcf()
    _pl.subplot(122)
    _pl.scatter(density, avg_eff)
    _pl.xlim(0, 0.00005)
