import numpy as _np
from scipy.stats import mode as _mode

class Image_Analysis():

    def __init__(self, image, debug_print = False):
        self.debug_print = debug_print
        self.image    = image

    def __get_intensity_location(self, intensity):
        x = _np.arange(0, self.image.shape[0], 1)
        y = _np.arange(0, self.image.shape[1], 1)

        x_grid, y_grid = _np.meshgrid(x, y)

        loc = self.image[x_grid, y_grid] == intensity

        x_loc, y_loc = x_grid[loc], y_grid[loc]

        if self.debug_print:
            print 'Image_Analysis.__get_intensity_location() ', [x_loc, y_loc]

        return [x_loc, y_loc]

    def mean(self):
        mean = self.image.mean()

        if self.debug_print :
            print 'Image_Analysis.mean()>                    ', mean

        return mean

    def mode(self):
        '''
        Returns an array of the modal (most common) value in the passed array.

        If there is more than one such value, only the first is returned.
        The bin-count for the modal bins is also returned.
        '''

        intensities = self.image.flatten()
        hist_intensities = _np.histogram(intensities, intensities.max(), (0, intensities.max()))
        hi_max = hist_intensities[0].max()
        mode = [hist_intensities[1][hi_max == hist_intensities[0]], hi_max]
        if len(mode[0]) > 1:
            mode = [mode[0][0], hi_max]

        if self.debug_print :
            print 'Image_Analysis.mode()>                    ', mode

        return mode

    def median(self):
        median = _np.median(self.image.flatten())

        if self.debug_print :
            print 'Image_Analysis.median()>                  ', median

        return median

    def min(self):
        '''
        Return the minimum value in the array and its index location as a list [x, y].
        '''

        minimum = self.image.min()

        loc     = self.__get_intensity_location(minimum)

        if self.debug_print:
            print 'Image_Analysis.min()>                     ', minimum, loc

        return minimum, loc

    def max(self):
        '''
        Return the maximum value in the array and its index location as a list [x, y].
        '''

        maximum = self.image.max()
        loc     = self.__get_intensity_location(maximum)

        if self.debug_print:
            print 'Image_Analysis.max()>                     ', maximum, loc

        return maximum, loc

    def standard_deviation(self):
        std_dev = self.image.std()

        if self.debug_print:
            print 'Image_Analysis.standard_deviation()>      ', std_dev

        return std_dev

    def get_pixels_above_threshold(self, threshold):
        pass # How does this differ from Cluster_Finder?

    def stats(self):
        stats = {}

        stats['mean']   = self.mean()
        stats['mode']   = self.mode()
        stats['median'] = self.median()
        stats['min']    = self.min()
        stats['max']    = self.max()
        stats['std']    = self.standard_deviation()

        return stats

def test(simple_image = True):
    import pyfits as _pyfits
    import Globals as _globals
    import pylab as _pylab

    if simple_image:
        image_file = _globals.photom_data_example_simple
    else:
        image_file = _globals.photom_data_example_complex

    # Usecase
    image = _pyfits.open(image_file)[0].data
    ia    = Image_Analysis(image, False)
    stats = ia.stats()

    print stats

    # Plotting
    _pylab.hist(_np.log(image.flatten()))
