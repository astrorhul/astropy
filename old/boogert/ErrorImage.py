import pylab  as _pylab
import pyfits as _pyfits
import numpy  as _numpy

class ErrorImage : 
    def __init__(self, image, errors = None) : 
        if type(image) == _pyfits.hdu.image.PrimaryHDU :
            # CCD gain 
            g = image.header['EGAIN']             
            self.image  = g*image.data
            self.errors = _pylab.sqrt(self.image)        
        elif errors != None : 
            self.image  = image
            self.errors = errors 

    def imageSize(self) :
        return self.image.shape

    def add(self, ei) : 
        image  = self.image + ei.image
        error  = _pylab.sqrt(self.errors**2+ei.errors**2)
        return ErrorImage(image,error)

    def subtract(self, ei) : 
        image  = self.image - ei.image
        error  = _pylab.sqrt(self.errors**2+ei.errors**2)
        return ErrorImage(image,error)

    def multiply(self, ei) : 
        image  = self.image * ei.image
        error  = image*_pylab.sqrt((self.errors/self.image)**2+(ei.errors/ei.image)**2)
        return ErrorImage(image,error)

    def divide(self, ei) : 
        image  = self.image / ei.image
        error  = image*_pylab.sqrt((self.errors/self.image)**2+(ei.errors/ei.image)**2)
        return ErrorImage(image,error)

    def maximum(self) : 
        return self.image.max()

    def minimum(self) : 
        return self.image.min()

    def mean(self) : 
        return self.image.mean()

    def median(self) : 
        pass 

    def mode(self) : 
        pass
    
    def rms(self) : 
        return self.image.std()
    
    def histogram(self, nbins = 20, min = None, max = None) : 
        flatImage = self.image.reshape(self.image.shape[0]*self.image.shape[1])
        return _numpy.histogram(flatImage,nbins,(min,max))

    def projectionX(self) : 
        image    = self.image
        error    = self.errors
        projX    = image.sum(0)
        range    = _numpy.arange(0,len(projX),1)
        projXErr = _numpy.sqrt((error**2).sum(0))
        return [range,projX,projXErr]

    def projectionY(self) :
        image    = self.image
        error    = self.errors
        projY    = image.sum(1)        
        range    = _numpy.arange(0,len(projY),1)
        projYErr = _numpy.sqrt((error**2).sum(1))
        return [range,projY,projYErr]
